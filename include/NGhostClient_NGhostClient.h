#ifndef NGHOSTCLIENT_NGHOSTCLIENT_PROTECT
#define NGHOSTCLIENT_NGHOSTCLIENT_PROTECT

// ---------------------------------
// struct NGhostClient::NGhostClient
// ---------------------------------

typedef struct NGhostClient
{
	// Configuration
	NGhostClientConfiguration *m_configuration;

	/* Devices list */
	NGhostDeviceContainerList *m_device;

	/* Services */
	// Scanner service (NGhostClientScannerService*)
	void *m_scannerService;

	/* Common basis */
	NGhostCommon *m_common;

	/* Is running? */
	NBOOL m_isRunning;

	/* Monitoring */
	NMonitoringAgent *m_monitoring;
} NGhostClient;

/**
 * Build the client
 *
 * @param configPath
 * 		The config file path
 *
 * @return the ghost client instance
 */
__ALLOC NGhostClient *NGhostClient_NGhostClient_Build( const char *configPath );

/**
 * Destroy client
 *
 * @param this
 * 		This instance
 */
void NGhostClient_NGhostClient_Destroy( NGhostClient** );

/**
 * Get configuration
 *
 * @param this
 * 		This instance
 *
 * @return the ghost client configuration
 */
const NGhostClientConfiguration *NGhostClient_NGhostClient_GetConfiguration( const NGhostClient* );

/**
 * Get scanner service
 *
 * @param this
 * 		This instance
 *
 * @return the scanner service (NGhostClientScannerService*)
 */
const void *NGhostClient_NGhostClient_GetScannerService( const NGhostClient* );

/**
 * Call scanning service
 *
 * @param this
 * 		This instance
 * @param wantedState
 * 		Wanted state: NTRUE for service activation, NFALSE for service deactivation
 *
 * @return if the scanned successfully started
 */
NBOOL NGhostClient_NGhostClient_CallScanningService( NGhostClient *this,
	NBOOL wantedState );

/**
 * Process scanner result set
 *
 * @param this
 * 		This instance
 * @param scannerResultSet
 * 		The scanner result set (NListe<NDeviceScannerResultSet*>)
 *
 * @return if operation succeeded
 */
NBOOL NGhostClient_NGhostClient_ProcessScannerResultSet( NGhostClient*,
	const NListe *scannerResultSet );

/**
 * Get device container list
 *
 * @param this
 * 		This instance
 *
 * @return the device container list
 */
const NGhostDeviceContainerList *NGhostClient_NGhostClient_GetDeviceContainerList( const NGhostClient* );

/**
 * Increase processed requests count
 *
 * @param this
 * 		This instance
 */
void NGhostClient_NGhostClient_IncreaseProcessedRequestCount( NGhostClient* );

/**
 * Get device status
 *
 * @param this
 * 		This instance
 *
 * @return the device status
 */
const NGhostDeviceStatus *NGhostClient_NGhostClient_GetDeviceStatus( const NGhostClient* );

/**
 * Get authentication manager
 *
 * @param this
 * 		This instance
 *
 * @return the authentication manager
 */
NAuthenticationManager *NGhostClient_NGhostClient_GetAuthenticationManager( const NGhostClient* );

/**
 * Get token manager
 *
 * @param this
 * 		This instance
 *
 * @return the token manager
 */
NGhostTokenManager *NGhostClient_NGhostClient_GetTokenManager( NGhostClient* );

/**
 * Get ghost info
 *
 * @param this
 * 		This instance
 *
 * @return the ghost info
 */
const NGhostInfo *NGhostClient_NGhostClient_GetGhostInfo( const NGhostClient* );

/**
 * Get common
 *
 * @param this
 * 		This instance
 *
 * @return the ghost common
 */
NGhostCommon *NGhostClient_NGhostClient_GetGhostCommon( NGhostClient* );

/**
 * Is running?
 *
 * @param this
 * 		This instance
 *
 * @return if client is running
 */
NBOOL NGhostClient_NGhostClient_IsRunning( const NGhostClient* );

/**
 * Get IP
 *
 * @param this
 * 		This instance
 *
 * @return the IP
 */
const char *NGhostClient_NGhostClient_GetIP( const NGhostClient* );

#endif // !NGHOSTCLIENT_NGHOSTCLIENT_PROTECT
