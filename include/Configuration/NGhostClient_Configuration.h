#ifndef NGHOSTCLIENT_CONFIGURATION_PROTECT
#define NGHOSTCLIENT_CONFIGURATION_PROTECT

// -------------------------------------
// namespace NGhostClient::Configuration
// -------------------------------------

// namespace NGhostClient::Configuration::Network
#include "Network/NGhostClient_Configuration_Network.h"

// namespace NGhostClient::Configuration::Scanner
#include "Scanner/NGhostClient_Configuration_Scanner.h"

// enum NGhostClient::Configuration::NGhostClientConfigurationProperty
#include "NGhostClient_Configuration_NGhostClientConfigurationProperty.h"

// struct NGhostClient::Configuration::NGhostClientConfiguration
#include "NGhostClient_Configuration_NGhostClientConfiguration.h"

#endif // !NGHOSTCLIENT_CONFIGURATION_PROTECT
