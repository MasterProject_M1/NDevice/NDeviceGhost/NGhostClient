#ifndef NGHOSTCLIENT_CONFIGURATION_NGHOSTCLIENTCONFIGURATION_PROTECT
#define NGHOSTCLIENT_CONFIGURATION_NGHOSTCLIENTCONFIGURATION_PROTECT

// -------------------------------------------------------------
// struct NGhostClient::Configuration::NGhostClientConfiguration
// -------------------------------------------------------------

typedef struct NGhostClientConfiguration
{
	/**
	 * Device info
	 */
	// Device name
	char *m_deviceName;

	// Device ID
	char *m_deviceUniqueID;

	/**
	 * Authentication
	 */
	// Username
	char *m_authenticationUsername;

	// Password
	char *m_authenticationPassword;

	// Authentication manager data persistent?
	NBOOL m_isAuthenticationManagerDataPersistent;

	/**
	 * Network
	 */
	// Listening interface
	char *m_networkListeningInterface;

	// Listening port
	NU32 m_networkListeningPort;

	// Trusted ip for token delivery
	NGhostTrustedTokenIPList *m_networkTokenTrustedIPList;

	/**
	 * Scanner
	 */
	// Scanner filtered interface
	NGhostClientConfigurationInterfaceFilter *m_scannerInterfaceFilter;

	// Thread count for scanning
	NU32 m_scannerThreadCount;

	// Connection timeout (ms)
	NU32 m_scannerConnectionTimeout;

	// Request timeout (ms)
	NU32 m_scannerRequestTimeout;

	// User addresses
	NGhostConfigurationScannerUserAddressList *m_scannerUserAddress;

	// Type filter
	NGhostClientConfigurationScannerTypeFilter *m_scannerTypeFilter;

	// UUID filter
	NGhostClientConfigurationScannerUUIDFilter *m_scannerUUIDFilter;

	/**
	 * Other informations
	 */
	// Build information
	char m_otherInformationBuildInformation[ 32 ];
} NGhostClientConfiguration;

/**
 * Load configuration
 *
 * @param configFile
 * 		The config file location
 *
 * @return the configuration instance
 */
__ALLOC NGhostClientConfiguration *NGhostClient_Configuration_NGhostClientConfiguration_Build( const char *configFile );

/**
 * Build default configuration
 *
 * @return the configuration instance
 */
__ALLOC NGhostClientConfiguration *NGhostClient_Configuration_NGhostClientConfiguration_Build2( void );

/**
 * Destroy configuration
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Configuration_NGhostClientConfiguration_Destroy( NGhostClientConfiguration** );

/**
 * Get the username
 *
 * @param this
 * 		This instance
 *
 * @return the username
 */
const char *NGhostClient_Configuration_NGhostClientConfiguration_GetAuthenticationUsername( const NGhostClientConfiguration* );

/**
 * Get the password
 *
 * @param this
 * 		This instance
 *
 * @return the password
 */
const char *NGhostClient_Configuration_NGhostClientConfiguration_GetAuthenticationPassword( const NGhostClientConfiguration* );

/**
 * Is authentication manager data persistent?
 *
 * @param this
 * 		This instance
 *
 * @return if the authentication manager data are persistent
 */
NBOOL NGhostClient_Configuration_NGhostClientConfiguration_IsAuthenticationManagerDataPersistent( const NGhostClientConfiguration* );
/**
 * Get the listening interface
 *
 * @param this
 * 		This instance
 *
 * @return the listening interface
 */
const char *NGhostClient_Configuration_NGhostClientConfiguration_GetNetworkListeningInterface( const NGhostClientConfiguration* );

/**
 * Is listening on all interfaces?
 *
 * @param this
 * 		This instance
 *
 * @return if the client must listen on all interface
 */
NBOOL NGhostClient_Configuration_NGhostClientConfiguration_IsNetworkMustListenAllInterface( const NGhostClientConfiguration * );

/**
 * Get listening port
 *
 * @param this
 * 		This instance
 *
 * @return the listening port
 */
NU32 NGhostClient_Configuration_NGhostClientConfiguration_GetNetworkListeningPort( const NGhostClientConfiguration* );

/**
 * Get the device name
 *
 * @param this
 * 		This instance
 *
 * @return the device name
 */
const char *NGhostClient_Configuration_NGhostClientConfiguration_GetDeviceName( const NGhostClientConfiguration* );

/**
 * Get build informations
 *
 * @param this
 * 		This instance
 *
 * @return the build informations
 */
const char *NGhostClient_Configuration_NGhostClientConfiguration_GetBuildInformation( const NGhostClientConfiguration* );

/**
 * Get the scan filter interface list
 *
 * @param this
 * 		This instance
 *
 * @return the scan filter interface list
 */
const NGhostClientConfigurationInterfaceFilter *NGhostClient_Configuration_NGhostClientConfiguration_GetScannerInterfaceFilter( const NGhostClientConfiguration* );

/**
 * Get device unique ID
 *
 * @param this
 * 		This instance
 *
 * @return the device unique ID
 */
const char *NGhostClient_Configuration_NGhostClientConfiguration_GetDeviceUniqueID( const NGhostClientConfiguration* );

/**
 * Get scanner thread count
 *
 * @param this
 * 		This instance
 *
 * @return the scanner thread count
 */
NU32 NGhostClient_Configuration_NGhostClientConfiguration_GetScannerThreadCount( const NGhostClientConfiguration* );

/**
 * Get scanner connection timeout
 *
 * @param this
 * 		This instance
 *
 * @return the scanner connection timeout
 */
NU32 NGhostClient_Configuration_NGhostClientConfiguration_GetScannerConnectionTimeout( const NGhostClientConfiguration* );

/**
 * Get scanner request timeout
 *
 * @param this
 * 		This instance
 *
 * @return the scanner request timeout
 */
NU32 NGhostClient_Configuration_NGhostClientConfiguration_GetScannerRequestTimeout( const NGhostClientConfiguration* );

/**
 * Get scanner user address
 *
 * @param this
 * 		This instance
 *
 * @return the user defined address to scan
 */
const NGhostConfigurationScannerUserAddressList *NGhostClient_Configuration_NGhostClientConfiguration_GetScannerUserAddressList( const NGhostClientConfiguration* );

/**
 * Get scanner uuid filter
 *
 * @param this
 * 		This instance
 *
 * @return the uuid filter list
 */
const NGhostClientConfigurationScannerUUIDFilter *NGhostClient_Configuration_NGhostClientConfiguration_GetScannerUUIDFilterList( const NGhostClientConfiguration* );

/**
 * Get scanner type filter
 *
 * @param this
 * 		This instance
 *
 * @return the type filter list
 */
const NGhostClientConfigurationScannerTypeFilter *NGhostClient_Configuration_NGhostClientConfiguration_GetScannerTypeFilterList( const NGhostClientConfiguration* );

/**
 * Is device type filtered?
 *
 * @param this
 * 		This instance
 * @param deviceType
 * 		The device type to be checked
 *
 * @return if the device type is filtered
 */
NBOOL NGhostClient_Configuration_NGhostClientConfiguration_IsDeviceTypeFiltered( const NGhostClientConfiguration*,
	NDeviceType deviceType );

/**
 * Is device uuid filtered
 *
 * @param this
 * 		This instance
 * @param uuid
 * 		The device uuid to be checked
 *
 * @return if the device uuid is filtered
 */
NBOOL NGhostClient_Configuration_NGhostClientConfiguration_IsDeviceUUIDFiltered( const NGhostClientConfiguration*,
	const char *uuid );

/**
 * Get trusted token ip list
 *
 * @param this
 * 		This instance
 *
 * @return the trusted ip list
 */
NGhostTrustedTokenIPList *NGhostClient_Configuration_NGhostClientConfiguration_GetTrustedTokenIPList( const NGhostClientConfiguration* );

/**
 * Save the configuration
 *
 * @param this
 * 		This instance
 * @param configPath
 * 		The config file path
 *
 * @return if operation succeeded
 */
NBOOL NGhostClient_Configuration_NGhostClientConfiguration_Save( const NGhostClientConfiguration*,
	const char *configPath );

#endif // !NGHOSTCLIENT_CONFIGURATION_NGHOSTCLIENTCONFIGURATION_PROTECT
