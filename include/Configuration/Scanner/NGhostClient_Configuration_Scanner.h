#ifndef NGHOSTCLIENT_CONFIGURATION_SCANNER_PROTECT
#define NGHOSTCLIENT_CONFIGURATION_SCANNER_PROTECT

// ----------------------------------------------
// namespace NGhostClient::Configuration::Scanner
// ----------------------------------------------

// struct NGhostClient::Configuration::Scanner::NGhostConfigurationScannerUserAddress
#include "NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddress.h"

// struct NGhostClient::Configuration::Scanner::NGhostConfigurationScannerUserAddressList
#include "NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList.h"

// struct NGhostClient::Configuration::Scanner::NGhostClientConfigurationScannerTypeFilter
#include "NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter.h"

// struct NGhostClient::Configuration::Scanner::NGhostClientConfigurationScannerUUIDFilter
#include "NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter.h"

#endif // !NGHOSTCLIENT_CONFIGURATION_SCANNER_PROTECT
