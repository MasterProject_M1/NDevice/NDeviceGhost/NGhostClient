#ifndef NGHOSTCLIENT_CONFIGURATION_SCANNER_NGHOSTCLIENTCONFIGURATIONSCANNERUUIDFILTER_PROTECT
#define NGHOSTCLIENT_CONFIGURATION_SCANNER_NGHOSTCLIENTCONFIGURATIONSCANNERUUIDFILTER_PROTECT

// ---------------------------------------------------------------------------------------
// struct NGhostClient::Configuration::Scanner::NGhostClientConfigurationScannerUUIDFilter
// ---------------------------------------------------------------------------------------

typedef struct NGhostClientConfigurationScannerUUIDFilter
{
	// UUID list
	NListe *m_uuid;
} NGhostClientConfigurationScannerUUIDFilter;

/**
 * Build uuid filter list
 *
 * @param configurationLine
 * 		The configuration line
 *
 * @return the instance
 */
__ALLOC NGhostClientConfigurationScannerUUIDFilter *NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_Build( const char *configurationLine );

/**
 * Destroy uuid filter list
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_Destroy( NGhostClientConfigurationScannerUUIDFilter** );

/**
 * Get filtered uuid count
 *
 * @param this
 * 		This instance
 *
 * @return the filtered uuid count
 */
NU32 NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_GetFilteredUUIDCount( const NGhostClientConfigurationScannerUUIDFilter* );

/**
 * Get filtered uuid
 *
 * @param this
 * 		This instance
 * @param index
 * 		The filtered uuid index
 *
 * @return the filtered uuid
 */
const char *NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_GetFilteredUUID( const NGhostClientConfigurationScannerUUIDFilter*,
	NU32 index );

/**
 * Is the uuid filtered?
 *
 * @param this
 * 		This instance
 * @param uuid
 * 		The uuid
 *
 * @return if the uuid is filtered
 */
NBOOL NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_IsUUIDFiltered( const NGhostClientConfigurationScannerUUIDFilter*,
	const char *uuid );

/**
 * Export uuid filter list
 *
 * @param this
 * 		This instance
 *
 * @return the configuration line associated to this instance
 */
__ALLOC char *NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_Export( const NGhostClientConfigurationScannerUUIDFilter* );

#endif // !NGHOSTCLIENT_CONFIGURATION_SCANNER_NGHOSTCLIENTCONFIGURATIONSCANNERUUIDFILTER_PROTECT
