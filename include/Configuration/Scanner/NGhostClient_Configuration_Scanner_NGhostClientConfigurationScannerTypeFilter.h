#ifndef NGHOSTCLIENT_CONFIGURATION_SCANNER_NGHOSTCLIENTCONFIGURATIONSCANNERTYPEFILTER_PROTECT
#define NGHOSTCLIENT_CONFIGURATION_SCANNER_NGHOSTCLIENTCONFIGURATIONSCANNERTYPEFILTER_PROTECT

// ---------------------------------------------------------------------------------------
// struct NGhostClient::Configuration::Scanner::NGhostClientConfigurationScannerTypeFilter
// ---------------------------------------------------------------------------------------

typedef struct NGhostClientConfigurationScannerTypeFilter
{
	// Type count
	NU32 m_typeCount;

	// Type filtered
	NDeviceType *m_filteredType;
} NGhostClientConfigurationScannerTypeFilter;

/**
 * Build filtered type
 *
 * @param configurationProperty
 * 		The configuration property
 *
 * @return the instance
 */
__ALLOC NGhostClientConfigurationScannerTypeFilter *NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_Build( const char *configurationProperty );

/**
 * Destroy filtered type
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_Destroy( NGhostClientConfigurationScannerTypeFilter** );

/**
 * Get filtered types count
 *
 * @param this
 * 		This instance
 *
 * @return the filtered types count
 */
NU32 NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_GetFilteredTypeCount( const NGhostClientConfigurationScannerTypeFilter* );

/**
 * Get filtered type
 *
 * @param this
 * 		This instance
 * @param index
 * 		The type index
 *
 * @return the filtered type
 */
NDeviceType NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_GetFilteredType( const NGhostClientConfigurationScannerTypeFilter*,
	NU32 index );

/**
 * Is filtered?
 *
 * @param this
 * 		This instance
 * @param deviceType
 * 		The device type
 *
 * @return if the type is filtered
 */
NBOOL NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_IsFiltered( const NGhostClientConfigurationScannerTypeFilter*,
	NDeviceType deviceType );

/**
 * Export to configuration line
 *
 * @param this
 * 		This instance
 *
 * @return the exported data
 */
__ALLOC char *NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_Export( const NGhostClientConfigurationScannerTypeFilter* );

#endif // !NGHOSTCLIENT_CONFIGURATION_SCANNER_NGHOSTCLIENTCONFIGURATIONSCANNERTYPEFILTER_PROTECT
