#ifndef NGHOSTCLIENT_CONFIGURATION_SCANNER_NGHOSTCONFIGURATIONSCANNERUSERADDRESS_PROTECT
#define NGHOSTCLIENT_CONFIGURATION_SCANNER_NGHOSTCONFIGURATIONSCANNERUSERADDRESS_PROTECT

// ----------------------------------------------------------------------------------
// struct NGhostClient::Configuration::Scanner::NGhostConfigurationScannerUserAddress
// ----------------------------------------------------------------------------------

typedef struct NGhostConfigurationScannerUserAddress
{
	// User address
	char *m_address;

	// User CIDR
	NU32 m_cidr;
} NGhostConfigurationScannerUserAddress;

/**
 * Build user address
 *
 * @param configurationProperty
 * 		The configuration read property to parse (X.X.X.X/CIDR)
 *
 * @return the user address
 */
__ALLOC NGhostConfigurationScannerUserAddress *NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddress_Build( const char *configurationProperty );

/**
 * Destroy user address
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddress_Destroy( NGhostConfigurationScannerUserAddress** );

/**
 * Get address
 *
 * @param this
 * 		This instance
 *
 * @return the address
 */
const char *NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddress_GetAddress( const NGhostConfigurationScannerUserAddress* );

/**
 * Get CIDR
 *
 * @param this
 * 		This instance
 *
 * @return the CIDR
 */
NU32 NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddress_GetCIDR( const NGhostConfigurationScannerUserAddress* );

/**
 * Export user address
 *
 * @param this
 * 		This instance
 *
 * @return the address with X.X.X.X/CIDR shape
 */
__ALLOC char *NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddress_Export( const NGhostConfigurationScannerUserAddress* );

#endif // !NGHOSTCLIENT_CONFIGURATION_SCANNER_NGHOSTCONFIGURATIONSCANNERUSERADDRESS_PROTECT
