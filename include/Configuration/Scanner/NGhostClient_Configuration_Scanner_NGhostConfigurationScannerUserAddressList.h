#ifndef NGHOSTCLIENT_CONFIGURATION_SCANNER_NGHOSTCONFIGURATIONSCANNERUSERADDRESSLIST_PROTECT
#define NGHOSTCLIENT_CONFIGURATION_SCANNER_NGHOSTCONFIGURATIONSCANNERUSERADDRESSLIST_PROTECT

// --------------------------------------------------------------------------------------
// struct NGhostClient::Configuration::Scanner::NGhostConfigurationScannerUserAddressList
// --------------------------------------------------------------------------------------

typedef struct NGhostConfigurationScannerUserAddressList
{
	// User defined addresses list (NGhostConfigurationScannerUserAddressList*)
	NListe *m_userAddressList;
} NGhostConfigurationScannerUserAddressList;

/**
 * Build user addresses list
 *
 * @param configurationLine
 * 		The configuration line
 *
 * @return the instance
 */
__ALLOC NGhostConfigurationScannerUserAddressList *NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_Build( const char *configurationLine );

/**
 * Destroy user addresses list
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_Destroy( NGhostConfigurationScannerUserAddressList** );

/**
 * Get the addresses count
 *
 * @param this
 * 		This instance
 *
 * @return the user address count
 */
NU32 NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_GetAddressCount( const NGhostConfigurationScannerUserAddressList* );

/**
 * Get the address
 *
 * @param this
 * 		This instance
 * @param index
 * 		The address index
 *
 * @return the user address
 */
const NGhostConfigurationScannerUserAddress *NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_GetAddress( const NGhostConfigurationScannerUserAddressList*,
	NU32 index );

/**
 * Export user address list
 *
 * @param this
 * 		This instance
 *
 * @return the exported data
 */
__ALLOC char *NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_Export( const NGhostConfigurationScannerUserAddressList* );

#endif // !NGHOSTCLIENT_CONFIGURATION_SCANNER_NGHOSTCONFIGURATIONSCANNERUSERADDRESSLIST_PROTECT
