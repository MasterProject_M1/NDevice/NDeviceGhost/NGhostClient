#ifndef NGHOSTCLIENT_CONFIGURATION_NETWORK_NGHOSTCLIENTCONFIGURATIONINTERFACEFILTER_PROTECT
#define NGHOSTCLIENT_CONFIGURATION_NETWORK_NGHOSTCLIENTCONFIGURATIONINTERFACEFILTER_PROTECT

// -------------------------------------------------------------------------------------
// struct NGhostClient::Configuration::Network::NGhostClientConfigurationInterfaceFilter
// -------------------------------------------------------------------------------------

typedef struct NGhostClientConfigurationInterfaceFilter
{
	// Filter
	NListe *m_interfaceList;
} NGhostClientConfigurationInterfaceFilter;

/**
 * Build interface filter
 *
 * @param configText
 * 		The config text loaded from file
 *
 * @return the filter instance
 */
__ALLOC NGhostClientConfigurationInterfaceFilter *NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Build( const char *configText );

/**
 * Destroy interface filter
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Destroy( NGhostClientConfigurationInterfaceFilter** );

/**
 * Is interface authorized?
 *
 * @param this
 * 		This instance
 * @param instanceName
 * 		The instance name to check
 *
 * @return if the interface is authorized
 */
NBOOL NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_IsInterfaceAuthorized( const NGhostClientConfigurationInterfaceFilter*,
	const char *instanceName );

/**
 * Export filter
 *
 * @param this
 * 		This instance
 *
 * @return an exported conf-compatible version of interface filter
 */
__ALLOC char *NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Export( const NGhostClientConfigurationInterfaceFilter* );

#endif // !NGHOSTCLIENT_CONFIGURATION_NETWORK_NGHOSTCLIENTCONFIGURATIONINTERFACEFILTER_PROTECT
