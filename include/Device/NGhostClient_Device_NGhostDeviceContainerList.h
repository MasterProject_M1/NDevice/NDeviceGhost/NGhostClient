#ifndef NGHOSTCLIENT_DEVICE_NGHOSTDEVICECONTAINERLIST_PROTECT
#define NGHOSTCLIENT_DEVICE_NGHOSTDEVICECONTAINERLIST_PROTECT

// ------------------------------------------------------
// struct NGhostClient::Device::NGhostDeviceContainerList
// ------------------------------------------------------

typedef struct NGhostDeviceContainerList
{
	// Device list (NListe<NGhostDeviceContainer*>)
	NListe *m_device;

	// Configuration
	const NGhostClientConfiguration *m_configuration;

	// Common data
	NGhostCommon *m_ghostCommon;

	// Authentication manager
	NAuthenticationManager *m_authenticationManager;

	// Is duplicate (reduced functionnalities)
	NBOOL m_isDuplicate;
} NGhostDeviceContainerList;

/**
 * Build device container list
 *
 * @param configuration
 * 		The ghost configuration
 * @param ghostCommon
 * 		The ghost common instance
 *
 * @return the device container list
 */
__ALLOC NGhostDeviceContainerList *NGhostClient_Device_NGhostDeviceContainerList_Build( const NGhostClientConfiguration *configuration,
	NGhostCommon *ghostCommon );

/**
 * Destroy device container list
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Device_NGhostDeviceContainerList_Destroy( NGhostDeviceContainerList** );

/**
 * Process scanner result set
 *
 * @param this
 * 		This instance
 * @param scannerResultSetList
 *		The scanner result set (NListe<NDeviceScannerResultSet*>)
 *
 * @return if process succeeded
 */
__WILLLOCK __WILLUNLOCK  NBOOL NGhostClient_Device_NGhostDeviceContainerList_ProcessScannerResultSet( NGhostDeviceContainerList*,
	const NListe *scannerResultSetList );

/**
 * Get ghost watcher device ip list
 *
 * @param this
 * 		This instance
 *
 * @return the ghost watcher ip list
 */
__MUSTBEPROTECTED __ALLOC NListe *NGhostClient_Device_NGhostDeviceContainerList_FindGhostWatcherIPList( const NGhostDeviceContainerList* );

/**
 * Get device with uuid
 *
 * @param this
 * 		This instance
 * @param uuid
 * 		The device uuid
 *
 * @return the device
 */
__MUSTBEPROTECTED NGhostDeviceContainer *NGhostClient_Device_NGhostDeviceContainerList_FindDeviceWithUUID( const NGhostDeviceContainerList*,
	const char *uuid );

/**
 * Find device with ip and type
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The ip
 * @param type
 * 		The type
 *
 * @return the device or NULL
 */
__MUSTBEPROTECTED NGhostDeviceContainer *NGhostClient_Device_NGhostDeviceContainerList_FindDeviceWithIPAndType( const NGhostDeviceContainerList*,
	const char *ip,
	NDeviceType type );

/**
 * Get device with index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The device index
 *
 * @return the device
 */
__MUSTBEPROTECTED NGhostDeviceContainer *NGhostClient_Device_NGhostDeviceContainerList_FindDeviceWithIndex( const NGhostDeviceContainerList*,
	NU32 index );

/**
 * Get device count
 *
 * @param this
 * 		This instance
 *
 * @return the device count
 */
__MUSTBEPROTECTED NU32 NGhostClient_Device_NGhostDeviceContainerList_GetDeviceCount( const NGhostDeviceContainerList* );

/**
 * Get configuration
 *
 * @param this
 * 		This instance
 *
 * @return the configuration
 */
const NGhostClientConfiguration *NGhostClient_Device_NGhostDeviceContainerList_GetConfiguration( const NGhostDeviceContainerList* );

/**
 * Get ghost common instance
 *
 * @param this
 * 		This instance
 *
 * @return the ghost common instance
 */
NGhostCommon *NGhostClient_Device_NGhostDeviceContainerList_GetCommon( const NGhostDeviceContainerList* );

/**
 * Duplicate list (to use each time you want to work with items)
 *
 * @param this
 * 		This instance
 *
 * @return the duplicated devices list
 */
__ALLOC __WILLLOCK __WILLUNLOCK NGhostDeviceContainerList *NGhostClient_Device_NGhostDeviceContainerList_Duplicate( const NGhostDeviceContainerList* );

/**
 * Get the device to process (only for duplicate)
 *
 * @param this
 * 		This instance
 * @param requestedDevice
 * 		The requested devices uri
 * @param cursor
 * 		The cursor in the requested uri
 *
 * @return the device to be processed
 */
const NGhostDeviceContainer *NGhostClient_Device_NGhostDeviceContainerList_GetDeviceToProcess( const NGhostDeviceContainerList*,
	const char *requestedDevice,
	NU32 *cursor );

/**
 * Broadcast packet request to ghost devices
 *
 * @param this
 * 		This instance
 * @param request
 * 		The http request
 *
 * @return if the operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NGhostClient_Device_NGhostDeviceContainerList_BroadcastRequestGhostDevice( NGhostDeviceContainerList*,
	const NRequeteHTTP *request );

/**
 * Broadcast packet request to ghost devices except specified ip:port one
 *
 * @param this
 * 		This instance
 * @param request
 * 		The http request
 * @param ipFilter
 * 		The ip filter
 *
 * @return if the operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NGhostClient_Device_NGhostDeviceContainerList_BroadcastRequestGhostDevice2( NGhostDeviceContainerList*,
	const NRequeteHTTP *request,
	const char *ipFilter );

/**
 * Activate protection
 *
 * @param this
 * 		This instance
 */
__WILLLOCK void NGhostClient_Device_NGhostDeviceContainerList_ActivateProtection( NGhostDeviceContainerList* );

/**
 * Deactivate protection
 *
 * @param this
 * 		This instance
 */
__WILLUNLOCK void NGhostClient_Device_NGhostDeviceContainerList_DeactivateProtection( NGhostDeviceContainerList* );

#endif // !NGHOSTCLIENT_DEVICE_NGHOSTDEVICECONTAINERLIST_PROTECT
