#ifndef NGHOSTCLIENT_DEVICE_REMOTE_PROTECT
#define NGHOSTCLIENT_DEVICE_REMOTE_PROTECT

// --------------------------------------
// namespace NGhostClient::Device::Remote
// --------------------------------------

// Authentication client timeout
#define NGHOSTCLIENT_DEVICE_REMOTE_AUTHENTICATION_CONNECTION_TIMEOUT		2000

// Authentication retry delay
#define NGHOSTCLIENT_DEVICE_REMOTE_AUTHENTICATION_CONNECTION_RETRY_DELAY	2000

// Connection timeout
#define NGHOST_CLIENT_DEVICE_REMOTE_BROADCAST_CONNECTION_TIMEOUT			500

// Delay between two emission of packet (ms)
#define NGHOST_CLIENT_DEVICE_REMOTE_DELAY_BETWEEN_TWO_HTTP_PACKET			600

// Delay between two updates of ghost remote device
#define NGHOST_CLIENT_DEVICE_REMOTE_DELAY_BETWEEN_UPDATE					10000

// struct NGhostClient::Device::Remote::NGhostRemoteDeviceAuthentication
#include "NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication.h"

// struct NGhostClient::Device::Remote::NGhostRemoteDevice
#include "NGhostClient_Device_Remote_NGhostRemoteDevice.h"

// Assert timeout
nassert( NGHOST_CLIENT_DEVICE_REMOTE_DELAY_BETWEEN_TWO_HTTP_PACKET > NHTTP_SERVEUR_TEMPS_AVANT_TIMEOUT_RECEPTION,
	"Timeout is too short!" );

#endif // !NGHOSTCLIENT_DEVICE_REMOTE_PROTECT
