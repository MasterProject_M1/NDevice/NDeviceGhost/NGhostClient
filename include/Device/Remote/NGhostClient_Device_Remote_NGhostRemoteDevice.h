#ifndef NGHOSTCLIENT_DEVICE_REMOTE_NGHOSTREMOTEDEVICE_PROTECT
#define NGHOSTCLIENT_DEVICE_REMOTE_NGHOSTREMOTEDEVICE_PROTECT

// -------------------------------------------------------
// struct NGhostClient::Device::Remote::NGhostRemoteDevice
// -------------------------------------------------------

typedef struct NGhostRemoteDevice
{
	// Ghost device container list
	NGhostDeviceContainerList *m_deviceContainerList;

	// Authentication manager
	NAuthenticationManager *m_authenticationManager;

	// Address we used to contact the device
	char *m_contactingIP;

	// Authentication
	NGhostRemoteDeviceAuthentication *m_authentication;

	// HTTP client
	NClientHTTP *m_httpClient;

	// Update thread
	NBOOL m_isUpdateThreadRunning;
	NThread *m_updateThread;
} NGhostRemoteDevice;

/**
 * Build ghost remote device
 *
 * @param authenticationManager
 * 		The authentication manager
 * @param contactingIP
 * 		The contacting IP
 * @param hostname
 * 		The remote ghost hostname
 * @param port
 * 		The remote ghost port
 * @param deviceType
 * 		The remote device type
 * @param deviceContainerList
 * 		The device container list
 *
 * @return the ghost remote device instance
 */
__ALLOC NGhostRemoteDevice *NGhostClient_Device_Remote_NGhostRemoteDevice_Build( NAuthenticationManager *authenticationManager,
	const char *contactingIP,
	const char *hostname,
	NU32 port,
	NDeviceType deviceType,
	NGhostDeviceContainerList *deviceContainerList );

/**
 * Destroy remote device
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Device_Remote_NGhostRemoteDevice_Destroy( NGhostRemoteDevice** );

/**
 * Get remote authentication
 *
 * @param this
 * 		This instance
 *
 * @return the remote authentication
 */
const NGhostRemoteDeviceAuthentication *NGhostClient_Device_Remote_NGhostRemoteDevice_GetRemoteAuthentication( const NGhostRemoteDevice* );

/**
 * Get contacting IP
 *
 * @param this
 * 		This instance
 *
 * @return the contacting IP
 */
const char *NGhostClient_Device_Remote_NGhostRemoteDevice_GetContactingIP( const NGhostRemoteDevice* );

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
NBOOL NGhostClient_Device_Remote_NGhostRemoteDevice_BuildParserOutputList( const NGhostRemoteDevice*,
	const char *key,
	__OUTPUT NParserOutputList *parserOutputList );

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 *
 * @return if operation succeeded
 */
NBOOL NGhostClient_Device_Remote_NGhostRemoteDevice_ProcessRESTGetRequest( const NGhostRemoteDevice*,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *requestedElement,
	NU32 *cursor );

/**
 * Send request
 *
 * @param this
 * 		This instance
 * @param request
 * 		The http request
 * @param requestTimeout
 * 		The request timeout
 *
 * @return if the operation succeeded
 */
NBOOL NGhostClient_Device_Remote_NGhostRemoteDevice_SendRequest( NGhostRemoteDevice *this,
	__WILLBEOWNED NRequeteHTTP *request,
	NU32 requestTimeout );

#endif // !NGHOSTCLIENT_DEVICE_REMOTE_NGHOSTREMOTEDEVICE_PROTECT
