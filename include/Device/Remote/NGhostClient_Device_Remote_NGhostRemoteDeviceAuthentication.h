#ifndef NGHOSTCLIENT_DEVICE_REMOTE_NGHOSTREMOTEDEVICEAUTHENTICATION_PROTECT
#define NGHOSTCLIENT_DEVICE_REMOTE_NGHOSTREMOTEDEVICEAUTHENTICATION_PROTECT

// ---------------------------------------------------------------------
// struct NGhostClient::Device::Remote::NGhostRemoteDeviceAuthentication
// ---------------------------------------------------------------------

typedef struct NGhostRemoteDeviceAuthentication
{
	// Device container list
	NGhostDeviceContainerList *m_deviceContainerList;

	// Authentication thread
	NBOOL m_isAuthenticationThreadRunning;
	NThread *m_thread;

	// Authentication manager
	NAuthenticationManager *m_authenticationManager;

	// Last authentication attempt
	NU64 m_lastAuthenticationAttemptTimestamp;

	// Authentication retry count
	NU32 m_authenticationRetryCount;

	// Authentication entry
	const NAuthenticationEntry *m_authenticationEntry;

	// Hostname
	char *m_hostname;

	// Port
	NU32 m_port;

	// Device type
	NDeviceType m_deviceType;

	// HTTP client
	const NClientHTTP *m_httpClient;

	// Contacting IP
	const char *m_contactingIP;
} NGhostRemoteDeviceAuthentication;

/**
 * Notify we are here to this instance
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
NBOOL NGhostClient_Device_Remote_NGhostRemoteAuthentication_NotifyClient( const NGhostRemoteDeviceAuthentication* );

/**
 * Build authentication
 *
 * @param hostname
 * 		The remote hostname
 * @param port
 * 		The remote port
 * @param authenticationManager
 * 		The authentication manager
 * @param deviceType
 * 		The device type
 * @param httpClient
 * 		The http client
 * @param deviceContainerList
 * 		The device container list
 * @param contactingIP
 * 		The IP used to contact the device (from our end) The value is stored in NGhostRemoteDevice
 *
 * @return the device authentication instance
 */
__ALLOC NGhostRemoteDeviceAuthentication *NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_Build( const char *hostname,
	NU32 port,
	NAuthenticationManager *authenticationManager,
	NDeviceType deviceType,
	const NClientHTTP *httpClient,
	NGhostDeviceContainerList *deviceContainerList,
	const char *contactingIP );

/**
 * Destroy authentication
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_Destroy( NGhostRemoteDeviceAuthentication** );

/**
 * Get authentication entry
 *
 * @param this
 * 		This instance
 *
 * @return the authentication entry
 */
const NAuthenticationEntry *NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_GetAuthenticationEntry( const NGhostRemoteDeviceAuthentication* );

/**
 * Is authenticated?
 *
 * @param this
 * 		This instance
 *
 * @return if we're authenticated on remote device
 */
NBOOL NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_IsAuthenticated( const NGhostRemoteDeviceAuthentication* );

/**
 * Get device type
 *
 * @param this
 * 		This instance
 *
 * @return the device type
 */
NDeviceType NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_GetDeviceType( const NGhostRemoteDeviceAuthentication* );

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if operation succeeded
 */
NBOOL NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_BuildParserOutputList( const NGhostRemoteDeviceAuthentication*,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList );

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 *
 * @return if operation succeeded
 */
NBOOL NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_ProcessRESTGetRequest( const NGhostRemoteDeviceAuthentication*,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *requestedElement,
	NU32 *cursor );

/**
 * Packet receive callback
 *
 * @param packet
 * 		The received packet
 * @param client
 * 		The client
 *
 * @return if operation succeeded
 */
__CALLBACK NBOOL NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_CallbackPacketReceive( const NPacket *packet,
	const NClient *client );

#endif // !NGHOSTCLIENT_DEVICE_REMOTE_NGHOSTREMOTEDEVICEAUTHENTICATION_PROTECT
