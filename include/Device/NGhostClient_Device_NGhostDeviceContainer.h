#ifndef NGHOSTCLIENT_DEVICE_NGHOSTDEVICECONTAINER_PROTECT
#define NGHOSTCLIENT_DEVICE_NGHOSTDEVICECONTAINER_PROTECT

// --------------------------------------------------
// struct NGhostClient::Device::NGhostDeviceContainer
// --------------------------------------------------

typedef struct NGhostDeviceContainer
{
	// Device type
	NDeviceType m_type;

	// Device
	void *m_device;

	// Device name
	char *m_name;

	// UUID
	char *m_uuid;

	// IP
	char *m_ip;

	// Ghost client configuration
	const NGhostClientConfiguration *m_configuration;

	// Action updater
	NGhostActionUpdater *m_actionUpdater;
} NGhostDeviceContainer;

/**
 * Build a device
 *
 * @param type
 * 		The device type
 * @param name
 * 		The device name
 * @param ip
 * 		The device ip
 * @param uuid
 * 		The unique device id
 * @param contactingIP
 * 		The contacting IP from our end used to contact the service
 * @param authenticationManager
 * 		The authentication manager
 * @param deviceContainerList
 * 		The device container list
 * @param actionList
 * 		The action list
 *
 * @return the device container instance
 */
__ALLOC NGhostDeviceContainer *NGhostClient_Device_NGhostDeviceContainer_Build( NDeviceType type,
	const char *name,
	const char *ip,
	const char *uuid,
	const char *contactingIP,
	NAuthenticationManager *authenticationManager,
	void *deviceContainerList,
	NGhostActionList *actionList );

/**
 * Destroy container
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Device_NGhostDeviceContainer_Destroy( NGhostDeviceContainer** );

/**
 * Get device type
 *
 * @param this
 * 		This instance
 *
 * @return the device type
 */
NDeviceType NGhostClient_Device_NGhostDeviceContainer_GetType( const NGhostDeviceContainer* );

/**
 * Get device name
 *
 * @param this
 * 		This instance
 *
 * @return the device name
 */
const char *NGhostClient_Device_NGhostDeviceContainer_GetName( const NGhostDeviceContainer* );

/**
 * Get device
 *
 * @param this
 * 		This instance
 *
 * @return the device
 */
void *NGhostClient_Device_NGhostDeviceContainer_GetDevice( const NGhostDeviceContainer* );

/**
 * Get the uuid
 *
 * @param this
 * 		This instance
 *
 * @return the uuid
 */
const char *NGhostClient_Device_NGhostDeviceContainer_GetUUID( const NGhostDeviceContainer* );

/**
 * Get the device ip
 *
 * @param this
 * 		This instance
 *
 * @return the device ip
 */
const char *NGhostClient_Device_NGhostDeviceContainer_GetIP( const NGhostDeviceContainer* );

/**
 * Get configuration
 *
 * @param this
 * 		This instance
 *
 * @return the configuration
 */
const NGhostClientConfiguration *NGhostClient_Device_NGhostDeviceContainer_GetGhostClientConfiguration( const NGhostDeviceContainer* );

/**
 * Is this uuid associated to this device
 *
 * @param this
 * 		This instance
 * @param uuid
 * 		The uuid to compare with
 *
 * @return if uuid is the same then the device's one
 */
NBOOL NGhostClient_Device_NGhostDeviceContainer_IsUUIDAssociatedWithDevice( const NGhostDeviceContainer*,
	const char *uuid );

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param isRootOnly
 * 		We only want the root?
 * @param index
 * 		The device index
 *
 * @return if operation succeeded
 */
NBOOL NGhostClient_Device_NGhostDeviceContainer_BuildParserOutputList( const NGhostDeviceContainer*,
	__OUTPUT NParserOutputList *parserOutputList,
	NBOOL isRootOnly,
	NU32 index );

/**
 * Process REST get request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element in device
 * @param cursor
 * 		The cursor in the requested element
 * @param clientID
 * 		The client id
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostClient_Device_NGhostDeviceContainer_ProcessRESTGetRequest( const NGhostDeviceContainer*,
	const char *requestedElement,
	NU32 *cursor,
	NU32 clientID );

/**
 * Process REST put request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 *		The requested element
 * @param cursor
 * 		The cursor in the requested element
 * @param clientID
 * 		The client id
 * @param clientJsonData
 * 		The parsed client json sent data
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostClient_Device_NGhostDeviceContainer_ProcessRESTPutRequest( NGhostDeviceContainer*,
	const char *requestedElement,
	NU32 *cursor,
	NU32 clientID,
	const NParserOutputList *clientJsonData );

/**
 * Process REST post request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 *		The requested element
 * @param cursor
 * 		The cursor in the requested element
 * @param clientID
 * 		The client id
 * @param clientJsonData
 * 		The parsed client json sent data
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostClient_Device_NGhostDeviceContainer_ProcessRESTPostRequest( NGhostDeviceContainer*,
	const char *requestedElement,
	NU32 *cursor,
	NU32 clientID,
	const NParserOutputList *clientJsonData );

/**
 * Send request to device
 *
 * @param this
 * 		This instance
 * @param request
 * 		The request
 *
 * @return if the operation succeeded
 */
NBOOL NGhostClient_Device_NGhostDeviceContainer_SendRequest( NGhostDeviceContainer*,
	__WILLBEOWNED NRequeteHTTP *request );

#endif // !NGHOSTCLIENT_DEVICE_NGHOSTDEVICECONTAINER_PROTECT
