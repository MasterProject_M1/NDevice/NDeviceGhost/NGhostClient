#ifndef NGHOSTCLIENT_DEVICE_ACTION_PROTECT
#define NGHOSTCLIENT_DEVICE_ACTION_PROTECT

// --------------------------------------
// namespace NGhostClient::Device::Action
// --------------------------------------

// Delay between action update
#define NGHOST_CLIENT_DEVICE_ACTION_DELAY_BETWEEN_UPDATE			10000

/**
 * Action update thread
 *
 * @param ghostActionUpdater
 * 		Ghost action updater instance
 *
 * @return if the operation succeeded
 */
__THREAD NBOOL NGhostClient_Device_Action_UpdateThread( NGhostActionUpdater *actionUpdater );

#endif // !NGHOSTCLIENT_DEVICE_ACTION_PROTECT
