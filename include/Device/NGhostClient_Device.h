#ifndef NGHOSTCLIENT_DEVICE_PROTECT
#define NGHOSTCLIENT_DEVICE_PROTECT

// ------------------------------
// namespace NGhostClient::Device
// ------------------------------

// namespace NGhostClient::Device::Action
#include "Action/NGhostClient_Device_Action.h"

// struct NGhostClient::Device::NGhostDeviceContainer
#include "NGhostClient_Device_NGhostDeviceContainer.h"

// struct NGhostClient::Device::NGhostDeviceContainerList
#include "NGhostClient_Device_NGhostDeviceContainerList.h"

// namespace NGhostClient::Device::Remote
#include "Remote/NGhostClient_Device_Remote.h"

#endif // !NGhostClient_Device_PROTECT
