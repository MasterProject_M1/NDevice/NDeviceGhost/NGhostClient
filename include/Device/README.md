Device container
================

Introduction
------------

The NGhostDeviceContainerList instance will be use
to store built devices.

Once created, it won't be delete until the NGhostClient
destruction.

When trying to add a new device, the new-coming uuid will be
checked and won't be accepted if already present in the list.

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

