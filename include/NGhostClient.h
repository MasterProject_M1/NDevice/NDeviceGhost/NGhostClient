#ifndef NGHOSTCLIENT_PROTECT
#define NGHOSTCLIENT_PROTECT

// ----------------------
// namespace NGhostClient
// ----------------------

// namespace NLib
#include "../../../../NLib/NLib/NLib/include/NLib/NLib.h"

// namespace NHTTP
#include "../../../../NLib/NHTTP/include/NHTTP.h"

// namespace NDeviceCommon
#include "../../../NDeviceCommon/include/NDeviceCommon.h"

// namespace NDeviceScanner
#include "../../../NDeviceScanner/include/NDeviceScanner.h"

// namespace NDeviceHUE
#include "../../../NDeviceHUE/include/NDeviceHUE.h"

// namespace NDeviceMFI
#include "../../../NDeviceMFI/include/NDeviceMFI.h"

// namespace NParser
#include "../../../../NParser/NParser/include/NParser.h"

// namespace NParser::NJson
#include "../../../../NParser/NJson/include/NJson.h"

// __Version
#include "__/__NGhostClient_Version.h"

// namespace NGhostCommon
#include "../../NGhostCommon/include/NGhostCommon.h"

// namespace NGhostClient::Configuration
#include "Configuration/NGhostClient_Configuration.h"

// namespace NGhostClient::Device
#include "Device/NGhostClient_Device.h"

// struct NGhostClient::NGhostClient
#include "NGhostClient_NGhostClient.h"

// namespace NGhostClient::Service
#include "Service/NGhostClient_Service.h"

#endif // !NGHOSTCLIENT_PROTECT
