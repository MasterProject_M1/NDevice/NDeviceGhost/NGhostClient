#ifndef NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTDEVICESERVICETYPE_PROTECT
#define NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTDEVICESERVICETYPE_PROTECT

// -----------------------------------------------------------
// enum NGhostClient::Service::Device::NGhostDeviceServiceType
// -----------------------------------------------------------

typedef enum NGhostDeviceServiceType
{
	NGHOST_CLIENT_SERVICE_DEVICE_ROOT,

	NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_COUNT,
	NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_UUID,
	NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_IP,
	NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_TYPE,
	NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_STATE,

	NGHOST_CLIENT_SERVICES_DEVICE
} NGhostDeviceServiceType;

/**
 * Get json key
 *
 * @param service
 * 		The service
 *
 * @return the json key
 */
const char *NGhostClient_Service_Device_NGhostDeviceServiceList_GetJsonKey( NGhostDeviceServiceType service );

/**
 * Get low level json key
 *
 * @param service
 * 		The service
 *
 * @return the json low level key
 */
const char *NGhostClient_Service_Device_NGhostDeviceServiceList_GetLowLevelJsonKey( NGhostDeviceServiceType service );

/**
 * Get service from devices/
 *
 * @param element
 * 		The element
 * @param cursor
 * 		The current cursor
 * @param isKeepPosition
 * 		Stay at current position?
 *
 * @return the service type
 */
NGhostDeviceServiceType NGhostClient_Service_Device_NGhostDeviceServiceList_FindService( const char *element,
	NU32 *cursor,
	NBOOL isKeepPosition );

#ifdef NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTDEVICESERVICETYPE_INTERNE

#define NGHOST_CLIENT_SERVICE_DEVICE_ROOT_KEY				"devices"

static const char NGhostDeviceServiceTypeJson[ NGHOST_CLIENT_SERVICES_DEVICE ][ 64 ] =
{
	NGHOST_CLIENT_SERVICE_DEVICE_ROOT_KEY,

	NGHOST_CLIENT_SERVICE_DEVICE_ROOT_KEY".count",
	NGHOST_CLIENT_SERVICE_DEVICE_ROOT_KEY".%d.uuid",
	NGHOST_CLIENT_SERVICE_DEVICE_ROOT_KEY".%d.ip",
	NGHOST_CLIENT_SERVICE_DEVICE_ROOT_KEY".%d.type",
	NGHOST_CLIENT_SERVICE_DEVICE_ROOT_KEY".%d.state"
};

static const char NGhostDeviceServiceTypeURI[ NGHOST_CLIENT_SERVICES_DEVICE ][ 64 ] =
{
	"",
	"",

	"uuid",
	"ip",
	"type",
	"state"
};
#endif // NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTDEVICESERVICETYPE_INTERNE

#endif // !NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTDEVICESERVICETYPE_PROTECT
