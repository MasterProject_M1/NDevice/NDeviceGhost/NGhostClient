#ifndef NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTREMOTEDEVICEAUTHENTICATIONSERVICE_PROTECT
#define NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTREMOTEDEVICEAUTHENTICATIONSERVICE_PROTECT

// ---------------------------------------------------------------------------
// enum NGhostClient::Service::Device::NGhostRemoteDeviceAuthenticationService
// ---------------------------------------------------------------------------

typedef enum NGhostRemoteDeviceAuthenticationService
{
	NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_ROOT,

	NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_HOSTNAME,
	NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_PORT,

	NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_DEVICE_TYPE,

	NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_IS_AUTHENTICATED,

	NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_LAST_ATTEMPT_TIME,
	NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_ATTEMPT_COUNT,

	NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_TOKEN,

	NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICES
} NGhostRemoteDeviceAuthenticationService;

/**
 * Get service
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostClient_Service_Device_NGhostRemoteDeviceAuthenticationService_GetServiceName( NGhostRemoteDeviceAuthenticationService serviceType );

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 *
 * @return the service type
 */
NGhostRemoteDeviceAuthenticationService NGhostClient_Service_Device_NGhostRemoteDeviceAuthenticationService_FindService( const char *requestedElement,
	NU32 *cursor );

#ifdef NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTREMOTEDEVICEAUTHENTICATIONSERVICE_INTERNE
static const char NGhostRemoteDeviceAuthenticationServiceURI[ NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICES ][ 32 ] =
{
	"",

	"hostname",
	"port",

	"deviceType",

	"isAuthenticated",

	"lastAttempTime",
	"retryCount",

	"token"
};
#endif // NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTREMOTEDEVICEAUTHENTICATIONSERVICE_INTERNE

#endif // !NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTREMOTEDEVICEAUTHENTICATIONSERVICE_PROTECT
