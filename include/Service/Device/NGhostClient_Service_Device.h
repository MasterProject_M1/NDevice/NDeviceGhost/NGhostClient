#ifndef NGHOSTCLIENT_SERVICE_DEVICE_PROTECT
#define NGHOSTCLIENT_SERVICE_DEVICE_PROTECT

// ---------------------------------------
// namespace NGhostClient::Service::Device
// ---------------------------------------

// enum NGhostClient::Service::Device::NGhostDeviceServiceType
#include "NGhostClient_Service_Device_NGhostDeviceServiceType.h"

// enum NGhostClient::Service::Device::NGhostRemoteDeviceService
#include "NGhostClient_Service_Device_NGhostRemoteDeviceService.h"

// enum NGhostClient::Service::Device::NGhostRemoteDeviceAuthenticationService
#include "NGhostClient_Service_Device_NGhostRemoteDeviceAuthenticationService.h"

// namespace NGhostClient::Service::Device::Proxy
#include "Proxy/NGhostClient_Service_Device_Proxy.h"

/**
 * Build all devices parser output list
 *
 * @param deviceContainerList
 * 		The device container list
 * @param outputList
 * 		The output list
 *
 * @return the parser output list
 */
NBOOL NGhostClient_Service_Device_BuildParserOutputListAllDevice( const NGhostDeviceContainerList *deviceContainerList,
	__OUTPUT NParserOutputList *outputList );

/**
 * Build all devices response packet
 *
 * @param deviceContainerList
 * 		The device container list
 * @param clientID
 * 		The requesting client ID
 * @param requestingIP
 * 		The requesting IP
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostClient_Service_Device_BuildHTTPResponseDescribeAllDevice( const NGhostDeviceContainerList *deviceContainerList,
	NU32 clientID,
	const char *requestingIP );

/**
 * Process a GET REST request
 *
 * @param clientID
 * 		The client ID
 * @param requestingIP
 * 		The requesting IP
 * @param request
 * 		The http request
 * @param ghostDeviceContainerList
 * 		The ghost device container list
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostClient_Service_Device_ProcessRESTGetRequest( NU32 clientID,
	const char *requestingIP,
	const NRequeteHTTP *request,
	const NGhostDeviceContainerList *ghostDeviceContainerList );

/**
 * Process a PUT REST request
 *
 * @param clientID
 * 		The client ID
 * @param requestingIP
 * 		The requesting IP
 * @param request
 * 		The http request
 * @param ghostDeviceContainerList
 * 		The ghost device container list
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostClient_Service_Device_ProcessRESTPUTRequest( NU32 clientID,
	const char *requestingIP,
	const NRequeteHTTP *request,
	const NGhostDeviceContainerList *ghostDeviceContainerList );

/**
 * Process a POST REST request
 *
 * @param clientID
 * 		The client ID
 * @param requestingIP
 * 		The requesting ip
 * @param request
 * 		The http request
 * @param ghostDeviceContainerList
 * 		The ghost device container list
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostClient_Service_Device_ProcessRESTPOSTRequest( NU32 clientID,
	const char *requestingIP,
	const NRequeteHTTP *request,
	const NGhostDeviceContainerList *ghostDeviceContainerList );

#endif // !NGHOSTCLIENT_SERVICE_DEVICE_PROTECT
