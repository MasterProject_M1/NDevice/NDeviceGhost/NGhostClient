#ifndef NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTREMOTEDEVICESERVICE_PROTECT
#define NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTREMOTEDEVICESERVICE_PROTECT

// -------------------------------------------------------------
// enum NGhostClient::Service::Device::NGhostRemoteDeviceService
// -------------------------------------------------------------

typedef enum NGhostRemoteDeviceService
{
	NGHOST_REMOTE_DEVICE_SERVICE_ROOT,

	NGHOST_REMOTE_DEVICE_SERVICE_DEVICE_TYPE,

	NGHOST_REMOTE_DEVICE_SERVICE_AUTHENTICATION,
	NGHOST_REMOTE_DEVICE_SERVICE_DEVICE,

	NGHOST_REMOTE_DEVICE_SERVICES
} NGhostRemoteDeviceService;

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostClient_Service_Device_NGhostRemoteDeviceService_GetServiceName( NGhostRemoteDeviceService serviceType );

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostRemoteDeviceService NGhostClient_Service_Device_NGhostRemoteDeviceService_FindService( const char *requestedElement,
	NU32 *cursor );

#ifdef NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTREMOTEDEVICESERVICE_INTERNE
static const char NGhostRemoteDeviceServiceURI[ NGHOST_REMOTE_DEVICE_SERVICES ][ 64 ] =
{
	"",

	"deviceType",

	"authentication",
	"device"
};
#endif // NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTREMOTEDEVICESERVICE_INTERNE

#endif // !NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTREMOTEDEVICESERVICE_PROTECT
