#ifndef NGHOSTCLIENT_SERVICE_DEVICE_PROXY_PROTECT
#define NGHOSTCLIENT_SERVICE_DEVICE_PROXY_PROTECT

// ----------------------------------------------
// namespace NGhostClient::Service::Device::Proxy
// ----------------------------------------------

// namespace NGhostClient::Service::Device::Proxy::Audio
#include "Audio/NGhostClient_Service_Device_Proxy_Audio.h"

// namespace NGhostClient::Service::Device::Proxy::DoorOpener
#include "DoorOpener/NGhostClient_Service_Device_Proxy_DoorOpener.h"

#endif // !NGHOSTCLIENT_SERVICE_DEVICE_PROXY_PROTECT
