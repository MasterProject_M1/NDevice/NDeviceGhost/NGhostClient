#ifndef NGHOSTCLIENT_SERVICE_DEVICE_PROXY_AUDIO_NGHOSTCLIENTPROXYAUDIOSERVICE_PROTECT
#define NGHOSTCLIENT_SERVICE_DEVICE_PROXY_AUDIO_NGHOSTCLIENTPROXYAUDIOSERVICE_PROTECT

// -------------------------------------------------------------------------------
// enum NGhostClient::Service::Device::Proxy::Audio::NGhostClientProxyAudioService
// -------------------------------------------------------------------------------

typedef enum NGhostClientProxyAudioService
{
	NGHOST_CLIENT_PROXY_AUDIO_SERVICE_START,
	NGHOST_CLIENT_PROXY_AUDIO_SERVICE_STOP,

	NGHOST_CLIENT_PROXY_AUDIO_SERVICES
} NGhostClientProxyAudioService;

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service
 */
NGhostClientProxyAudioService NGhostClient_Service_Device_Proxy_Audio_NGhostClientProxyAudioService_FindService( const char *requestedElement,
	NU32 *cursor );

#ifdef NGHOSTCLIENT_SERVICE_DEVICE_PROXY_AUDIO_NGHOSTCLIENTPROXYAUDIOSERVICE_INTERNE
__PRIVATE const char NGhostClientProxyAudioServiceName[ NGHOST_CLIENT_PROXY_AUDIO_SERVICES ][ 64 ] =
{
	"start",
	"stop"
};
#endif // NGHOSTCLIENT_SERVICE_DEVICE_PROXY_AUDIO_NGHOSTCLIENTPROXYAUDIOSERVICE_INTERNE

#endif // !NGHOSTCLIENT_SERVICE_DEVICE_PROXY_AUDIO_NGHOSTCLIENTPROXYAUDIOSERVICE_PROTECT
