#ifndef NGHOSTCLIENT_SERVICE_DEVICE_PROXY_AUDIO_PROTECT
#define NGHOSTCLIENT_SERVICE_DEVICE_PROXY_AUDIO_PROTECT

// -----------------------------------------------------
// namespace NGhostClient::Service::Device::Proxy::Audio
// -----------------------------------------------------

// enum NGhostClient::Service::Device::Proxy::Audio::NGhostClientProxyAudioService
#include "NGhostClient_Service_Device_Proxy_Audio_NGhostClientProxyAudioService.h"

// enum NGhostClient::Service::Device::Proxy::Audio::NGhostClientAudioProxyParameter
#include "NGhostClient_Service_Device_Proxy_Audio_NGhostClientAudioProxyParameter.h"

/**
 * Process a start request
 *
 * @param remoteDevice
 * 		The remote device
 * @param userData
 * 		The user data
 *
 * @return if the operation succeeded
 */
NBOOL NGhostClient_Service_Device_Proxy_Audio_ProcessStartRequest( NGhostRemoteDevice *remoteDevice,
	const NParserOutputList *userData );

#endif // !NGHOSTCLIENT_SERVICE_DEVICE_PROXY_AUDIO_PROTECT
