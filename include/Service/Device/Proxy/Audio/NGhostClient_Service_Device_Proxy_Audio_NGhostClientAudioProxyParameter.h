#ifndef NGHOSTCLIENT_SERVICE_DEVICE_PROXY_AUDIO_NGHOSTCLIENTAUDIOPROXYPARAMETER_PROTECT
#define NGHOSTCLIENT_SERVICE_DEVICE_PROXY_AUDIO_NGHOSTCLIENTAUDIOPROXYPARAMETER_PROTECT

// ---------------------------------------------------------------------------------
// enum NGhostClient::Service::Device::Proxy::Audio::NGhostClientAudioProxyParameter
// ---------------------------------------------------------------------------------

typedef enum NGhostClientAudioProxyParameter
{
	NGHOST_CLIENT_AUDIO_PROXY_PARAMETER_NAME,
	NGHOST_CLIENT_AUDIO_PROXY_PARAMETER_HOSTNAME,
	NGHOST_CLIENT_AUDIO_PROXY_PARAMETER_PORT,
	NGHOST_CLIENT_AUDIO_PROXY_PARAMETER_FILE_PATH,
	NGHOST_CLIENT_AUDIO_PROXY_PARAMETER_IS_LOOP,
	NGHOST_CLIENT_AUDIO_PROXY_PARAMETER_VOLUME,
	NGHOST_CLIENT_AUDIO_PROXY_PARAMETER_SYNCH_LIST,

	NGHOST_CLIENT_AUDIO_PROXY_PARAMETERS
} NGhostClientAudioProxyParameter;

/**
 * Get parameter name
 *
 * @param parameter
 * 		The parameter
 *
 * @return the parameter name
 */
const char *NGhostClient_Service_Device_Proxy_Audio_NGhostClientAudioProxyParameter_GetName( NGhostClientAudioProxyParameter parameter );

#ifdef NGHOSTCLIENT_SERVICE_DEVICE_PROXY_AUDIO_NGHOSTCLIENTAUDIOPROXYPARAMETER_INTERNE
__PRIVATE const char NGhostClientAudioProxyParameterName[ NGHOST_CLIENT_AUDIO_PROXY_PARAMETERS ][ 32 ] =
{
	"track.name",
	"track.hostname",
	"track.port",
	"track.path",
	"isLoop",
	"volume",
	"synch"
};
#endif // NGHOSTCLIENT_SERVICE_DEVICE_PROXY_AUDIO_NGHOSTCLIENTAUDIOPROXYPARAMETER_INTERNE

#endif // !NGHOSTCLIENT_SERVICE_DEVICE_PROXY_AUDIO_NGHOSTCLIENTAUDIOPROXYPARAMETER_PROTECT
