#ifndef NGHOSTCLIENT_SERVICE_DEVICE_PROXY_DOOROPENER_NGHOSTDOOROPENERPROXYSERVICE_PROTECT
#define NGHOSTCLIENT_SERVICE_DEVICE_PROXY_DOOROPENER_NGHOSTDOOROPENERPROXYSERVICE_PROTECT

// -----------------------------------------------------------------------------------
// enum NGhostClient::Service::Device::Proxy::DoorOpener::NGhostDoorOpenerProxyService
// -----------------------------------------------------------------------------------

typedef enum NGhostDoorOpenerProxyService
{
	NGHOST_DOOR_OPENER_PROXY_SERVICE_OPEN,

	NGHOST_DOOR_OPENER_PROXY_SERVICES
} NGhostDoorOpenerProxyService;

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 *
 * @return the service type or NGHOST_DOOR_OPENER_PROXY_SERVICES
 */
NGhostDoorOpenerProxyService NGhostClient_Service_Device_Proxy_DoorOpener_NGhostDoorOpenerProxyService_FindService( const char *requestedElement,
	NU32 *cursor );

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostClient_Service_Device_Proxy_DoorOpener_NGhostDoorOpenerProxyService_GetName( NGhostDoorOpenerProxyService serviceType );

#ifdef NGHOSTCLIENT_SERVICE_DEVICE_PROXY_DOOROPENER_NGHOSTDOOROPENERPROXYSERVICE_INTERNE
__PRIVATE const char NGhostDoorOpenerProxyServiceName[ NGHOST_DOOR_OPENER_PROXY_SERVICES ][ 32 ] =
{
	"open"
};
#endif // NGHOSTCLIENT_SERVICE_DEVICE_PROXY_DOOROPENER_NGHOSTDOOROPENERPROXYSERVICE_INTERNE

#endif // !NGHOSTCLIENT_SERVICE_DEVICE_PROXY_DOOROPENER_NGHOSTDOOROPENERPROXYSERVICE_PROTECT
