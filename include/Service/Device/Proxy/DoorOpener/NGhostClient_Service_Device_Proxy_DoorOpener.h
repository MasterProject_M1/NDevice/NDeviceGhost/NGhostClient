#ifndef NGHOSTCLIENT_SERVICE_DEVICE_PROXY_DOOROPENER_PROTECT
#define NGHOSTCLIENT_SERVICE_DEVICE_PROXY_DOOROPENER_PROTECT

// ----------------------------------------------------------
// namespace NGhostClient::Service::Device::Proxy::DoorOpener
// ----------------------------------------------------------

// enum NGhostClient::Service::Device::Proxy::DoorOpener::NGhostDoorOpenerProxyService
#include "NGhostClient_Service_Device_Proxy_DoorOpener_NGhostDoorOpenerProxyService.h"

/**
 * Process door opening
 *
 * @param deviceContainer
 * 		The device container
 *
 * @return if the operation succeeded
 */
NBOOL NGhostClient_Service_Device_Proxy_DoorOpener_ProcessDoorOpening( const NGhostDeviceContainer *deviceContainer );

#endif // !NGHOSTCLIENT_SERVICE_DEVICE_PROXY_DOOROPENER_PROTECT
