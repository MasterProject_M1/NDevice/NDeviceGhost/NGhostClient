#ifndef NGHOSTCLIENT_SERVICE_NGHOSTSERVICETYPE_PROTECT
#define NGHOSTCLIENT_SERVICE_NGHOSTSERVICETYPE_PROTECT

// ---------------------------------------------
// enum NGhostClient::Service::NGhostServiceType
// ---------------------------------------------

typedef enum NGhostServiceType
{
	NGHOST_SERVICE_TYPE_LIST_ALL_DEVICE,

	// Scanner
	NGHOST_SERVICE_TYPE_LAUNCH_SCAN_FOR_DEVICE,
	NGHOST_SERVICE_TYPE_GET_SCAN_FOR_DEVICE_STATE,
	NGHOST_SERVICE_TYPE_STOP_SCAN_FOR_DEVICE,

	NGHOST_SERVICE_TYPE_GET_DEVICE_ROOT,
	NGHOST_SERVICE_TYPE_PUT_DEVICE_ROOT,
	NGHOST_SERVICE_TYPE_POST_DEVICE_ROOT,

	NGHOST_SERVICE_TYPES
} NGhostServiceType;

/**
 * Find service type
 *
 * @param apiElement
 * 		The requested element
 * @param httpMethod
 * 		The http method
 *
 * @return the service type or NGHOST_SERVICE_TYPES if not in known list
 */
NGhostServiceType NGhostClient_Service_NGhostServiceType_FindServiceType( const char *apiElement,
	NTypeRequeteHTTP httpMethod );

/**
 * Get api element
 *
 * @param type
 * 		The service type
 *
 * @return the api element
 */
const char *NGhostClient_Service_NGhostServiceType_GetAPIElement( NGhostServiceType type );

/**
 * Get api method
 *
 * @param type
 * 		The service type
 *
 * @return the api method
 */
NTypeRequeteHTTP NGhostClient_Service_NGhostServiceType_GetAPIMethod( NGhostServiceType type );

#ifdef NGHOSTCLIENT_SERVICE_NGHOSTSERVICETYPE_INTERNE
// Service api access
static const char NGhostServiceTypeAPI[ NGHOST_SERVICE_TYPES ][ 64 ] =
{
	"/ghost/devices",
	"/ghost/devices",
	"/ghost/devices",
	"/ghost/devices",

	"/ghost/devices/",
	"/ghost/devices/",
	"/ghost/devices/",
};

// Service api access method
static const NTypeRequeteHTTP NGhostServiceTypeAPIMethod[ NGHOST_SERVICE_TYPES ] =
{
	NTYPE_REQUETE_HTTP_GET,
	NTYPE_REQUETE_HTTP_POST,
	NTYPE_REQUETE_HTTP_TRACE,
	NTYPE_REQUETE_HTTP_DELETE,

	NTYPE_REQUETE_HTTP_GET,
	NTYPE_REQUETE_HTTP_PUT,
	NTYPE_REQUETE_HTTP_POST,
};

// Is API uri root for deeper elements?
static const NBOOL NGhostServiceURIAPIIsRoot[ NGHOST_SERVICE_TYPES ] =
{
	NFALSE,
	NFALSE,
	NFALSE,
	NFALSE,

	NTRUE,
	NTRUE,
	NTRUE
};
#endif // NGHOSTCLIENT_SERVICE_NGHOSTSERVICETYPE_INTERNE

#endif // !NGHOSTCLIENT_SERVICE_NGHOSTSERVICETYPE_PROTECT

