#ifndef NGHOSTCLIENT_SERVICE_PROTECT
#define NGHOSTCLIENT_SERVICE_PROTECT

// -------------------------------
// namespace NGhostClient::Service
// -------------------------------

// enum NGhostClient::Service::NGhostServiceType
#include "NGhostClient_Service_NGhostServiceType.h"

// namespace NGhostClient::Service::Scanner
#include "Scanner/NGhostClient_Service_Scanner.h"

// namespace NGhostClient::Service::Device
#include "Device/NGhostClient_Service_Device.h"

/**
 * Build http response with parser output list
 *
 * @param httpResponseCode
 * 		The http response code
 * @param parserOutputList
 * 		The parser output list
 * @param clientID
 * 		The client ID
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostClient_Service_GenerateJsonHTTPResponseWithParserOutputList( NHTTPCode httpResponseCode,
	__WILLBEOWNED NParserOutputList *parserOutputList,
	NU32 clientID );

/**
 * Process an HTTP request
 *
 * @param request
 * 		The http request
 * @param client
 * 		The requesting client
 *
 * @return the HTTP response
 */
__ALLOC NReponseHTTP *NGhostClient_Service_ProcessRequest( const NRequeteHTTP *request,
	const NClientServeur *client );

#endif // !NGHOSTCLIENT_SERVICE_PROTECT
