#ifndef NGHOSTCLIENT_SERVICE_SCANNER_PROTECT
#define NGHOSTCLIENT_SERVICE_SCANNER_PROTECT

// ----------------------------------------
// namespace NGhostClient::Service::Scanner
// ----------------------------------------

// struct NGhostClient::Service::Scanner::NGhostClientScannerService
#include "NGhostClient_Service_Scanner_NGhostClientScannerService.h"

#define NGHOST_CLIENT_SERVICE_SCANNER_THREAD_COUNT						"scanner.threadCount"
#define NGHOST_CLIENT_SERVICE_SCANNER_CONNECTION_TIMEOUT				"scanner.connectionTimeout"
#define NGHOST_CLIENT_SERVICE_SCANNER_REQUEST_TIMEOUT					"scanner.requestTimeout"
#define NGHOST_CLIENT_SERVICE_SCANNER_SCANNED_INTERFACE					"scanner.scannedInterface"
#define NGHOST_CLIENT_SERVICE_SCANNER_FILTERED_TYPE						"scanner.filter.filteredType"
#define NGHOST_CLIENT_SERVICE_SCANNER_FILTERED_UUID						"scanner.filter.filteredUUID"
#define NGHOST_CLIENT_SERVICE_SCANNER_START_TIMESTAMP					"scanner.state.startingTimestamp"
#define NGHOST_CLIENT_SERVICE_SCANNER_IS_RUNNING						"scanner.state.isRunning"
#define NGHOST_CLIENT_SERVICE_SCANNER_CURRENT_PROCESSED_INTERFACE		"scanner.state.processedInterface"
#define NGHOST_CLIENT_SERVICE_SCANNER_CURRENT_IP_SCANNED_COUNT			"scanner.state.ipScannedCount"
#define NGHOST_CLIENT_SERVICE_SCANNER_CURRENT_TOTAL_IP_TO_SCAN_COUNT	"scanner.state.totalIpCount"
#define NGHOST_CLIENT_SERVICE_SCANNER_CURRENT_PERCENT_PROGRESS			"scanner.state.progress"
#define NGHOST_CLIENT_SERVICE_SCANNER_CURRENT_FOUND_DEVICES_COUNT		"scanner.state.foundDeviceCount"
#define NGHOST_CLIENT_SERVICE_SCANNER_TIME_TAKEN						"scanner.state.timeTaken"

#define NGHOST_CLIENT_SERVICE_SCANNER_SUBKEY_INTERFACE_NAME				"name"
#define NGHOST_CLIENT_SERVICE_SCANNER_SUBKEY_INTERFACE_IP				"ipAddress"
#define NGHOST_CLIENT_SERVICE_SCANNER_SUBKEY_INTERFACE_CIDR				"cidr"
#define NGHOST_CLIENT_SERVICE_SCANNER_SUBKEY_INTERFACE_MAC				"macAddress"

/**
 * Generate parser output list
 *
 * @param configuration
 * 		The ghost client configuration
 * @param scanner
 * 		The ghost scanner service
 * @param parserOutputList
 * 		The parser output list
 *
 * @return the parser output list
 */
NBOOL NGhostClient_Service_Scanner_GenerateScannerParserOutputList( const NGhostClientConfiguration *configuration,
	const NGhostClientScannerService *scanner,
	__OUTPUT NParserOutputList *parserOutputList );

/**
 * Build scanning started response
 *
 * @param responseCode
 * 		The response code
 * @param clientID
 * 		The client ID
 * @param configuration
 * 		The ghost client configuration
 * @param scanner
 * 		The scanner service
 *
 * @return the response
 */
__ALLOC NReponseHTTP *NGhostClient_Service_Scanner_BuildScannerStartResponse( NHTTPCode responseCode,
	NU32 clientID,
	const NGhostClientConfiguration *configuration,
	const NGhostClientScannerService *scanner );

#endif // !NGHOSTCLIENT_SERVICE_SCANNER_PROTECT

