#ifndef NGHOSTCLIENT_SERVICE_SCANNER_NGHOSTCLIENTSCANNERSERVICE_PROTECT
#define NGHOSTCLIENT_SERVICE_SCANNER_NGHOSTCLIENTSCANNERSERVICE_PROTECT

// -----------------------------------------------------------------
// struct NGhostClient::Service::Scanner::NGhostClientScannerService
// -----------------------------------------------------------------

typedef struct NGhostClientScannerService
{
	// Mutex
	NMutex *m_mutex;

	// Client
	NGhostClient *m_client;

	// Client configuration
	const NGhostClientConfiguration *m_configuration;

	// Is currently running?
	NBOOL m_isRunning;

	// Is scan operating?
	NBOOL m_isScanOperating;

	// Scanning thread
	NThread *m_scanningThread;

	// Interface scan must be run on (NListe<NInterfaceReseau*>)
	NListe *m_interfaceScanList;

	// Scan timestamp
	NU64 m_startTimestamp;
	NU64 m_endTimestamp;

	// Total count ip to scan
	NU64 m_totalIPToScanCount;

	// Current scanned ip count
	NU64 m_currentScanIPCount;

	// Current found device count
	NU32 m_currentFoundDeviceCount;

	// Current interface being scanned
	const char *m_currentScanInterface;
} NGhostClientScannerService;

/**
 * Build scanner service
 *
 * @param ghostClient
 * 		The ghost client
 *
 * @return the scanner instance
 */
__ALLOC NGhostClientScannerService *NGhostClient_Service_Scanner_NGhostClientScannerService_Build( NGhostClient *ghostClient );

/**
 * Destroy scanner service
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Service_Scanner_NGhostClientScannerService_Destroy( NGhostClientScannerService** );

/**
 * Run scan
 *
 * @param this
 * 		This instance
 *
 * @return if the scan run successfully
 */
__WILLLOCK __WILLUNLOCK NBOOL NGhostClient_Service_Scanner_NGhostClientScannerService_RunScan( NGhostClientScannerService* );

/**
 * Stop scan
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Service_Scanner_NGhostClientScannerService_StopScan( NGhostClientScannerService* );

/**
 * Get starting timestamp
 *
 * @param this
 * 		This instance
 *
 * @return the starting timestamp
 */
NU64 NGhostClient_Service_Scanner_NGhostClientScannerService_GetStartingTimestamp( const NGhostClientScannerService* );

/**
 * Get ending timestamp
 *
 * @param this
 * 		This instance
 *
 * @return the ending timestamp
 */
NU64 NGhostClient_Service_Scaner_NGhostClientScannerService_GetEndingTimestamp( const NGhostClientScannerService* );

/**
 * Is scanner currently running?
 *
 * @param this
 * 		This instance
 *
 * @return if the scanner is currently running
 */
NBOOL NGhostClient_Service_Scanner_NGhostClientScannerService_IsRunning( const NGhostClientScannerService* );

/**
 * Is scanner operating?
 *
 * @param this
 * 		This instance
 *
 * @return if the scanner is operating
 */
NBOOL NGhostClient_Service_Scanner_NGhostClientScannerService_IsOperating( const NGhostClientScannerService* );

/**
 * Get scanned interfaces list
 *
 * @param this
 * 		This instance
 *
 * @return the scanned interfaces list
 */
const NListe *NGhostClient_Service_Scanner_NGhostClientScannerService_GetScanInterfaceList( const NGhostClientScannerService* );

/**
 * Get total ip to scan count
 *
 * @param this
 * 		This instance
 *
 * @return the total count of ip to scan
 */
NU64 NGhostClient_Service_Scanner_NGhostClientScannerService_GetTotalIPToScanCount( const NGhostClientScannerService* );

/**
 * Get ip already scan count
 *
 * @param this
 * 		This instance
 *
 * @return the count of ip already scanned
 */
NU64 NGhostClient_Service_Scanner_NGhostClientScannerService_GetAlreadyScanIPCount( const NGhostClientScannerService* );

/**
 * Get current interface being processed
 *
 * @param this
 * 		This instance
 *
 * @return the current interface being processed
 */
const char *NGhostClient_Service_Scanner_NGhostClientScannerService_GetCurrentInterfaceScan( const NGhostClientScannerService* );

/**
 * Get found devices count
 *
 * @param this
 * 		This instance
 *
 * @return the current found devices count
 */
NU32 NGhostClient_Service_Scanner_NGhostClientScannerService_GetFoundDeviceCount( const NGhostClientScannerService* );

#endif // !NGHOSTCLIENT_SERVICE_SCANNER_NGHOSTCLIENTSCANNERSERVICE_PROTECT
