#include "../../../include/NGhostClient.h"

// -------------------------------------------------------------------------------------
// struct NGhostClient::Configuration::Network::NGhostClientConfigurationInterfaceFilter
// -------------------------------------------------------------------------------------

/**
 * Build interface filter
 *
 * @param configText
 * 		The config text loaded from file
 *
 * @return the filter instance
 */
__ALLOC NGhostClientConfigurationInterfaceFilter *NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Build( const char *configText )
{
	// Output
	__OUTPUT NGhostClientConfigurationInterfaceFilter *out;

	// Buffer
	char *buffer;

	// Cursor
	NU32 cursor = 0;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostClientConfigurationInterfaceFilter ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Create filter
	if( !( out->m_interfaceList = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NLib_Memoire_Liberer ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Read interfaces
	while( ( buffer = NLib_Chaine_LireJusqua( configText,
		',',
		&cursor,
		NFALSE ) ) != NULL )
		if( strlen( buffer ) > 0 )
		{
			// Remove first spaces
			NLib_Chaine_SupprimerCaractereDebut( buffer,
				' ' );

			// Add
			NLib_Memoire_NListe_Ajouter( out->m_interfaceList,
				buffer );
		}
		else
		{
			// Free
			NFREE( buffer );

			// Quit
			break;
		}

	// OK
	return out;
}

/**
 * Destroy interface filter
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Destroy( NGhostClientConfigurationInterfaceFilter **this )
{
	// Destroy filter
	NLib_Memoire_NListe_Detruire( &(*this)->m_interfaceList );

	// Free
	NFREE( (*this) );
}

/**
 * Is interface authorized?
 *
 * @param this
 * 		This instance
 * @param interfaceName
 * 		The interface name to check
 *
 * @return if the interface is authorized
 */
NBOOL NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_IsInterfaceAuthorized( const NGhostClientConfigurationInterfaceFilter *this,
	const char *interfaceName )
{
	// Iterator
	NU32 i = 0;

	// Interface
	const char *currentInterfaceName;

	// Look for
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_interfaceList ); i++ )
		// Get interface
		if( ( currentInterfaceName = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_interfaceList,
			i ) ) != NULL )
			// Check
			if( NLib_Chaine_Comparer( currentInterfaceName,
				interfaceName,
				NTRUE,
				0 ) )
				// Found it, this interface is filtered
				return NFALSE;

	// Not found, not filtered
	return NTRUE;
}

/**
 * Export filter
 *
 * @param this
 * 		This instance
 *
 * @return an exported conf-compatible version of interface filter
 */
__ALLOC char *NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Export( const NGhostClientConfigurationInterfaceFilter *this )
{
	// Output
	__OUTPUT char *output = NULL;

	// Output size
	NU32 outputSize = 0;

	// Iterator
	NU32 i = 0;

	// Buffer
	char buffer[ 512 ];

	// Add interfaces
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_interfaceList ); i++ )
	{
		// Build string to add
		snprintf( buffer,
			512,
			"%s%s",
			(const char*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_interfaceList,
				i ),
			i + 1 < NLib_Memoire_NListe_ObtenirNombre( this->m_interfaceList ) ?
				", "
				: "" );

		// Add interface name
		NLib_Memoire_AjouterData2( &output,
			outputSize,
			buffer );

		// Increase size
		outputSize += strlen( buffer );
	}

	// OK
	return output;
}

