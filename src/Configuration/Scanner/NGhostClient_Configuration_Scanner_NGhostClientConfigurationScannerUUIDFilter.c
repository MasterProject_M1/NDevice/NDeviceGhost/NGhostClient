#include "../../../include/NGhostClient.h"

// ---------------------------------------------------------------------------------------
// struct NGhostClient::Configuration::Scanner::NGhostClientConfigurationScannerUUIDFilter
// ---------------------------------------------------------------------------------------

/**
 * Build uuid filter list
 *
 * @param configurationLine
 * 		The configuration line
 *
 * @return the instance
 */
__ALLOC NGhostClientConfigurationScannerUUIDFilter *NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_Build( const char *configurationLine )
{
	// Output
	__OUTPUT NGhostClientConfigurationScannerUUIDFilter *out;

	// Buffer
	char *buffer;

	// Cursor
	NU32 cursor = 0;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostClientConfigurationScannerUUIDFilter ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Create list
	if( !( out->m_uuid = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NLib_Memoire_Liberer ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Read line
	while( ( buffer = NLib_Chaine_LireJusqua( configurationLine,
		',',
		&cursor,
		NFALSE ) ) != NULL )
	{
		// Remove initial spaces
		NLib_Chaine_SupprimerCaractereDebut( buffer,
			' ' );

		// Check length
		if( strlen( buffer ) <= 0 )
		{
			// Free
			NFREE( buffer );

			// Quit
			break;
		}

		// Is it a valid UUID?
		if( NDeviceCommon_UUID_ConvertUUIDToNumber( buffer ) != (NU64)0xFFFFFFFFFFFFFFFF )
			// Add to list
			NLib_Memoire_NListe_Ajouter( out->m_uuid,
				buffer );
		// Not a valid UUID
		else
			NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );
	}

	// OK
	return out;
}

/**
 * Destroy uuid filter list
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_Destroy( NGhostClientConfigurationScannerUUIDFilter **this )
{
	// Destroy list
	NLib_Memoire_NListe_Detruire( &(*this)->m_uuid );

	// Free
	NFREE( (*this) );
}

/**
 * Get filtered uuid count
 *
 * @param this
 * 		This instance
 *
 * @return the filtered uuid count
 */
NU32 NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_GetFilteredUUIDCount( const NGhostClientConfigurationScannerUUIDFilter *this )
{
	return NLib_Memoire_NListe_ObtenirNombre( this->m_uuid );
}

/**
 * Get filtered uuid
 *
 * @param this
 * 		This instance
 * @param index
 * 		The filtered uuid index
 *
 * @return the filtered uuid
 */
const char *NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_GetFilteredUUID( const NGhostClientConfigurationScannerUUIDFilter *this,
	NU32 index )
{
	return NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_uuid,
		index );
}

/**
 * Is the uuid filtered?
 *
 * @param this
 * 		This instance
 * @param uuid
 * 		The uuid
 *
 * @return if the uuid is filtered
 */
NBOOL NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_IsUUIDFiltered( const NGhostClientConfigurationScannerUUIDFilter *this,
	const char *uuid )
{
	// Iterator
	NU32 i = 0;

	// Check if filtered
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_uuid ); i++ )
		// Check
		if( NLib_Chaine_Comparer( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_uuid,
				i ),
			uuid,
			NTRUE,
			0 ) )
			// Filtered
			return NTRUE;

	// Not filtered
	return NFALSE;
}

/**
 * Export uuid filter list
 *
 * @param this
 * 		This instance
 *
 * @return the configuration line associated to this instance
 */
__ALLOC char *NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_Export( const NGhostClientConfigurationScannerUUIDFilter *this )
{
	// Output
	__OUTPUT char *output = NULL;
	NU32 outputSize = 0;

	// Iterator
	NU32 i = 0;

	// Buffer
	char buffer[ 64 ];

	// Export
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_uuid ); i++ )
	{
		// Build part
		snprintf( buffer,
			64,
			"%s%s",
			(char*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_uuid,
				i ),
			( i + 1 >= NLib_Memoire_NListe_ObtenirNombre( this->m_uuid ) ) ?
				""
				: ", " );

		// Add to output
		NLib_Memoire_AjouterData2( &output,
			outputSize,
			buffer );

		// Increase length
		outputSize += strlen( buffer );
	}

	// OK
	return output;
}

