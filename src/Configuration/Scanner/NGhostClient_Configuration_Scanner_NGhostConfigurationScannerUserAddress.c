#include "../../../include/NGhostClient.h"

// ----------------------------------------------------------------------------------
// struct NGhostClient::Configuration::Scanner::NGhostConfigurationScannerUserAddress
// ----------------------------------------------------------------------------------

/**
 * Build user address
 *
 * @param configurationProperty
 * 		The configuration read property to parse (X.X.X.X/CIDR)
 *
 * @return the user address
 */
__ALLOC NGhostConfigurationScannerUserAddress *NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddress_Build( const char *configurationProperty )
{
	// Output
	__OUTPUT NGhostConfigurationScannerUserAddress *out;

	// Cursor
	NU32 cursor = 0;

	// Buffer
	char *buffer;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostConfigurationScannerUserAddress ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Read address
	if( !( out->m_address = NLib_Chaine_LireJusqua( configurationProperty,
		'/',
		&cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Read address
	if( !( buffer = NLib_Chaine_LireJusqua( configurationProperty,
		'\0',
		&cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Free
		NFREE( out->m_address );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Remove '/'
	NLib_Chaine_SupprimerCaractereDebut( buffer,
		'/' );

	// Convert CIDR
	out->m_cidr = (NU32)strtol( buffer,
		NULL,
		10 );

	// Free
	NFREE( buffer );

	// OK
	return out;
}

/**
 * Destroy user address
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddress_Destroy( NGhostConfigurationScannerUserAddress **this )
{
	// Free
	NFREE( (*this)->m_address );
	NFREE( (*this) );
}

/**
 * Get address
 *
 * @param this
 * 		This instance
 *
 * @return the address
 */
const char *NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddress_GetAddress( const NGhostConfigurationScannerUserAddress *this )
{
	return this->m_address;
}

/**
 * Get CIDR
 *
 * @param this
 * 		This instance
 *
 * @return the CIDR
 */
NU32 NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddress_GetCIDR( const NGhostConfigurationScannerUserAddress *this )
{
	return this->m_cidr;
}

/**
 * Export user address
 *
 * @param this
 * 		This instance
 *
 * @return the address with X.X.X.X/CIDR shape
 */
__ALLOC char *NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddress_Export( const NGhostConfigurationScannerUserAddress *this )
{
	// Output
	char *output;

	// Allocate memory
	if( !( output = calloc( NLIB_TAILLE_MAXIMALE_CHAINE_IP + 5,
		sizeof( char ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Fill with info
	snprintf( output,
		NLIB_TAILLE_MAXIMALE_CHAINE_IP + 5,
		"%s/%d",
		this->m_address,
		this->m_cidr );

	// OK
	return output;
}

