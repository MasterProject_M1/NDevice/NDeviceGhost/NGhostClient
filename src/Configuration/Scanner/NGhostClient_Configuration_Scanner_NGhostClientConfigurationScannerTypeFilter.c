#include "../../../include/NGhostClient.h"

// ---------------------------------------------------------------------------------------
// struct NGhostClient::Configuration::Scanner::NGhostClientConfigurationScannerTypeFilter
// ---------------------------------------------------------------------------------------

/**
 * Build filtered type
 *
 * @param configurationProperty
 * 		The configuration property
 *
 * @return the instance
 */
__ALLOC NGhostClientConfigurationScannerTypeFilter *NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_Build( const char *configurationProperty )
{
	// Output
	__OUTPUT NGhostClientConfigurationScannerTypeFilter *out;

	// Iterator
	NU32 i = 0;

	// Cursor
	NU32 cursor = 0;

	// Buffer
	char *buffer;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostClientConfigurationScannerTypeFilter ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Check if empty
	if( !strlen( configurationProperty )
		|| NLib_Chaine_EstVide( configurationProperty ) )
		// We're done
		return out;

	// Allocate memory
	if( !( out->m_filteredType = calloc( ( out->m_typeCount = NLib_Chaine_CompterNombreOccurence( configurationProperty,
			',' ) + 1 ),
		sizeof( NDeviceType ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Read filtered types
	for( i = 0; i < out->m_typeCount; i++ )
	{
		// Read filtered type
		if( !( buffer = NLib_Chaine_LireJusqua( configurationProperty,
			',',
			&cursor,
			NFALSE ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Free
			NFREE( out->m_filteredType );
			NFREE( out );

			// Quit
			return NULL;
		}

		// Remove initial spaces
		NLib_Chaine_SupprimerCaractereDebut( buffer,
			' ' );

		// Parse
		if( ( out->m_filteredType[ i ] = NDeviceCommon_Type_NDeviceType_FindTypeFromName( buffer ) ) >= NDEVICES_TYPE )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Free
			NFREE( buffer );
			NFREE( out->m_filteredType );
			NFREE( out );

			// Quit
			return NULL;
		}

		// Free
		NFREE( buffer );
	}

	// OK
	return out;
}

/**
 * Destroy filtered type
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_Destroy( NGhostClientConfigurationScannerTypeFilter **this )
{
	// Free
	NFREE( (*this)->m_filteredType );
	NFREE( (*this) );
}

/**
 * Get filtered types count
 *
 * @param this
 * 		This instance
 *
 * @return the filtered types count
 */
NU32 NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_GetFilteredTypeCount( const NGhostClientConfigurationScannerTypeFilter *this )
{
	return this->m_typeCount;
}

/**
 * Get filtered type
 *
 * @param this
 * 		This instance
 * @param index
 * 		The type index
 *
 * @return the filtered type
 */
NDeviceType NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_GetFilteredType( const NGhostClientConfigurationScannerTypeFilter *this,
	NU32 index )
{
	return this->m_filteredType[ index ];
}

/**
 * Is filtered?
 *
 * @param this
 * 		This instance
 * @param deviceType
 * 		The device type
 *
 * @return if the type is filtered
 */
NBOOL NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_IsFiltered( const NGhostClientConfigurationScannerTypeFilter *this,
	NDeviceType deviceType )
{
	// Iterator
	NU32 i = 0;

	// Look for
	for( ; i < this->m_typeCount; i++ )
		// Check
		if( this->m_filteredType[ i ] == deviceType )
			// Filtered
			return NTRUE;

	// Not filtered
	return NFALSE;
}

/**
 * Export to configuration line
 *
 * @param this
 * 		This instance
 *
 * @return the exported data
 */
__ALLOC char *NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_Export( const NGhostClientConfigurationScannerTypeFilter *this )
{
	// Output
	char *output = NULL;
	NU32 outputSize = 0;

	// Buffer
	char buffer[ 64 ];

	// Iterator
	NDeviceType i = (NDeviceType)0;

	// Add filter
	for( ; i < this->m_typeCount; i++ )
	{
		// Write in buffer
		snprintf( buffer,
			64,
			"%s%s",
			NDeviceCommon_Type_NDeviceType_GetName( this->m_filteredType[ i ] ),
			i + 1 >= this->m_typeCount ?
				""
				: ", " );

		// Add
		NLib_Memoire_AjouterData2( &output,
			outputSize,
			buffer );

		// Increment size
		outputSize += strlen( buffer );
	}

	// OK
	return output;
}
