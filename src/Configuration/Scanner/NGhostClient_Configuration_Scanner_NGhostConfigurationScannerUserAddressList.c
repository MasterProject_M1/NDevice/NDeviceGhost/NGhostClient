#include "../../../include/NGhostClient.h"

// --------------------------------------------------------------------------------------
// struct NGhostClient::Configuration::Scanner::NGhostConfigurationScannerUserAddressList
// --------------------------------------------------------------------------------------

/**
 * Build user addresses list
 *
 * @param configurationLine
 * 		The configuration line
 *
 * @return the instance
 */
__ALLOC NGhostConfigurationScannerUserAddressList *NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_Build( const char *configurationLine )
{
	// Output
	__OUTPUT NGhostConfigurationScannerUserAddressList *out;

	// Buffer
	char *buffer;

	// User address
	NGhostConfigurationScannerUserAddress *address;

	// Cursor
	NU32 cursor = 0;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostConfigurationScannerUserAddressList ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Create list
	if( !( out->m_userAddressList = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddress_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Read addresses
	while( ( buffer = NLib_Chaine_LireJusqua( configurationLine,
		',',
		&cursor,
		NFALSE ) ) != NULL )
	{
		// Check size
		if( strlen( buffer ) > 0 )
		{
			// Remove spaces
			NLib_Chaine_SupprimerCaractereDebut( buffer,
				' ' );

			// Build user address
			if( ( address = NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddress_Build( buffer ) ) != NULL )
				// Add to list
				NLib_Memoire_NListe_Ajouter( out->m_userAddressList,
					address );

			// Free
			NFREE( buffer );
		}
		else
		{
			// Free
			NFREE( buffer );

			// Quit
			break;
		}
	}

	// OK
	return out;
}

/**
 * Destroy user addresses list
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_Destroy( NGhostConfigurationScannerUserAddressList **this )
{
	// Destroy list
	NLib_Memoire_NListe_Detruire( &(*this)->m_userAddressList );

	// Free
	NFREE( (*this) );
}

/**
 * Get the addresses count
 *
 * @param this
 * 		This instance
 *
 * @return the user address count
 */
NU32 NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_GetAddressCount( const NGhostConfigurationScannerUserAddressList *this )
{
	return NLib_Memoire_NListe_ObtenirNombre( this->m_userAddressList );
}

/**
 * Get the address
 *
 * @param this
 * 		This instance
 * @param index
 * 		The address index
 *
 * @return the user address
 */
const NGhostConfigurationScannerUserAddress *NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_GetAddress( const NGhostConfigurationScannerUserAddressList *this,
	NU32 index )
{
	return NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_userAddressList,
		index );
}

/**
 * Export user address list
 *
 * @param this
 * 		This instance
 *
 * @return the exported data
 */
__ALLOC char *NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_Export( const NGhostConfigurationScannerUserAddressList *this )
{
	// Output
	char *output= NULL;
	NU32 outputSize = 0;

	// Buffer
	char *buffer;

	// User address
	const NGhostConfigurationScannerUserAddress *userAddress;

	// Iterator
	NU32 i = 0;

	// Iterate
	for( ; i < NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_GetAddressCount( this ); i++ )
		// Get user address
		if( ( userAddress = NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_GetAddress( this,
			i ) ) != NULL )
			// Export
			if( ( buffer = NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddress_Export( userAddress ) ) != NULL )
			{
				// Add address
				NLib_Memoire_AjouterData2( &output,
					outputSize,
					buffer );

				// Increase
				outputSize += strlen( buffer );

				// Add separator
				if( i + 1 < NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_GetAddressCount( this ) )
				{
					NLib_Memoire_AjouterData2( &output,
						outputSize,
						", " );
					outputSize += strlen( ", " );
				}

				// Free
				NFREE( buffer );
			}

	// OK
	return output;
}

