#include "../../include/NGhostClient.h"

// -------------------------------------------------------------
// struct NGhostClient::Configuration::NGhostClientConfiguration
// -------------------------------------------------------------

/**
 * Load configuration
 *
 * @param configFile
 * 		The config file location
 *
 * @return the configuration instance
 */
__ALLOC NGhostClientConfiguration *NGhostClient_Configuration_NGhostClientConfiguration_Build( const char *configFile )
{
	// Output
	__OUTPUT NGhostClientConfiguration *out;

	// Config file
	NFichierClef *file;

	// Key set
	char **keySet;

	// Get key set
	if( !( keySet = NGhostClient_Configuration_NGhostClientConfigurationProperty_GetKeySet( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Quit
		return NULL;
	}

	// Load file
	if( !( file = NLib_Fichier_Clef_NFichierClef_Construire( configFile,
		(const char**)keySet,
		NGHOST_CLIENT_CONFIGURATION_PROPERTIES,
		NTRUE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( keySet );

		// Quit
		return NULL;
	}

	// Free
	NFREE( keySet );

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostClientConfiguration ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Destroy file
		NLib_Fichier_Clef_NFichierClef_Detruire( &file );

		// Quit
		return NULL;
	}

	// Read text properties
	if( !( out->m_authenticationUsername = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( file,
		NGHOST_CLIENT_CONFIGURATION_PROPERTY_AUTHENTICATION_USERNAME ) )
		|| !( out->m_authenticationPassword = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( file,
			NGHOST_CLIENT_CONFIGURATION_PROPERTY_AUTHENTICATION_PASSWORD ) )
		|| !( out->m_networkListeningInterface = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( file,
			NGHOST_CLIENT_CONFIGURATION_PROPERTY_NETWORK_LISTENING_INTERFACE ) )
		|| !( out->m_deviceName = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( file,
			NGHOST_CLIENT_CONFIGURATION_PROPERTY_DEVICE_NAME ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_FILE_KEY_NOT_FOUND );

		// Destroy file
		NLib_Fichier_Clef_NFichierClef_Detruire( &file );

		// Free
		NFREE( out->m_networkListeningInterface );
		NFREE( out->m_authenticationPassword );
		NFREE( out->m_authenticationUsername );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Boolean
	out->m_isAuthenticationManagerDataPersistent = NLib_Type_NBOOL_Parse( NLib_Fichier_Clef_NFichierClef_ObtenirValeur( file,
		NGHOST_CLIENT_CONFIGURATION_PROPERTY_IS_AUTHENTICATION_MANAGER_DATA_PERSISTENT ) );

	// Read numeric values
	out->m_networkListeningPort = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( file,
		NGHOST_CLIENT_CONFIGURATION_PROPERTY_NETWORK_LISTENING_PORT );
	out->m_scannerThreadCount = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( file,
		NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_THREAD_COUNT );
	out->m_scannerConnectionTimeout = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( file,
		NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_CONNECTION_TIMEOUT );
	out->m_scannerRequestTimeout = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( file,
		NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_REQUEST_TIMEOUT );

	// Build scan interface filter
	if( !( out->m_scannerInterfaceFilter = NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Build( NLib_Fichier_Clef_NFichierClef_ObtenirValeur( file,
		NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_INTERFACE_FILTER ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out->m_deviceName );
		NFREE( out->m_networkListeningInterface );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_authenticationPassword );
		NFREE( out );

		// Destroy file
		NLib_Fichier_Clef_NFichierClef_Detruire( &file );

		// Quit
		return NULL;
	}

	// Build scanner user address
	if( !( out->m_scannerUserAddress = NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_Build( NLib_Fichier_Clef_NFichierClef_ObtenirValeur( file,
		NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_USER_ADDRESS ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Destroy( &out->m_scannerInterfaceFilter );

		// Free
		NFREE( out->m_deviceName );
		NFREE( out->m_networkListeningInterface );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_authenticationPassword );
		NFREE( out );

		// Destroy file
		NLib_Fichier_Clef_NFichierClef_Detruire( &file );

		// Quit
		return NULL;
	}

	// Build scanner filtered types list
	if( !( out->m_scannerTypeFilter = NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_Build( NLib_Fichier_Clef_NFichierClef_ObtenirValeur( file,
		NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_TYPE_FILTER ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CANT_PROCESS_REQUEST );

		// Destroy
		NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Destroy( &out->m_scannerInterfaceFilter );
		NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_Destroy( &out->m_scannerUserAddress );

		// Destroy file
		NLib_Fichier_Clef_NFichierClef_Detruire( &file );

		// Free
		NFREE( out->m_deviceName );
		NFREE( out->m_networkListeningInterface );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_authenticationPassword );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build trusted token ip list
	if( !( out->m_networkTokenTrustedIPList = NGhostCommon_Token_NGhostTrustedTokenIPList_Build( NLib_Fichier_Clef_NFichierClef_ObtenirValeur( file,
		NGHOST_CLIENT_CONFIGURATION_PROPERTY_NETWORK_TOKEN_DELIVERY_TRUSTED_IP ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CANT_PROCESS_REQUEST );

		// Destroy file
		NLib_Fichier_Clef_NFichierClef_Detruire( &file );

		// Destroy
		NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_Destroy( &out->m_scannerTypeFilter );
		NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Destroy( &out->m_scannerInterfaceFilter );
		NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_Destroy( &out->m_scannerUserAddress );

		// Free
		NFREE( out->m_deviceName );
		NFREE( out->m_networkListeningInterface );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_authenticationPassword );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build uuid filter
	if( !( out->m_scannerUUIDFilter = NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_Build( NLib_Fichier_Clef_NFichierClef_ObtenirValeur( file,
		NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_UUID_FILTER ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CANT_PROCESS_REQUEST );

		// Destroy file
		NLib_Fichier_Clef_NFichierClef_Detruire( &file );

		// Destroy
		NGhostCommon_Token_NGhostTrustedTokenIPList_Destroy( &out->m_networkTokenTrustedIPList );
		NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_Destroy( &out->m_scannerTypeFilter );
		NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Destroy( &out->m_scannerInterfaceFilter );
		NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_Destroy( &out->m_scannerUserAddress );

		// Free
		NFREE( out->m_deviceName );
		NFREE( out->m_networkListeningInterface );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_authenticationPassword );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Destroy file
	NLib_Fichier_Clef_NFichierClef_Detruire( &file );

	// Build version
	NGhostClient_Helper_BuildVersionShort( out->m_otherInformationBuildInformation );

	// Read unique ID
	if( !( out->m_deviceUniqueID = NGhostCommon_Hardware_GetUniqueID( NDEVICE_TYPE_GHOST_CLIENT ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CANT_PROCESS_REQUEST );

		// Destroy
		NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_Destroy( &out->m_scannerUUIDFilter );
		NGhostCommon_Token_NGhostTrustedTokenIPList_Destroy( &out->m_networkTokenTrustedIPList );
		NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_Destroy( &out->m_scannerTypeFilter );
		NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Destroy( &out->m_scannerInterfaceFilter );
		NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_Destroy( &out->m_scannerUserAddress );

		// Free
		NFREE( out->m_deviceName );
		NFREE( out->m_networkListeningInterface );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_authenticationPassword );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Build default configuration
 *
 * @return the configuration instance
 */
__ALLOC NGhostClientConfiguration *NGhostClient_Configuration_NGhostClientConfiguration_Build2( void )
{
	// Output
	__OUTPUT NGhostClientConfiguration *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostClientConfiguration ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Duplicate default values
	if( !( out->m_authenticationUsername = NLib_Chaine_Dupliquer( NGHOST_CLIENT_CONFIGURATION_PROPERTY_AUTHENTICATION_DEFAULT_USERNAME ) )
		|| !( out->m_authenticationPassword = NLib_Chaine_Dupliquer( NGHOST_CLIENT_CONFIGURATION_PROPERTY_AUTHENTICATION_DEFAULT_PASSWORD ) )
		|| !( out->m_networkListeningInterface = NLib_Chaine_Dupliquer( NGHOST_CLIENT_CONFIGURATION_PROPERTY_NETWORK_DEFAULT_LISTENING_INTERFACE ) )
		|| !( out->m_deviceName = NLib_Chaine_Dupliquer( NGHOST_CLIENT_CONFIGURATION_PROPERTY_DEVICE_DEFAULT_NAME ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out->m_networkListeningInterface );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_authenticationPassword );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Copy default values
	out->m_networkListeningPort = NGHOST_CLIENT_CONFIGURATION_PROPERTY_NETWORK_DEFAULT_LISTENING_PORT;
	out->m_scannerThreadCount = NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_DEFAULT_THREAD_COUNT;
	out->m_scannerConnectionTimeout = NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_DEFAULT_CONNECTION_TIMEOUT;
	out->m_scannerRequestTimeout = NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_DEFAULT_REQUEST_TIMEOUT;
	out->m_isAuthenticationManagerDataPersistent = NGHOST_CLIENT_CONFIGURATION_PROPERTY_AUTHENTICATION_DEFAULT_IS_AUTHENTICATION_MANAGER_DATA_PERSISTENT;

	// Build default filter
	if( !( out->m_scannerInterfaceFilter = NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Build( NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_DEFAULT_INTERFACE_FILTER ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out->m_deviceName );
		NFREE( out->m_networkListeningInterface );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_authenticationPassword );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build default authorized ip for token obtention
	if( !( out->m_networkTokenTrustedIPList = NGhostCommon_Token_NGhostTrustedTokenIPList_Build( NGHOST_CLIENT_CONFIGURATION_PROPERTY_NETWORK_TOKEN_DELIVERY_TRUSTED_IP_DEFAULT ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Destroy( &out->m_scannerInterfaceFilter );

		// Free
		NFREE( out->m_deviceName );
		NFREE( out->m_networkListeningInterface );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_authenticationPassword );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build default scanner user address
	if( !( out->m_scannerUserAddress = NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_Build( NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_DEFAULT_USER_ADDRESS ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NGhostCommon_Token_NGhostTrustedTokenIPList_Destroy( &out->m_networkTokenTrustedIPList );
		NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Destroy( &out->m_scannerInterfaceFilter );

		// Free
		NFREE( out->m_deviceName );
		NFREE( out->m_networkListeningInterface );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_authenticationPassword );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build scanner filtered types list
	if( !( out->m_scannerTypeFilter = NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_Build( NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_DEFAULT_TYPE_FILTER ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CANT_PROCESS_REQUEST );

		// Destroy
		NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Destroy( &out->m_scannerInterfaceFilter );
		NGhostCommon_Token_NGhostTrustedTokenIPList_Destroy( &out->m_networkTokenTrustedIPList );
		NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_Destroy( &out->m_scannerUserAddress );

		// Free
		NFREE( out->m_deviceName );
		NFREE( out->m_networkListeningInterface );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_authenticationPassword );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build uuid filter
	if( !( out->m_scannerUUIDFilter = NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_Build( NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_DEFAULT_UUID_FILTER ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CANT_PROCESS_REQUEST );

		// Destroy
		NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_Destroy( &out->m_scannerTypeFilter );
		NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Destroy( &out->m_scannerInterfaceFilter );
		NGhostCommon_Token_NGhostTrustedTokenIPList_Destroy( &out->m_networkTokenTrustedIPList );
		NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_Destroy( &out->m_scannerUserAddress );

		// Free
		NFREE( out->m_deviceName );
		NFREE( out->m_networkListeningInterface );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_authenticationPassword );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build version
	NGhostClient_Helper_BuildVersionShort( out->m_otherInformationBuildInformation );

	// Read unique ID
	if( !( out->m_deviceUniqueID = NGhostCommon_Hardware_GetUniqueID( NDEVICE_TYPE_GHOST_CLIENT ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CANT_PROCESS_REQUEST );

		// Destroy
		NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_Destroy( &out->m_scannerUUIDFilter );
		NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_Destroy( &out->m_scannerTypeFilter );
		NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Destroy( &out->m_scannerInterfaceFilter );
		NGhostCommon_Token_NGhostTrustedTokenIPList_Destroy( &out->m_networkTokenTrustedIPList );
		NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_Destroy( &out->m_scannerUserAddress );

		// Free
		NFREE( out->m_deviceName );
		NFREE( out->m_networkListeningInterface );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_authenticationPassword );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy configuration
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Configuration_NGhostClientConfiguration_Destroy( NGhostClientConfiguration **this )
{
	// Destroy
	NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_Destroy( &(*this)->m_scannerUUIDFilter );
	NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_Destroy( &(*this)->m_scannerTypeFilter );
	NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_Destroy( &(*this)->m_scannerUserAddress );
	NGhostCommon_Token_NGhostTrustedTokenIPList_Destroy( &(*this)->m_networkTokenTrustedIPList );
	NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Destroy( &(*this)->m_scannerInterfaceFilter );

	// Free
	NFREE( (*this)->m_deviceUniqueID );
	NFREE( (*this)->m_deviceName );
	NFREE( (*this)->m_networkListeningInterface );
	NFREE( (*this)->m_authenticationUsername );
	NFREE( (*this)->m_authenticationPassword );
	NFREE( (*this) );
}

/**
 * Get the username
 *
 * @param this
 * 		This instance
 *
 * @return the username
 */
const char *NGhostClient_Configuration_NGhostClientConfiguration_GetAuthenticationUsername( const NGhostClientConfiguration *this )
{
	return this->m_authenticationUsername;
}

/**
 * Get the password
 *
 * @param this
 * 		This instance
 *
 * @return the password
 */
const char *NGhostClient_Configuration_NGhostClientConfiguration_GetAuthenticationPassword( const NGhostClientConfiguration *this )
{
	return this->m_authenticationPassword;
}

/**
 * Is authentication manager data persistent?
 *
 * @param this
 * 		This instance
 *
 * @return if the authentication manager data are persistent
 */
NBOOL NGhostClient_Configuration_NGhostClientConfiguration_IsAuthenticationManagerDataPersistent( const NGhostClientConfiguration *this )
{
	return this->m_isAuthenticationManagerDataPersistent;
}

/**
 * Get the listening interface
 *
 * @param this
 * 		This instance
 *
 * @return the listening interface
 */
const char *NGhostClient_Configuration_NGhostClientConfiguration_GetNetworkListeningInterface( const NGhostClientConfiguration *this )
{
	return this->m_networkListeningInterface;
}

/**
 * Is listening on all interfaces?
 *
 * @param this
 * 		This instance
 *
 * @return if the client must listen on all interface
 */
NBOOL NGhostClient_Configuration_NGhostClientConfiguration_IsNetworkMustListenAllInterface( const NGhostClientConfiguration *this )
{
	return NLib_Chaine_Comparer( this->m_networkListeningInterface,
		"*",
		NTRUE,
		0 );
}

/**
 * Get listening port
 *
 * @param this
 * 		This instance
 *
 * @return the listening port
 */
NU32 NGhostClient_Configuration_NGhostClientConfiguration_GetNetworkListeningPort( const NGhostClientConfiguration *this )
{
	return this->m_networkListeningPort;
}

/**
 * Get the device name
 *
 * @param this
 * 		This instance
 *
 * @return the device name
 */
const char *NGhostClient_Configuration_NGhostClientConfiguration_GetDeviceName( const NGhostClientConfiguration *this )
{
	return this->m_deviceName;
}

/**
 * Get build informations
 *
 * @param this
 * 		This instance
 *
 * @return the build informations
 */
const char *NGhostClient_Configuration_NGhostClientConfiguration_GetBuildInformation( const NGhostClientConfiguration *this )
{
	return this->m_otherInformationBuildInformation;
}

/**
 * Get the scan filter interface list
 *
 * @param this
 * 		This instance
 *
 * @return the scan filter interface list
 */
const NGhostClientConfigurationInterfaceFilter *NGhostClient_Configuration_NGhostClientConfiguration_GetScannerInterfaceFilter( const NGhostClientConfiguration *this )
{
	return this->m_scannerInterfaceFilter;
}

/**
 * Get device unique ID
 *
 * @param this
 * 		This instance
 *
 * @return the device unique ID
 */
const char *NGhostClient_Configuration_NGhostClientConfiguration_GetDeviceUniqueID( const NGhostClientConfiguration *this )
{
	return this->m_deviceUniqueID;
}

/**
 * Get scanner thread count
 *
 * @param this
 * 		This instance
 *
 * @return the scanner thread count
 */
NU32 NGhostClient_Configuration_NGhostClientConfiguration_GetScannerThreadCount( const NGhostClientConfiguration *this )
{
	return this->m_scannerThreadCount;
}

/**
 * Get scanner connection timeout
 *
 * @param this
 * 		This instance
 *
 * @return the scanner connection timeout
 */
NU32 NGhostClient_Configuration_NGhostClientConfiguration_GetScannerConnectionTimeout( const NGhostClientConfiguration *this )
{
	return this->m_scannerConnectionTimeout;
}

/**
 * Get scanner request timeout
 *
 * @param this
 * 		This instance
 *
 * @return the scanner request timeout
 */
NU32 NGhostClient_Configuration_NGhostClientConfiguration_GetScannerRequestTimeout( const NGhostClientConfiguration *this )
{
	return this->m_scannerRequestTimeout;
}

/**
 * Get scanner user address
 *
 * @param this
 * 		This instance
 *
 * @return the user defined address to scan
 */
const NGhostConfigurationScannerUserAddressList *NGhostClient_Configuration_NGhostClientConfiguration_GetScannerUserAddressList( const NGhostClientConfiguration *this )
{
	return this->m_scannerUserAddress;
}

/**
 * Get scanner uuid filter
 *
 * @param this
 * 		This instance
 *
 * @return the uuid filter list
 */
const NGhostClientConfigurationScannerUUIDFilter *NGhostClient_Configuration_NGhostClientConfiguration_GetScannerUUIDFilterList( const NGhostClientConfiguration *this )
{
	return this->m_scannerUUIDFilter;
}

/**
 * Get scanner type filter
 *
 * @param this
 * 		This instance
 *
 * @return the type filter list
 */
const NGhostClientConfigurationScannerTypeFilter *NGhostClient_Configuration_NGhostClientConfiguration_GetScannerTypeFilterList( const NGhostClientConfiguration *this )
{
	return this->m_scannerTypeFilter;
}

/**
 * Is device type filtered?
 *
 * @param this
 * 		This instance
 * @param deviceType
 * 		The device type to be checked
 *
 * @return if the device type is filtered
 */
NBOOL NGhostClient_Configuration_NGhostClientConfiguration_IsDeviceTypeFiltered( const NGhostClientConfiguration *this,
	NDeviceType deviceType )
{
	// Check
	return NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_IsFiltered( this->m_scannerTypeFilter,
		deviceType );
}

/**
 * Is device uuid filtered
 *
 * @param this
 * 		This instance
 * @param uuid
 * 		The device uuid to be checked
 *
 * @return if the device uuid is filtered
 */
NBOOL NGhostClient_Configuration_NGhostClientConfiguration_IsDeviceUUIDFiltered( const NGhostClientConfiguration *this,
	const char *uuid )
{
	// Check
	return NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_IsUUIDFiltered( this->m_scannerUUIDFilter,
		uuid );
}

/**
 * Get trusted token ip list
 *
 * @param this
 * 		This instance
 *
 * @return the trusted ip list
 */
NGhostTrustedTokenIPList *NGhostClient_Configuration_NGhostClientConfiguration_GetTrustedTokenIPList( const NGhostClientConfiguration *this )
{
	return this->m_networkTokenTrustedIPList;
}

/**
 * Save the configuration
 *
 * @param this
 * 		This instance
 * @param configPath
 * 		The config file path
 *
 * @return if operation succeeded
 */
NBOOL NGhostClient_Configuration_NGhostClientConfiguration_Save( const NGhostClientConfiguration *this,
	const char *configPath )
{
	// File
	NFichierTexte *file;

	// Buffer
	char buffer[ 2048 ];

	// Export
	char *export;

#define WRITE_TEXT_PROPERTY( key, value, isNumber ) \
    { \
        /* Prepare buffer */ \
        snprintf( buffer, \
            2048, \
			( isNumber ) ? \
				"%s %d\n" \
				: "%s %s\n", \
            NGhostClient_Configuration_NGhostClientConfigurationProperty_GetKey( key ), \
            value ); \
 \
        /* Write in file */ \
        NLib_Fichier_NFichierTexte_Ecrire3( file, \
            buffer ); \
    }

	// Open file
	if( !( file = NLib_Fichier_NFichierTexte_ConstruireEcriture( configPath,
		NTRUE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quit
		return NFALSE;
	}

	// Introduction
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"# Generated by Ghost version " );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		this->m_otherInformationBuildInformation );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"\n# Please always use spaces, never tabulate...\n\n" );

	// Device properties
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"[Device]\n" );

	WRITE_TEXT_PROPERTY( NGHOST_CLIENT_CONFIGURATION_PROPERTY_DEVICE_NAME,
		this->m_deviceName,
		NFALSE );

	// Authentication properties
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"\n[Authentication]\n" );

	WRITE_TEXT_PROPERTY( NGHOST_CLIENT_CONFIGURATION_PROPERTY_AUTHENTICATION_USERNAME,
		this->m_authenticationUsername,
		NFALSE );
	WRITE_TEXT_PROPERTY( NGHOST_CLIENT_CONFIGURATION_PROPERTY_AUTHENTICATION_PASSWORD,
		this->m_authenticationPassword,
		NFALSE );
	WRITE_TEXT_PROPERTY( NGHOST_CLIENT_CONFIGURATION_PROPERTY_IS_AUTHENTICATION_MANAGER_DATA_PERSISTENT,
		NLib_Type_NBOOL_GetKeyword( this->m_isAuthenticationManagerDataPersistent ),
		NFALSE );

	// Network properties
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"\n[Network]\n# Listen on one specified ip, or on all interface with *\n" );

	WRITE_TEXT_PROPERTY( NGHOST_CLIENT_CONFIGURATION_PROPERTY_NETWORK_LISTENING_INTERFACE,
		this->m_networkListeningInterface,
		NFALSE );
	WRITE_TEXT_PROPERTY( NGHOST_CLIENT_CONFIGURATION_PROPERTY_NETWORK_LISTENING_PORT,
		this->m_networkListeningPort,
		NTRUE );
	if( ( export = NGhostCommon_Token_NGhostTrustedTokenIPList_Export( this->m_networkTokenTrustedIPList ) ) != NULL )
	{
		WRITE_TEXT_PROPERTY( NGHOST_CLIENT_CONFIGURATION_PROPERTY_NETWORK_TOKEN_DELIVERY_TRUSTED_IP,
			export,
			NFALSE );
		NFREE( export );
	}

	// Scanner properties
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"\n[Scanner]\n" );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"# The interfaces devices won't be looking for on (comma separated)\n" );
	if( ( export = NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_Export( this->m_scannerInterfaceFilter ) ) != NULL )
	{
		WRITE_TEXT_PROPERTY( NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_INTERFACE_FILTER,
			export,
			NFALSE );
		NFREE( export );
	}
	WRITE_TEXT_PROPERTY( NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_THREAD_COUNT,
		this->m_scannerThreadCount,
		NTRUE );
	WRITE_TEXT_PROPERTY( NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_CONNECTION_TIMEOUT,
		this->m_scannerConnectionTimeout,
		NTRUE );
	WRITE_TEXT_PROPERTY( NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_REQUEST_TIMEOUT,
		this->m_scannerRequestTimeout,
		NTRUE );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"# The address to scan in addition to found interface address (comma separated, X.X.X.X/CIDR)\n" );
	if( ( export = NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_Export( this->m_scannerUserAddress ) ) != NULL )
	{
		WRITE_TEXT_PROPERTY( NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_USER_ADDRESS,
			export,
			NFALSE );
		NFREE( export );
	}
	else
		WRITE_TEXT_PROPERTY( NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_USER_ADDRESS,
			"",
			NFALSE );
	if( ( export = NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_Export( this->m_scannerTypeFilter ) ) != NULL )
	{
		WRITE_TEXT_PROPERTY( NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_TYPE_FILTER,
			export,
			NFALSE );
		NFREE( export );
	}
	else
		WRITE_TEXT_PROPERTY( NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_TYPE_FILTER,
			"",
			NFALSE );
	if( ( export = NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_Export( this->m_scannerUUIDFilter ) ) != NULL )
	{
		WRITE_TEXT_PROPERTY( NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_UUID_FILTER,
			export,
			NFALSE );
		NFREE( export );
	}
	else
		WRITE_TEXT_PROPERTY( NGHOST_CLIENT_CONFIGURATION_PROPERTY_SCANNER_UUID_FILTER,
			"",
			NFALSE );

#undef WRITE_TEXT_PROPERTY

	// Close file
	NLib_Fichier_NFichierTexte_Detruire( &file );

	// OK
	return NTRUE;
}

