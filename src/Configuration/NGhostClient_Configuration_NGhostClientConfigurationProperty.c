#define NGHOSTCLIENT_CONFIGURATION_NGHOSTCLIENTCONFIGURATIONPROPERTY_INTERNE
#include "../../include/NGhostClient.h"

// -------------------------------------------------------------------
// enum NGhostClient::Configuration::NGhostClientConfigurationProperty
// -------------------------------------------------------------------

/**
 * Get key set
 *
 * @return the key set
 */
__ALLOC char **NGhostClient_Configuration_NGhostClientConfigurationProperty_GetKeySet( void )
{
	// Output
	char **out;

	// Iterator
	NU32 i = 0;

	// Allocate memory
	if( !( out = calloc( NGHOST_CLIENT_CONFIGURATION_PROPERTIES,
		sizeof( char* ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Copy properties addresses
	for( ; i < NGHOST_CLIENT_CONFIGURATION_PROPERTIES; i++ )
		out[ i ] = (char*)NGhostClientConfigurationPropertyText[ i ];

	// OK
	return out;
}

/**
 * Get key
 *
 * @param key
 * 		The key to get
 *
 * @return the key
 */
const char *NGhostClient_Configuration_NGhostClientConfigurationProperty_GetKey( NGhostClientConfigurationProperty key )
{
	return NGhostClientConfigurationPropertyText[ key ];
}

