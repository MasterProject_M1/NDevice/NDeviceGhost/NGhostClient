#include "../../include/NGhostClient.h"

// -------------------------------
// namespace NGhostClient::Service
// -------------------------------

/**
 * Process an HTTP request (private)
 *
 * @param request
 * 		The http request
 * @param client
 * 		The requesting client
 *
 * @return the HTTP response
 */
__PRIVATE __ALLOC NReponseHTTP *NGhostClient_Service_ProcessRequestInternal( const NRequeteHTTP *request,
	const NClientServeur *client )
{
	// Response code
	NHTTPCode responseCode;

	// Service type
	NGhostServiceType serviceType;
	NGhostCommonService commonServiceType;

	// HTTP Server
	const NHTTPServeur *httpServer;

	// Ghost client
	NGhostClient *ghostClient;

	// Ghost common
	NGhostCommon *ghostCommon;

	// Response
	NReponseHTTP *response;

	// Configuration
	const NGhostClientConfiguration *ghostClientConfiguration;

	// Is authorized?
	NBOOL isAuthorized;

	// Device container list
	const NGhostDeviceContainerList *ghostDeviceContainerList;

	// Cursor
	NU32 cursor = 0;

	// Get server and client with configuration
	if( !( httpServer = (NHTTPServeur*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client ) )
		|| !( ghostClient = (NGhostClient*)NHTTP_Serveur_NHTTPServeur_ObtenirDataUtilisateur( httpServer ) )
		|| !( ghostCommon = NGhostClient_NGhostClient_GetGhostCommon( ghostClient ) )
		|| !( ghostClientConfiguration = NGhostClient_NGhostClient_GetConfiguration( ghostClient ) )
		|| !( ghostDeviceContainerList = NGhostClient_NGhostClient_GetDeviceContainerList( ghostClient ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NULL;
	}

	// Process standard services
	if( ( response = NGhostCommon_Service_ProcessBasicService( request,
		ghostCommon,
		&isAuthorized,
		&commonServiceType,
		&cursor,
		client,
		NGhostCommon_NGhostCommon_GetIP( ghostCommon ) ) ) != NULL )
	{
		// Broadcast to all known ghost devices
		switch( commonServiceType )
		{
			default:
				break;

			case NGHOST_COMMON_SERVICE_AUTHENTICATION_ADD:
			case NGHOST_COMMON_SERVICE_AUTHENTICATION_DELETE:
			case NGHOST_COMMON_SERVICE_AUTHENTICATION_UPDATE:
				NGhostClient_Device_NGhostDeviceContainerList_BroadcastRequestGhostDevice2( (NGhostDeviceContainerList *)ghostDeviceContainerList,
					request,
					NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ) );
				break;
		}

		// OK
		return response;
	}

	// Check authentication
	if( !isAuthorized )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_AUTH_FAILED );

		// Quit
		return NGhostCommon_Service_CreateUnauthorizedResponse( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );
	}

	// Find service type
	if( ( serviceType = NGhostClient_Service_NGhostServiceType_FindServiceType( NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElementInitial( request ),
		NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirType( request ) ) ) >= NGHOST_SERVICE_TYPES )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

		// Quit
		return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
			NHTTP_CODE_400_BAD_REQUEST,
			NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );
	}

	// Response code is 200 by default
	responseCode = NHTTP_CODE_200_OK;

	// Init cursor
	cursor = (NU32)strlen( NGhostClient_Service_NGhostServiceType_GetAPIElement( serviceType ) );

	// Analyze service type
	switch( serviceType )
	{
		case NGHOST_SERVICE_TYPE_LIST_ALL_DEVICE:
			return NGhostClient_Service_Device_BuildHTTPResponseDescribeAllDevice( ghostDeviceContainerList,
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ),
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ) );

		case NGHOST_SERVICE_TYPE_LAUNCH_SCAN_FOR_DEVICE:
			// Start service
			responseCode = NGhostClient_NGhostClient_CallScanningService( ghostClient,
				NTRUE ) ?
					NHTTP_CODE_202_ACCEPTED
					: NHTTP_CODE_208_ALREADY_REPORTED;

			// Notify
			return NGhostClient_Service_Scanner_BuildScannerStartResponse( responseCode,
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ),
				ghostClientConfiguration,
				NGhostClient_NGhostClient_GetScannerService( ghostClient ) );

		case NGHOST_SERVICE_TYPE_STOP_SCAN_FOR_DEVICE:
			responseCode = NGhostClient_NGhostClient_CallScanningService( ghostClient,
				NFALSE ) ?
					NHTTP_CODE_202_ACCEPTED
					: NHTTP_CODE_428_PRECONDITION_REQUIRED;

		case NGHOST_SERVICE_TYPE_GET_SCAN_FOR_DEVICE_STATE:
			return NGhostClient_Service_Scanner_BuildScannerStartResponse( responseCode,
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ),
				ghostClientConfiguration,
				NGhostClient_NGhostClient_GetScannerService( ghostClient ) );

		case NGHOST_SERVICE_TYPE_GET_DEVICE_ROOT:
			return ( response = NGhostClient_Service_Device_ProcessRESTGetRequest( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ),
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ),
				request,
				ghostDeviceContainerList ) ) != NULL ?
					response
					: NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
				NHTTP_CODE_400_BAD_REQUEST,
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );

		case NGHOST_SERVICE_TYPE_PUT_DEVICE_ROOT:
			return ( response = NGhostClient_Service_Device_ProcessRESTPUTRequest( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ),
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ),
				request,
				ghostDeviceContainerList ) ) != NULL ?
					response
					: NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
				NHTTP_CODE_400_BAD_REQUEST,
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );

		case NGHOST_SERVICE_TYPE_POST_DEVICE_ROOT:
			return ( response = NGhostClient_Service_Device_ProcessRESTPOSTRequest( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ),
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ),
				request,
				ghostDeviceContainerList ) ) != NULL ?
					response
					: NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
				NHTTP_CODE_400_BAD_REQUEST,
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );

		default:
			// Process as normal file
			return NHTTP_Commun_HTTP_Traitement_TraiterRequete( request,
				client );
	}
}

/**
 * Process an HTTP request
 *
 * @param request
 * 		The http request
 * @param client
 * 		The requesting client
 *
 * @return the HTTP response
 */
__ALLOC NReponseHTTP *NGhostClient_Service_ProcessRequest( const NRequeteHTTP *request,
	const NClientServeur *client )
{
	// Response packet
	NReponseHTTP *response;

	// Process request
	if( !( response = NGhostClient_Service_ProcessRequestInternal( request,
		client ) ) )
		return NGhostCommon_Service_CreateInternalErrorResponse( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );

	// OK
	return response;
}

