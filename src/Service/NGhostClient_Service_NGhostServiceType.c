#define NGHOSTCLIENT_SERVICE_NGHOSTSERVICETYPE_INTERNE
#include "../../include/NGhostClient.h"

// ---------------------------------------------
// enum NGhostClient::Service::NGhostServiceType
// ---------------------------------------------

/**
 * Find service type
 *
 * @param apiElement
 * 		The requested element
 * @param httpMethod
 * 		The http method
 *
 * @return the service type or NGHOST_SERVICE_TYPES if not in known list
 */
NGhostServiceType NGhostClient_Service_NGhostServiceType_FindServiceType( const char *apiElement,
	NTypeRequeteHTTP httpMethod )
{
	// Iterator
	__OUTPUT NGhostServiceType output = (NGhostServiceType)0;

	// Iterate
	for( ; output < NGHOST_SERVICE_TYPES; output++ )
		// Check method
		if( httpMethod == NGhostServiceTypeAPIMethod[ output ] )
		{
			// REST structure?
			if( NGhostServiceURIAPIIsRoot[ output ] )
			{
				// Starting with root?
				if( NLib_Chaine_EstChaineCommencePar( apiElement,
					NGhostServiceTypeAPI[ output ] ) )
					// Found it
					return output;
			}
			// Not a REST structure?
			else
				// Check URI
				if( NLib_Chaine_Comparer( apiElement,
					NGhostServiceTypeAPI[ output ],
					NTRUE,
					0 ) )
					// Found it
					return output;
		}

	// Can't be found
	return NGHOST_SERVICE_TYPES;
}

/**
 * Get api element
 *
 * @param type
 * 		The service type
 *
 * @return the api element
 */
const char *NGhostClient_Service_NGhostServiceType_GetAPIElement( NGhostServiceType type )
{
	return NGhostServiceTypeAPI[ type ];
}

/**
 * Get api method
 *
 * @param type
 * 		The service type
 *
 * @return the api method
 */
NTypeRequeteHTTP NGhostClient_Service_NGhostServiceType_GetAPIMethod( NGhostServiceType type )
{
	return NGhostServiceTypeAPIMethod[ type ];
}

