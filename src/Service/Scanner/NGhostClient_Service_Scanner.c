#include "../../../include/NGhostClient.h"

// ----------------------------------------
// namespace NGhostClient::Service::Scanner
// ----------------------------------------

/**
 * Generate parser output list
 *
 * @param configuration
 * 		The ghost client configuration
 * @param scanner
 * 		The ghost scanner service
 * @param parserOutputList
 * 		The parser output list
 *
 * @return the parser output list
 */
NBOOL NGhostClient_Service_Scanner_GenerateScannerParserOutputList( const NGhostClientConfiguration *configuration,
	const NGhostClientScannerService *scanner,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Buffer
	char buffer[ 2048 ];

	// Interfaces list
	const NListe *interfaceList;

	// Interface
	const NInterfaceReseau *interface;

	// Iterator
	NU32 i = 0;

	// UUID filter
	const NGhostClientConfigurationScannerUUIDFilter *ghostClientConfigurationScannerUUIDFilter;

	// Type filter
	const NGhostClientConfigurationScannerTypeFilter *ghostClientConfigurationScannerTypeFilter;

	// Get filter
	if( !( ghostClientConfigurationScannerUUIDFilter = NGhostClient_Configuration_NGhostClientConfiguration_GetScannerUUIDFilterList( configuration ) )
		|| !( ghostClientConfigurationScannerTypeFilter = NGhostClient_Configuration_NGhostClientConfiguration_GetScannerTypeFilterList( configuration ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	// Add thread count
	NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
		NGHOST_CLIENT_SERVICE_SCANNER_THREAD_COUNT,
		NGhostClient_Configuration_NGhostClientConfiguration_GetScannerThreadCount( configuration ) );

	// Add connection timeout
	NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
		NGHOST_CLIENT_SERVICE_SCANNER_CONNECTION_TIMEOUT,
		NGhostClient_Configuration_NGhostClientConfiguration_GetScannerConnectionTimeout( configuration ) );

	// Add request timeout
	NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
		NGHOST_CLIENT_SERVICE_SCANNER_REQUEST_TIMEOUT,
		NGhostClient_Configuration_NGhostClientConfiguration_GetScannerRequestTimeout( configuration ) );

	// Add start timestamp
	NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
		NGHOST_CLIENT_SERVICE_SCANNER_START_TIMESTAMP,
		(NU32)NGhostClient_Service_Scanner_NGhostClientScannerService_GetStartingTimestamp( scanner ) );

	// Add current time taken
	NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
		NGHOST_CLIENT_SERVICE_SCANNER_TIME_TAKEN,
		NGhostClient_Service_Scanner_NGhostClientScannerService_IsRunning( scanner ) ?
			( (NU32)NLib_Temps_ObtenirTimestamp( ) - (NU32)NGhostClient_Service_Scanner_NGhostClientScannerService_GetStartingTimestamp( scanner ) )
			: ( (NU32)NGhostClient_Service_Scaner_NGhostClientScannerService_GetEndingTimestamp( scanner ) - (NU32)NGhostClient_Service_Scanner_NGhostClientScannerService_GetStartingTimestamp( scanner ) ) );

	// Add is running informations
	NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
		NGHOST_CLIENT_SERVICE_SCANNER_IS_RUNNING,
		NGhostClient_Service_Scanner_NGhostClientScannerService_IsRunning( scanner ) );

	// Current processed interface
	if( NGhostClient_Service_Scanner_NGhostClientScannerService_GetCurrentInterfaceScan( scanner ) != NULL )
		NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
			NGHOST_CLIENT_SERVICE_SCANNER_CURRENT_PROCESSED_INTERFACE,
			NGhostClient_Service_Scanner_NGhostClientScannerService_GetCurrentInterfaceScan( scanner ) );

	// Current scanned ip count
	NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
		NGHOST_CLIENT_SERVICE_SCANNER_CURRENT_IP_SCANNED_COUNT,
		(NU32)NGhostClient_Service_Scanner_NGhostClientScannerService_GetAlreadyScanIPCount( scanner ) );

	// Total ip to scan count
	NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
		NGHOST_CLIENT_SERVICE_SCANNER_CURRENT_TOTAL_IP_TO_SCAN_COUNT,
		(NU32)NGhostClient_Service_Scanner_NGhostClientScannerService_GetTotalIPToScanCount( scanner )  );

	// Total ip to scan count
	NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
		NGHOST_CLIENT_SERVICE_SCANNER_CURRENT_FOUND_DEVICES_COUNT,
		NGhostClient_Service_Scanner_NGhostClientScannerService_GetFoundDeviceCount( scanner ) );

	// Progress percent
	NParser_Output_NParserOutputList_AddEntryDouble( parserOutputList,
		NGHOST_CLIENT_SERVICE_SCANNER_CURRENT_PERCENT_PROGRESS,
		NGhostClient_Service_Scanner_NGhostClientScannerService_GetTotalIPToScanCount( scanner ) != 0 ?
			( (double)NGhostClient_Service_Scanner_NGhostClientScannerService_GetAlreadyScanIPCount( scanner ) / (double)NGhostClient_Service_Scanner_NGhostClientScannerService_GetTotalIPToScanCount( scanner ) * 100.0 )
			: 0.0 );

	// Add scanned interfaces informations
	if( ( interfaceList = NGhostClient_Service_Scanner_NGhostClientScannerService_GetScanInterfaceList( scanner ) ) != NULL )
		for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( interfaceList ); i++ )
			// Get interface
			if( ( interface = NLib_Memoire_NListe_ObtenirElementDepuisIndex( interfaceList,
				i ) ) != NULL )
			{
				// Interface name
				snprintf( buffer,
					2048,
					"%s.%d.%s",
					NGHOST_CLIENT_SERVICE_SCANNER_SCANNED_INTERFACE,
					i,
					NGHOST_CLIENT_SERVICE_SCANNER_SUBKEY_INTERFACE_NAME );
				NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					buffer,
					NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirNom( interface ) );

				// Interface IP
				snprintf( buffer,
					2048,
					"%s.%d.%s",
					NGHOST_CLIENT_SERVICE_SCANNER_SCANNED_INTERFACE,
					i,
					NGHOST_CLIENT_SERVICE_SCANNER_SUBKEY_INTERFACE_IP );
				NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					buffer,
					NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirAdresse( interface ) );

				// Interface CIDR
				snprintf( buffer,
					2048,
					"%s.%d.%s",
					NGHOST_CLIENT_SERVICE_SCANNER_SCANNED_INTERFACE,
					i,
					NGHOST_CLIENT_SERVICE_SCANNER_SUBKEY_INTERFACE_CIDR );
				NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
					buffer,
					NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirCIDR( interface ) );

				// Interface MAC
				snprintf( buffer,
					2048,
					"%s.%d.%s",
					NGHOST_CLIENT_SERVICE_SCANNER_SCANNED_INTERFACE,
					i,
					NGHOST_CLIENT_SERVICE_SCANNER_SUBKEY_INTERFACE_MAC );
				NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					buffer,
					NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirAdresseMAC( interface ) );
			}

	// Add uuid filter details
	for( i = 0; i < NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_GetFilteredUUIDCount( ghostClientConfigurationScannerUUIDFilter ); i++ )
		NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
			NGHOST_CLIENT_SERVICE_SCANNER_FILTERED_UUID,
			NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerUUIDFilter_GetFilteredUUID( ghostClientConfigurationScannerUUIDFilter,
				i ) );

	// Add type filter details
	for( i = 0; i < NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_GetFilteredTypeCount( ghostClientConfigurationScannerTypeFilter ); i++ )
		NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
			NGHOST_CLIENT_SERVICE_SCANNER_FILTERED_TYPE,
			NDeviceCommon_Type_NDeviceType_GetUserFriendlyName( NGhostClient_Configuration_Scanner_NGhostClientConfigurationScannerTypeFilter_GetFilteredType( ghostClientConfigurationScannerTypeFilter,
				i ) ) );

	// OK
	return NTRUE;
}

/**
 * Build scanning started response
 *
 * @param responseCode
 * 		The response code
 * @param clientID
 * 		The client ID
 * @param configuration
 * 		The ghost client configuration
 * @param scanner
 * 		The scanner service
 *
 * @return the response
 */
__ALLOC NReponseHTTP *NGhostClient_Service_Scanner_BuildScannerStartResponse( NHTTPCode responseCode,
	NU32 clientID,
	const NGhostClientConfiguration *configuration,
	const NGhostClientScannerService *scanner )
{
	// Parser output
	NParserOutputList *parserOutputList;

	// Generate parser output list
	if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	if( !NGhostClient_Service_Scanner_GenerateScannerParserOutputList( configuration,
		scanner,
		parserOutputList ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NULL;
	}

	// Generate http response
	return NDeviceCommon_Service_GenerateJsonHTTPResponseWithParserOutputList( responseCode,
		parserOutputList,
		clientID );
}
