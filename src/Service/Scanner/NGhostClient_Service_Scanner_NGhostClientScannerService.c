#include "../../../include/NGhostClient.h"

// -----------------------------------------------------------------
// struct NGhostClient::Service::Scanner::NGhostClientScannerService
// -----------------------------------------------------------------

/**
 * Create interface scan list (private)
 *
 * @param configuration
 * 		The ghost client configuration
 *
 * @return the interface to scan list (NListe<NInterfaceReseau*>)
 */
__PRIVATE __ALLOC NListe *NGhostClient_Service_Scanner_NGhostClientScannerService_CreateInterfaceScanList( const NGhostClientConfiguration *configuration )
{
	// Output
	NListe *out;

	// Interfaces list
	NListe *interfaceList;

	// Network interface
	const NInterfaceReseau *interface;
	NInterfaceReseau *interfaceCopy;

	// Interface filter
	const NGhostClientConfigurationInterfaceFilter *interfaceFilter;

	// User addresses list
	const NGhostConfigurationScannerUserAddressList *userAddressScanList;

	// User address
	const NGhostConfigurationScannerUserAddress *userAddress;

	// Buffer
	char buffer[ 128 ];

	// Empty MAC address
	const char emptyMAC[ NTAILLE_ADRESSE_MAC + 1 ] = "00:00:00:00:00:00";

	// Iterator
	NU32 i;

	// Get options
	if( !( interfaceFilter = NGhostClient_Configuration_NGhostClientConfiguration_GetScannerInterfaceFilter( configuration ) )
		|| !( userAddressScanList = NGhostClient_Configuration_NGhostClientConfiguration_GetScannerUserAddressList( configuration ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quit
		return NULL;
	}

	// List interfaces
	if( !( interfaceList = NLib_Module_Reseau_Interface_TrouverTouteInterface( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Quit
		return NULL;
	}

	// Create list
	if( !( out = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NLib_Module_Reseau_Interface_NInterfaceReseau_Detruire ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NLib_Memoire_NListe_Detruire( &interfaceList );

		// Quit
		return NULL;
	}

	// Fill list with found interfaces
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( interfaceList ); i++ )
	{
		// Get interface
		interface = (const NInterfaceReseau*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( interfaceList,
			i );

		// Check name
		if( NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_IsInterfaceAuthorized( interfaceFilter,
			NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirNom( interface ) ) )
		{
			// Duplicate interface
			if( !( interfaceCopy = NLib_Module_Reseau_Interface_NInterfaceReseau_Construire2( interface ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_COPY );

				// Free
				NLib_Memoire_NListe_Detruire( &out );
				NLib_Memoire_NListe_Detruire( &interfaceList );

				// Quit
				return NULL;
			}

			// Add
			NLib_Memoire_NListe_Ajouter( out,
				interfaceCopy );
		}
	}

	// Fill list with user mades addresses
	for( i = 0; i < NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_GetAddressCount( userAddressScanList ); i++ )
		// Get address
		if( ( userAddress = NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddressList_GetAddress( userAddressScanList,
			i ) ) != NULL )
		{
			// Build name
			snprintf( buffer,
				128,
				"UserInterface%d",
				i );

			// Check name
			if( NGhostClient_Configuration_Network_NGhostClientConfigurationInterfaceFilter_IsInterfaceAuthorized( interfaceFilter,
				buffer ) )
				// Create interface
				if( ( interfaceCopy = NLib_Module_Reseau_Interface_NInterfaceReseau_Construire( buffer,
					NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddress_GetAddress( userAddress ),
					NGhostClient_Configuration_Scanner_NGhostConfigurationScannerUserAddress_GetCIDR( userAddress ),
					emptyMAC ) ) != NULL )
					// Add to list
					NLib_Memoire_NListe_Ajouter( out,
						interfaceCopy );
		}

	// Destroy interfaces list
	NLib_Memoire_NListe_Detruire( &interfaceList );

	// OK
	return out;
}

/**
 * Build scanner service
 *
 * @param ghostClient
 * 		The ghost client
 *
 * @return the scanner instance
 */
__ALLOC NGhostClientScannerService *NGhostClient_Service_Scanner_NGhostClientScannerService_Build( NGhostClient *ghostClient )
{
	// Output
	__OUTPUT NGhostClientScannerService *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostClientScannerService ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Save
	if( !( out->m_client = ghostClient )
		|| !( out->m_configuration = NGhostClient_NGhostClient_GetConfiguration( ghostClient ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build interface list
	if( !( out->m_interfaceScanList = NGhostClient_Service_Scanner_NGhostClientScannerService_CreateInterfaceScanList( out->m_configuration ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Create mutex
	if( !( out->m_mutex = NLib_Mutex_NMutex_Construire( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NLib_Memoire_NListe_Detruire( &out->m_interfaceScanList );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy scanner service
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Service_Scanner_NGhostClientScannerService_Destroy( NGhostClientScannerService **this )
{
	// Destroy scanning thread
	if( (*this)->m_scanningThread != NULL )
		NLib_Thread_NThread_Detruire( &(*this)->m_scanningThread );

	// Destroy mutex
	NLib_Mutex_NMutex_Detruire( &(*this)->m_mutex );

	// Destroy interfaces list
	NLib_Memoire_NListe_Detruire( &(*this)->m_interfaceScanList );

	// Free
	NFREE( (*this) );
}

/**
 * Scanning thread (private)
 *
 * @param this
 * 		This instance
 *
 * @return if operation succeeded
 */
__PRIVATE __THREAD NBOOL NGhostClient_Service_Scanner_NGhostClientScannerService_ScanningThread( NGhostClientScannerService *this )
{
	// Scanner (one interface per interface)
	NDeviceScanner *scanner;

	// Scanner result set
	NDeviceScannerResultSet *resultSet;

	// Final result set (NListe<NDeviceScannerResultSet*>)
	NListe *finalResultSet;

	// Network interface
	const NInterfaceReseau *interface;

	// Iterator
	NU32 i;

#define STOP_SCANNER_RUN( ) \
	{ \
		/* Lock mutex */ \
		NLib_Mutex_NMutex_Lock( this->m_mutex ); \
 \
		/* We're not running anymore */ \
		this->m_isRunning = NFALSE; \
 \
		/* Save ending timestamp */ \
		this->m_endTimestamp = (NU32)NLib_Temps_ObtenirTimestamp( ); \
 \
		/* Unlock mutex */ \
		NLib_Mutex_NMutex_Unlock( this->m_mutex ); \
	}

	// Build final result set
	if( !( finalResultSet = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NDeviceScanner_Result_NDeviceScannerResultSet_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Stop scan
		STOP_SCANNER_RUN( );

		// Quit
		return NFALSE;
	}

	// Set to zero
	this->m_totalIPToScanCount = 0;
	this->m_currentScanIPCount = 0;
	this->m_currentFoundDeviceCount = 0;

	// Calculate total ip count
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_interfaceScanList ); i++ )
		// Get interface
		if( ( interface = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_interfaceScanList,
			i ) ) != NULL )
			// Add
			this->m_totalIPToScanCount += NLib_Module_Reseau_CalculerNombreIPDansCIDR( NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirCIDR( interface ) )
				// We don't scan network/broadcast address
				- 2;

	// Scan interface ip ranges
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_interfaceScanList ); i++ )
	{
		// Get interface
		if( ( interface = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_interfaceScanList,
			i ) ) != NULL )
		{
			// Update interface being scanned
			this->m_currentScanInterface = NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirNom( interface );

			// Create scanner
			if( !( scanner = NDeviceScanner_NDeviceScanner_Build( NGhostClient_Configuration_NGhostClientConfiguration_GetScannerThreadCount( this->m_configuration ),
				NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirAdresse( interface ),
				NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirCIDR( interface ),
				NGhostClient_Configuration_NGhostClientConfiguration_GetScannerConnectionTimeout( this->m_configuration ),
				NGhostClient_Configuration_NGhostClientConfiguration_GetScannerRequestTimeout( this->m_configuration ),
				NULL ) ) )
				// Ignore error
				continue;

			// Scan
			if( ( resultSet = NDeviceScanner_NDeviceScanner_Scan2( scanner,
				&this->m_currentScanIPCount,
				&this->m_isScanOperating,
				&this->m_currentFoundDeviceCount ) ) != NULL )
				// Add to final list
				NLib_Memoire_NListe_Ajouter( finalResultSet,
					resultSet );

			// Destroy scanner
			NDeviceScanner_NDeviceScanner_Destroy( &scanner );
		}
	}

	// Process results
	NGhostClient_NGhostClient_ProcessScannerResultSet( this->m_client,
		finalResultSet );

	// Clear results
	NLib_Memoire_NListe_Detruire( &finalResultSet );

	// Scan is complete
	STOP_SCANNER_RUN( );

	// OK
	return NTRUE;
}

/**
 * Run scan
 *
 * @param this
 * 		This instance
 *
 * @return if the scan run successfully
 */
__WILLLOCK __WILLUNLOCK NBOOL NGhostClient_Service_Scanner_NGhostClientScannerService_RunScan( NGhostClientScannerService *this )
{
	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Check running state
	if( this->m_isRunning )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Unlock mutex
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quit
		return NFALSE;
	}

	// We're now running scan
	this->m_isRunning = NTRUE;

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// Save starting time
	this->m_startTimestamp = NLib_Temps_ObtenirTimestamp( );

	// Destroy old thread
	if( this->m_scanningThread != NULL )
		NLib_Thread_NThread_Detruire( &this->m_scanningThread );

	// Create thread
	this->m_isScanOperating = NTRUE;
	if( !( this->m_scanningThread = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NGhostClient_Service_Scanner_NGhostClientScannerService_ScanningThread,
		this,
		&this->m_isScanOperating ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Unlock
		STOP_SCANNER_RUN( );

		// Quit
		return NFALSE;
	}
#undef STOP_SCANNER_RUN

	// Scan launched successfully
	return NTRUE;
}

/**
 * Stop scan
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Service_Scanner_NGhostClientScannerService_StopScan( NGhostClientScannerService *this )
{
	// Stop scan nicely
	this->m_isScanOperating = NFALSE;
}

/**
 * Get starting timestamp
 *
 * @param this
 * 		This instance
 *
 * @return the starting timestamp
 */
NU64 NGhostClient_Service_Scanner_NGhostClientScannerService_GetStartingTimestamp( const NGhostClientScannerService *this )
{
	return this->m_startTimestamp;
}

/**
 * Get ending timestamp
 *
 * @param this
 * 		This instance
 *
 * @return the ending timestamp
 */
NU64 NGhostClient_Service_Scaner_NGhostClientScannerService_GetEndingTimestamp( const NGhostClientScannerService *this )
{
	return this->m_endTimestamp;
}

/**
 * Is scanner currently running?
 *
 * @param this
 * 		This instance
 *
 * @return if the scanner is currently running
 */
NBOOL NGhostClient_Service_Scanner_NGhostClientScannerService_IsRunning( const NGhostClientScannerService *this )
{
	return this->m_isRunning;
}

/**
 * Is scanner operating?
 *
 * @param this
 * 		This instance
 *
 * @return if the scanner is operating
 */
NBOOL NGhostClient_Service_Scanner_NGhostClientScannerService_IsOperating( const NGhostClientScannerService *this )
{
	return this->m_isScanOperating;
}

/**
 * Get scanned interfaces list
 *
 * @param this
 * 		This instance
 *
 * @return the scanned interfaces list
 */
const NListe *NGhostClient_Service_Scanner_NGhostClientScannerService_GetScanInterfaceList( const NGhostClientScannerService *this )
{
	return this->m_interfaceScanList;
}

/**
 * Get total ip to scan count
 *
 * @param this
 * 		This instance
 *
 * @return the total count of ip to scan
 */
NU64 NGhostClient_Service_Scanner_NGhostClientScannerService_GetTotalIPToScanCount( const NGhostClientScannerService *this )
{
	return this->m_totalIPToScanCount;
}

/**
 * Get ip already scan count
 *
 * @param this
 * 		This instance
 *
 * @return the count of ip already scanned
 */
NU64 NGhostClient_Service_Scanner_NGhostClientScannerService_GetAlreadyScanIPCount( const NGhostClientScannerService *this )
{
	return this->m_currentScanIPCount;
}

/**
 * Get current interface being processed
 *
 * @param this
 * 		This instance
 *
 * @return the current interface being processed
 */
const char *NGhostClient_Service_Scanner_NGhostClientScannerService_GetCurrentInterfaceScan( const NGhostClientScannerService *this )
{
	return this->m_currentScanInterface;
}

/**
 * Get found devices count
 *
 * @param this
 * 		This instance
 *
 * @return the current found devices count
 */
NU32 NGhostClient_Service_Scanner_NGhostClientScannerService_GetFoundDeviceCount( const NGhostClientScannerService *this )
{
	return this->m_currentFoundDeviceCount;
}

