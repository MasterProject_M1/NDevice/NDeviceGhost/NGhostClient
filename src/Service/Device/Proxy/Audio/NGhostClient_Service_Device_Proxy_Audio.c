#include "../../../../../include/NGhostClient.h"

// -----------------------------------------------------
// namespace NGhostClient::Service::Device::Proxy::Audio
// -----------------------------------------------------

/**
 * Process a start request
 *
 * @param remoteDevice
 * 		The remote device
 * @param userData
 * 		The user data
 *
 * @return if the operation succeeded
 */
NBOOL NGhostClient_Service_Device_Proxy_Audio_ProcessStartRequest( NGhostRemoteDevice *remoteDevice,
	const NParserOutputList *userData )
{
	// HTTP request
	NRequeteHTTP *request;

	// Parser output list
	NParserOutputList *parserOutputList;

	// Parser output
	const NParserOutput *parserOutput;

	// Synch parser output list
	NParserOutputList *synchParserOutputList;

	// Json
	char *json;

	// Iterator
	NU32 i;

#define GET_USER_DATA( key, type, isFatal ) \
    { \
		/* Parser output to zero */ \
		parserOutput = NULL; \
 \
		/* Get parser output */ \
        if( ( !( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( userData, \
            (key), \
            NTRUE, \
            NTRUE ) ) \
            || NParser_Output_NParserOutput_GetType( parserOutput ) != (type) ) \
            && (isFatal) ) \
        { \
            /* Notify */ \
            NOTIFIER_ERREUR( NERREUR_SYNTAX ); \
 \
            /* Destroy request */ \
            NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Detruire( &request ); \
 \
            /* Destroy parser output list */ \
            NParser_Output_NParserOutputList_Destroy( &parserOutputList ); \
 \
            /* Quit */ \
            return NFALSE; \
        } \
    }

	// Build http request
	if( !( request = NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire2( NTYPE_REQUETE_HTTP_POST,
		NDEVICE_COMMON_TYPE_GHOST_AUDIO_AUDIO_SERVICE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Build parser output list
	if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy request
		NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Detruire( &request );

		// Quit
		return NFALSE;
	}

	/* Fill parser output list */
	// Name
		// Get
			GET_USER_DATA( NGhostClient_Service_Device_Proxy_Audio_NGhostClientAudioProxyParameter_GetName( NGHOST_CLIENT_AUDIO_PROXY_PARAMETER_NAME ),
				NPARSER_OUTPUT_TYPE_STRING,
				NTRUE );
		// Add
			NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				"name",
				NParser_Output_NParserOutput_GetValue( parserOutput ) );

	// Hostname
		// Get
			GET_USER_DATA( NGhostClient_Service_Device_Proxy_Audio_NGhostClientAudioProxyParameter_GetName( NGHOST_CLIENT_AUDIO_PROXY_PARAMETER_HOSTNAME ),
				NPARSER_OUTPUT_TYPE_STRING,
				NTRUE );
		// Add
			NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				"remote.hostname",
				NParser_Output_NParserOutput_GetValue( parserOutput ) );

	// Port
		// Get
			GET_USER_DATA( NGhostClient_Service_Device_Proxy_Audio_NGhostClientAudioProxyParameter_GetName( NGHOST_CLIENT_AUDIO_PROXY_PARAMETER_PORT ),
				NPARSER_OUTPUT_TYPE_INTEGER,
				NTRUE );
		// Add
			NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				"remote.port",
				*(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput ) );

	// Remote file path
		// Get
			GET_USER_DATA( NGhostClient_Service_Device_Proxy_Audio_NGhostClientAudioProxyParameter_GetName( NGHOST_CLIENT_AUDIO_PROXY_PARAMETER_FILE_PATH ),
				NPARSER_OUTPUT_TYPE_STRING,
				NTRUE );
		// Add
			NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				"remote.filePath",
				NParser_Output_NParserOutput_GetValue( parserOutput ) );

	// Is loop?
		// Get
			GET_USER_DATA( NGhostClient_Service_Device_Proxy_Audio_NGhostClientAudioProxyParameter_GetName( NGHOST_CLIENT_AUDIO_PROXY_PARAMETER_IS_LOOP ),
				NPARSER_OUTPUT_TYPE_BOOLEAN,
				NFALSE );
		// Add
			if( parserOutput != NULL )
				NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
					"isLoop",
					*(NBOOL*)NParser_Output_NParserOutput_GetValue( parserOutput ) );

	// Volume
		// Get
			GET_USER_DATA( NGhostClient_Service_Device_Proxy_Audio_NGhostClientAudioProxyParameter_GetName( NGHOST_CLIENT_AUDIO_PROXY_PARAMETER_VOLUME ),
				NPARSER_OUTPUT_TYPE_INTEGER,
				NFALSE );
		// Add
			if( parserOutput != NULL )
				NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
					"state.volume",
					*(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput ) );

	// The track will be started as soon as possible
		// Add
			NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
				"state.isPaused",
				NFALSE );

	// Synch list
	if( ( synchParserOutputList = NParser_Output_NParserOutputList_GetAllOutput( userData,
		NGhostClient_Service_Device_Proxy_Audio_NGhostClientAudioProxyParameter_GetName( NGHOST_CLIENT_AUDIO_PROXY_PARAMETER_SYNCH_LIST ),
		NTRUE,
		NTRUE ) ) != NULL )
	{
		// Iterate
		for( i = 0; i < NParser_Output_NParserOutputList_GetEntryCount( synchParserOutputList ); i++ )
			// Get entry
			if( ( parserOutput = NParser_Output_NParserOutputList_GetEntry( synchParserOutputList,
				i ) ) != NULL
				&& NParser_Output_NParserOutput_GetType( parserOutput ) == NPARSER_OUTPUT_TYPE_STRING )
				// Add entry
				NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					"synchList",
					NParser_Output_NParserOutput_GetValue( parserOutput ) );

		// Destroy
		NParser_Output_NParserOutputList_Destroy( &synchParserOutputList );
	}

	// Build json
	if( !( json = NJson_Engine_Builder_Build( parserOutputList ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Detruire( &request );
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NFALSE;
	}

	// Destroy parser output list
	NParser_Output_NParserOutputList_Destroy( &parserOutputList );

	// Set data
	NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterData( request,
		json,
		(NU32)strlen( json ) );

	// Free
	NFREE( json );

	// OK
	return NGhostClient_Device_Remote_NGhostRemoteDevice_SendRequest( remoteDevice,
		request,
		NDEVICE_COMMON_GHOST_CONNECTION_DEFAULT_TIMEOUT );
}
