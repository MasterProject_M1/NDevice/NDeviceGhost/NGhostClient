#define NGHOSTCLIENT_SERVICE_DEVICE_PROXY_AUDIO_NGHOSTCLIENTPROXYAUDIOSERVICE_INTERNE
#include "../../../../../include/NGhostClient.h"

// -------------------------------------------------------------------------------
// enum NGhostClient::Service::Device::Proxy::Audio::NGhostClientProxyAudioService
// -------------------------------------------------------------------------------

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service
 */
NGhostClientProxyAudioService NGhostClient_Service_Device_Proxy_Audio_NGhostClientProxyAudioService_FindService( const char *requestedElement,
	NU32 *cursor )
{
	// Output
	NGhostClientProxyAudioService out = (NGhostClientProxyAudioService)0;

	// Buffer
	char *buffer;

	// Read service name
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NGHOST_CLIENT_PROXY_AUDIO_SERVICES;
	}

	// Look for
	for( ; out < NGHOST_CLIENT_PROXY_AUDIO_SERVICES; out++ )
		// Check name
		if( NLib_Chaine_Comparer( buffer,
			NGhostClientProxyAudioServiceName[ out ],
			NTRUE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// Found it
			return out;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NGHOST_CLIENT_PROXY_AUDIO_SERVICES;
}

