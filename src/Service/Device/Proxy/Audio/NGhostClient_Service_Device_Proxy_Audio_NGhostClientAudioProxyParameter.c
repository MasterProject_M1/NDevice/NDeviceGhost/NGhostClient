#define NGHOSTCLIENT_SERVICE_DEVICE_PROXY_AUDIO_NGHOSTCLIENTAUDIOPROXYPARAMETER_INTERNE
#include "../../../../../include/NGhostClient.h"

// ---------------------------------------------------------------------------------
// enum NGhostClient::Service::Device::Proxy::Audio::NGhostClientAudioProxyParameter
// ---------------------------------------------------------------------------------

/**
 * Get parameter name
 *
 * @param parameter
 * 		The parameter
 *
 * @return the parameter name
 */
const char *NGhostClient_Service_Device_Proxy_Audio_NGhostClientAudioProxyParameter_GetName( NGhostClientAudioProxyParameter parameter )
{
	return NGhostClientAudioProxyParameterName[ parameter ];
}
