#include "../../../../../include/NGhostClient.h"

// ----------------------------------------------------------
// namespace NGhostClient::Service::Device::Proxy::DoorOpener
// ----------------------------------------------------------

/**
 * Process door opening
 *
 * @param deviceContainer
 * 		The device container
 *
 * @return if the operation succeeded
 */
NBOOL NGhostClient_Service_Device_Proxy_DoorOpener_ProcessDoorOpening( const NGhostDeviceContainer *deviceContainer )
{
	// HTTP client
	NClientHTTP *httpClient;

	// Result
	__OUTPUT NBOOL result;

	// Build client
	if( !( httpClient = NHTTP_Client_NClientHTTP_Construire( NGhostClient_Device_NGhostDeviceContainer_GetIP( deviceContainer ),
		NDeviceCommon_Type_NDeviceType_GetListeningPort( NGhostClient_Device_NGhostDeviceContainer_GetType( deviceContainer ) ),
		NULL ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Send request
	result = NHTTP_Client_NClientHTTP_EnvoyerRequete2( httpClient,
		NTYPE_REQUETE_HTTP_POST,
		NDEVICE_COMMON_TYPE_GHOST_DOOR_OPENER_URI,
		NULL,
		0,
		500 );

	// Wait for the packet to be sent
	while( NHTTP_Client_NClientHTTP_EstPacketAttente( httpClient ) )
		// Wait
		NLib_Temps_Attendre( 16 );

	// Destroy http client
	NHTTP_Client_NClientHTTP_Detruire( &httpClient );

	// OK
	return result;
}
