#define NGHOSTCLIENT_SERVICE_DEVICE_PROXY_DOOROPENER_NGHOSTDOOROPENERPROXYSERVICE_INTERNE
#include "../../../../../include/NGhostClient.h"

// -----------------------------------------------------------------------------------
// enum NGhostClient::Service::Device::Proxy::DoorOpener::NGhostDoorOpenerProxyService
// -----------------------------------------------------------------------------------

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 *
 * @return the service type or NGHOST_DOOR_OPENER_PROXY_SERVICES
 */
NGhostDoorOpenerProxyService NGhostClient_Service_Device_Proxy_DoorOpener_NGhostDoorOpenerProxyService_FindService( const char *requestedElement,
	NU32 *cursor )
{
	// Buffer
	char *buffer;

	// Service
	NGhostDoorOpenerProxyService serviceType;

	// Read
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NGHOST_DOOR_OPENER_PROXY_SERVICES;
	}

	// Look for service
	for( serviceType = (NGhostDoorOpenerProxyService)0; serviceType < NGHOST_DOOR_OPENER_PROXY_SERVICES; serviceType++ )
		// Check name
		if( NLib_Chaine_Comparer( buffer,
			NGhostDoorOpenerProxyServiceName[ serviceType ],
			NTRUE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// Quit
			return serviceType;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NGHOST_DOOR_OPENER_PROXY_SERVICES;
}

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostClient_Service_Device_Proxy_DoorOpener_NGhostDoorOpenerProxyService_GetName( NGhostDoorOpenerProxyService serviceType )
{
	return NGhostDoorOpenerProxyServiceName[ serviceType ];
}
