#include "../../../include/NGhostClient.h"

// ---------------------------------------
// namespace NGhostClient::Service::Device
// ---------------------------------------

/**
 * Build all devices parser output list
 *
 * @param deviceContainerList
 * 		The device container list
 * @param outputList
 * 		The output list
 *
 * @return the parser output list
 */
NBOOL NGhostClient_Service_Device_BuildParserOutputListAllDevice( const NGhostDeviceContainerList *deviceContainerList,
	__OUTPUT NParserOutputList *outputList )
{
	// Device container list duplicate
	NGhostDeviceContainerList *deviceContainerListDuplicate;

	// Device
	const NGhostDeviceContainer *deviceContainer;

	// Iterator
	NU32 i = 0;

	// Duplicate
	if( !( deviceContainerListDuplicate = NGhostClient_Device_NGhostDeviceContainerList_Duplicate( deviceContainerList ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Quit
		return NFALSE;
	}

	// Add known devices count
	NParser_Output_NParserOutputList_AddEntryInteger( outputList,
		NGhostClient_Service_Device_NGhostDeviceServiceList_GetJsonKey( NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_COUNT ),
		NGhostClient_Device_NGhostDeviceContainerList_GetDeviceCount( deviceContainerListDuplicate ) );

	// Iterate devices
	for( ; i < NGhostClient_Device_NGhostDeviceContainerList_GetDeviceCount( deviceContainerListDuplicate ); i++ )
		// Get device
		if( ( deviceContainer = NGhostClient_Device_NGhostDeviceContainerList_FindDeviceWithIndex( deviceContainerListDuplicate,
			i ) ) != NULL )
			// Add parser output details
			NGhostClient_Device_NGhostDeviceContainer_BuildParserOutputList( deviceContainer,
				outputList,
				NFALSE,
				i );

	// Destroy duplicate
	NGhostClient_Device_NGhostDeviceContainerList_Destroy( &deviceContainerListDuplicate );

	// OK
	return NTRUE;
}

/**
 * Build all devices response packet
 *
 * @param deviceContainerList
 * 		The device container list
 * @param clientID
 * 		The requesting client ID
 * @param requestingIP
 * 		The requesting IP
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostClient_Service_Device_BuildHTTPResponseDescribeAllDevice( const NGhostDeviceContainerList *deviceContainerList,
	NU32 clientID,
	const char *requestingIP )
{
	// Parser output list
	NParserOutputList *parserOutputList;

	// Create parser output list
	if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Build parser output
	if( !NGhostClient_Service_Device_BuildParserOutputListAllDevice( deviceContainerList,
		parserOutputList ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NULL;
	}

	// Add device informations
	if( !NGhostCommon_Service_Info_BuildParserOutputListInformation( NGhostCommon_NGhostCommon_GetDeviceInfo( NGhostClient_Device_NGhostDeviceContainerList_GetCommon( deviceContainerList ) ),
		parserOutputList,
		requestingIP ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NULL;
	}

	// OK
	return NDeviceCommon_Service_GenerateJsonHTTPResponseWithParserOutputList( NHTTP_CODE_200_OK,
		parserOutputList,
		clientID );
}

/**
 * Process a REST request (private)
 *
 * @param clientID
 * 		The client ID
 * @param requestedIP
 * 		The requesting IP
 * @param request
 * 		The http request
 * @param ghostDeviceContainerList
 * 		The ghost device container list
 * @param restMethod
 * 		The rest method
 *
 * @return the http response
 */
__PRIVATE __ALLOC NReponseHTTP *NGhostClient_Service_Device_ProcessRESTRequestInternal( NU32 clientID,
	const char *requestingIP,
	const NRequeteHTTP *request,
	const NGhostDeviceContainerList *ghostDeviceContainerList,
	NTypeRequeteHTTP restMethod )
{
	// Device container list duplicate
	NGhostDeviceContainerList *ghostDeviceContainerListSecure;

	// Device
	NGhostDeviceContainer *deviceContainer;

	// Service type
	NGhostServiceType serviceType;

	// Parsed data
	NParserOutputList *parserOutputList;

	// Corrected requested element
	char *correctedElement;

	// Cursor
	NU32 cursor = 0;

	// HTTP response
	__OUTPUT NReponseHTTP *response = NULL;

	// Determine service type
	switch( restMethod )
	{
		case NTYPE_REQUETE_HTTP_GET:
			serviceType = NGHOST_SERVICE_TYPE_GET_DEVICE_ROOT;
			break;
		case NTYPE_REQUETE_HTTP_PUT:
			serviceType = NGHOST_SERVICE_TYPE_PUT_DEVICE_ROOT;
			break;
		case NTYPE_REQUETE_HTTP_POST:
			serviceType = NGHOST_SERVICE_TYPE_POST_DEVICE_ROOT;
			break;

		default:
			// Notify
			NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

			// Quit
			return NULL;
	}

	// Correct element
	if( !( correctedElement = NGhostCommon_Service_CorrectElementToBeProcessed( NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElementInitial( request ),
		NGhostClient_Service_NGhostServiceType_GetAPIElement( serviceType ) ) )
		|| strlen( correctedElement ) <= 0
		|| NLib_Chaine_EstVide( correctedElement ) )
	{
		// Free
		NFREE( correctedElement );

		// Standard GET devices
		return NGhostClient_Service_Device_BuildHTTPResponseDescribeAllDevice( ghostDeviceContainerList,
			clientID,
			requestingIP );
	}

	// Duplicate the device container list
	if( !( ghostDeviceContainerListSecure = NGhostClient_Device_NGhostDeviceContainerList_Duplicate( ghostDeviceContainerList ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( correctedElement );

		// Quit
		return NULL;
	}

	// Get device
	if( !( deviceContainer = (NGhostDeviceContainer*)NGhostClient_Device_NGhostDeviceContainerList_GetDeviceToProcess( ghostDeviceContainerListSecure,
		correctedElement,
		&cursor ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Destroy duplicate
		NGhostClient_Device_NGhostDeviceContainerList_Destroy( &ghostDeviceContainerListSecure );

		// Free
		NFREE( correctedElement );

		// Quit
		return NULL;
	}

	// Process
	switch( restMethod )
	{
		case NTYPE_REQUETE_HTTP_GET:
			// Process
			response = NGhostClient_Device_NGhostDeviceContainer_ProcessRESTGetRequest( deviceContainer,
				correctedElement,
				&cursor,
				clientID );
			break;

		case NTYPE_REQUETE_HTTP_PUT:
		case NTYPE_REQUETE_HTTP_POST:
			// Parse data
			parserOutputList = NJson_Engine_Parser_Parse( NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirData( request ),
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirTailleData( request ) );

			// Process
			switch( restMethod )
			{
				case NTYPE_REQUETE_HTTP_PUT:
					response = NGhostClient_Device_NGhostDeviceContainer_ProcessRESTPutRequest( deviceContainer,
						correctedElement,
						&cursor,
						clientID,
						parserOutputList );
					break;
				case NTYPE_REQUETE_HTTP_POST:
					response = NGhostClient_Device_NGhostDeviceContainer_ProcessRESTPostRequest( deviceContainer,
						correctedElement,
						&cursor,
						clientID,
						parserOutputList );
					break;

				default:
					break;
			}

			// Destroy parser output list
			if( parserOutputList != NULL )
				NParser_Output_NParserOutputList_Destroy( &parserOutputList );
			break;

		default:
			break;
	}

	// Free
	NFREE( correctedElement );

	// Destroy duplicate
	NGhostClient_Device_NGhostDeviceContainerList_Destroy( &ghostDeviceContainerListSecure );

	// OK
	return response;
}

/**
 * Process a GET REST request
 *
 * @param clientID
 * 		The client ID
 * @param requestingIP
 * 		The requesting IP
 * @param request
 * 		The http request
 * @param ghostDeviceContainerList
 * 		The ghost device container list
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostClient_Service_Device_ProcessRESTGetRequest( NU32 clientID,
	const char *requestingIP,
	const NRequeteHTTP *request,
	const NGhostDeviceContainerList *ghostDeviceContainerList )
{
	return NGhostClient_Service_Device_ProcessRESTRequestInternal( clientID,
		requestingIP,
		request,
		ghostDeviceContainerList,
		NTYPE_REQUETE_HTTP_GET );
}

/**
 * Process a PUT REST request
 *
 * @param clientID
 * 		The client ID
 * @param requestingIP
 * 		The requesting ip
 * @param request
 * 		The http request
 * @param ghostDeviceContainerList
 * 		The ghost device container list
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostClient_Service_Device_ProcessRESTPUTRequest( NU32 clientID,
	const char *requestingIP,
	const NRequeteHTTP *request,
	const NGhostDeviceContainerList *ghostDeviceContainerList )
{
	return NGhostClient_Service_Device_ProcessRESTRequestInternal( clientID,
		requestingIP,
		request,
		ghostDeviceContainerList,
		NTYPE_REQUETE_HTTP_PUT );
}

/**
 * Process a POST REST request
 *
 * @param clientID
 * 		The client ID
 * @param requestingIP
 * 		The requesting IP
 * @param request
 * 		The http request
 * @param ghostDeviceContainerList
 * 		The ghost device container list
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostClient_Service_Device_ProcessRESTPOSTRequest( NU32 clientID,
	const char *requestingIP,
	const NRequeteHTTP *request,
	const NGhostDeviceContainerList *ghostDeviceContainerList )
{
	return NGhostClient_Service_Device_ProcessRESTRequestInternal( clientID,
		requestingIP,
		request,
		ghostDeviceContainerList,
		NTYPE_REQUETE_HTTP_POST );
}