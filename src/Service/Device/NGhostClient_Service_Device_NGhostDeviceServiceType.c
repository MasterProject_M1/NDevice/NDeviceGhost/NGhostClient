#define NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTDEVICESERVICETYPE_INTERNE
#include "../../../include/NGhostClient.h"

// -----------------------------------------------------------
// enum NGhostClient::Service::Device::NGhostDeviceServiceType
// -----------------------------------------------------------

/**
 * Get json key
 *
 * @param service
 * 		The service
 *
 * @return the json key
 */
const char *NGhostClient_Service_Device_NGhostDeviceServiceList_GetJsonKey( NGhostDeviceServiceType service )
{
	return NGhostDeviceServiceTypeJson[ service ];
}

/**
 * Get low level json key
 *
 * @param service
 * 		The service
 *
 * @return the json low level key
 */
const char *NGhostClient_Service_Device_NGhostDeviceServiceList_GetLowLevelJsonKey( NGhostDeviceServiceType service )
{
	return NGhostDeviceServiceTypeURI[ service ];
}

/**
 * Get service from devices/
 *
 * @param element
 * 		The element
 * @param cursor
 * 		The current cursor
 * @param isKeepPosition
 * 		Stay at current position?
 *
 * @return the service type
 */
NGhostDeviceServiceType NGhostClient_Service_Device_NGhostDeviceServiceList_FindService( const char *element,
	NU32 *cursor,
	NBOOL isKeepPosition )
{
	// Buffer
	char *buffer;

	// Iterator
	NU32 i = 0;

	// Read until '/'
	if( !( buffer = NLib_Chaine_LireJusqua( element,
		'/',
		cursor,
		isKeepPosition ) ) )
		return NGHOST_CLIENT_SERVICES_DEVICE;

	// Look for
	for( ; i < NGHOST_CLIENT_SERVICES_DEVICE; i++ )
		// Check
		if( NLib_Chaine_Comparer( buffer,
			NGhostDeviceServiceTypeURI[ i ],
			NFALSE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// OK
			return (NGhostDeviceServiceType)i;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NGHOST_CLIENT_SERVICES_DEVICE;
}

