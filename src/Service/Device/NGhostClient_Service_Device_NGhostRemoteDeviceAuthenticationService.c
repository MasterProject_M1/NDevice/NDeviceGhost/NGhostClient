#define NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTREMOTEDEVICEAUTHENTICATIONSERVICE_INTERNE
#include "../../../include/NGhostClient.h"

// ---------------------------------------------------------------------------
// enum NGhostClient::Service::Device::NGhostRemoteDeviceAuthenticationService
// ---------------------------------------------------------------------------

/**
 * Get service
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostClient_Service_Device_NGhostRemoteDeviceAuthenticationService_GetServiceName( NGhostRemoteDeviceAuthenticationService serviceType )
{
	return NGhostRemoteDeviceAuthenticationServiceURI[ serviceType ];
}

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 *
 * @return the service type
 */
NGhostRemoteDeviceAuthenticationService NGhostClient_Service_Device_NGhostRemoteDeviceAuthenticationService_FindService( const char *requestedElement,
	NU32 *cursor )
{
	// Iterator
	NGhostRemoteDeviceAuthenticationService i = (NGhostRemoteDeviceAuthenticationService)0;

	// Buffer
	char *buffer;

	// Extract path
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICES;
	}

	// Look for
	for( ; i < NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICES; i++ )
		// Check
		if( NLib_Chaine_Comparer( buffer,
			NGhostRemoteDeviceAuthenticationServiceURI[ i ],
			NTRUE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// Found it
			return i;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICES;
}
