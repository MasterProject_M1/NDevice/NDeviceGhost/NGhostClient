#define NGHOSTCLIENT_SERVICE_DEVICE_NGHOSTREMOTEDEVICESERVICE_INTERNE
#include "../../../include/NGhostClient.h"

// -------------------------------------------------------------
// enum NGhostClient::Service::Device::NGhostRemoteDeviceService
// -------------------------------------------------------------

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostClient_Service_Device_NGhostRemoteDeviceService_GetServiceName( NGhostRemoteDeviceService serviceType )
{
	return NGhostRemoteDeviceServiceURI[ serviceType ];
}

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostRemoteDeviceService NGhostClient_Service_Device_NGhostRemoteDeviceService_FindService( const char *requestedElement,
	NU32 *cursor )
{
	// Iterator
	__OUTPUT NGhostRemoteDeviceService i = (NGhostRemoteDeviceService)0;

	// Buffer
	char *buffer;

	// Read part
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NGHOST_REMOTE_DEVICE_SERVICES;
	}

	// Look for
	for( ; i < NGHOST_REMOTE_DEVICE_SERVICES; i++ )
		// Check
		if( NLib_Chaine_Comparer( buffer,
			NGhostRemoteDeviceServiceURI[ i ],
			NTRUE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// Found it
			return i;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NGHOST_REMOTE_DEVICE_SERVICES;
}

