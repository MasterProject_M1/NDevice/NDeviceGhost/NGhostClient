#include "../../../include/NGhostClient.h"

// --------------------------------------
// namespace NGhostClient::Device::Action
// --------------------------------------

/**
 * Build hue action internal
 *
 * @param actionType
 * 		The action type
 * @param parserOutputList
 * 		The parser output list
 * @param isFullPath
 * 		Is it the full path?
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostClient_Device_Action_BuildHUEActionListDataInternal( NGhostActionType actionType,
	NParserOutputList *parserOutputList,
	NBOOL isFullPath )
{
	// Process action type
	switch( actionType )
	{
		case NGHOST_ACTION_TYPE_HUE_LIGHT_ACTIVATE:
		case NGHOST_ACTION_ACTION_TYPE_HUE_LIGHT_DEACTIVATE:
		case NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_ACTIVATE:
		case NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_DEACTIVATE:
			// Add data
			NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
				isFullPath ?
					NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_IS_ON,
						"" )
					: NDeviceHUE_Service_NDeviceHUELightServiceType_GetNodeKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_IS_ON ),
				( actionType == NGHOST_ACTION_TYPE_HUE_LIGHT_ACTIVATE || actionType == NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_ACTIVATE ) ?
					NTRUE
					: NFALSE );
			break;
		case NGHOST_ACTION_TYPE_HUE_LIGHT_CHANGE_COLOR_RGB:
		case NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_CHANGE_COLOR_RGB:
			NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				isFullPath ?
					NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB,
						"" )
					: NDeviceHUE_Service_NDeviceHUELightServiceType_GetNodeKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB ),
				"#1#" );
			NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				isFullPath ?
					NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB,
						"" )
					: NDeviceHUE_Service_NDeviceHUELightServiceType_GetNodeKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB ),
				"#2#" );
			NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				isFullPath ?
					NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB,
						"" )
					: NDeviceHUE_Service_NDeviceHUELightServiceType_GetNodeKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB ),
				"#3#" );
			break;
		case NGHOST_ACTION_TYPE_HUE_LIGHT_CHANGE_BRIGHTNESS:
		case NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_CHANGE_BRIGHTNESS:
			NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				isFullPath ?
					NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_BRIGHTNESS,
						"" )
					: NDeviceHUE_Service_NDeviceHUELightServiceType_GetNodeKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_BRIGHTNESS ),
				"#1#" );
			break;

		case NGHOST_ACTION_TYPE_HUE_LIGHT_BLINK:
		case NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_BLINK:
		case NGHOST_ACTION_TYPE_HUE_LIGHT_BLINK_LONG:
		case NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_BLINK_LONG:
			// Add data
			NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				isFullPath ?
					NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_ALERT,
						"" )
					: NDeviceHUE_Service_NDeviceHUELightServiceType_GetNodeKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_ALERT ),
				( actionType == NGHOST_ACTION_TYPE_HUE_LIGHT_BLINK_LONG || actionType == NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_BLINK_LONG ) ?
					"lselect"
	   				: "select" );
			break;
		case NGHOST_ACTION_TYPE_HUE_LIGHT_COLOR_LOOP:
		case NGHOST_ACTION_TYPE_HUE_LIGHT_STOP_COLOR_LOOP:
		case NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_COLOR_LOOP:
		case NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_STOP_COLOR_LOOP:
			// Add data
			NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				isFullPath ?
					NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_EFFECT,
						"" )
					: NDeviceHUE_Service_NDeviceHUELightServiceType_GetNodeKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_EFFECT ),
				actionType == NGHOST_ACTION_TYPE_HUE_LIGHT_COLOR_LOOP || actionType == NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_COLOR_LOOP ?
					"colorloop"
					: "none" );
			break;

		default:
			return NFALSE;
	}

	// OK
	return NTRUE;
}
/**
 * Build hue actions
 *
 * @param deviceContainer
 * 		The device container
 *
 * @return the hue action list
 */
__PRIVATE __ALLOC NListe *NGhostClient_Device_Action_BuildHUEActionList( const void *deviceContainer )
{
	// HUE device
	NDeviceHUE *deviceHUE;

	// Light
	const NHUELight *hueLight;

	// Light list
	NHUELightList *lightList;

	// Action list
	NListe *actionList;

	// Iterator
	NU32 i;

	// Action
	NGhostAction *action;

	// Action type
	NGhostActionType actionType;

	// Requested element
	char requestedElement[ 256 ],
		requestedElementBasis[ 256 ];

	// Parser output list
	NParserOutputList *parserOutputList;

	// Sentence buffer
	char sentenceBuffer[ 2048 ];

#define BUILD_PARSER_OUTPUT_LIST( ) \
	if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) ) \
	{ \
		/* Unlock light list */ \
		NDeviceHUE_Light_NHUELightList_DeactivateProtection( lightList ); \
 \
		/* Notify */ \
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED ); \
 \
		/* Destroy action list */ \
		NLib_Memoire_NListe_Detruire( &actionList ); \
 \
		/* Quit */ \
		return NULL; \
	}

	// Get light list
	if( !( deviceHUE = NGhostClient_Device_NGhostDeviceContainer_GetDevice( deviceContainer ) )
		|| !( lightList = NDeviceHUE_NDeviceHUE_GetLightList( deviceHUE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NULL;
	}

	// Build action list
	if( !( actionList = NLib_Memoire_NListe_Construire( (void ( ___cdecl * )( void * ))NGhostCommon_Action_NGhostAction_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Lock light list
	NDeviceHUE_Light_NHUELightList_ActivateProtection( lightList );

	// Iterate action types
	for( actionType = (NGhostActionType)0; actionType < NGHOST_ACTION_TYPES; actionType++ )
	{
		switch( actionType )
		{
			case NGHOST_ACTION_TYPE_HUE_LIGHT_ACTIVATE:
			case NGHOST_ACTION_ACTION_TYPE_HUE_LIGHT_DEACTIVATE:
			case NGHOST_ACTION_TYPE_HUE_LIGHT_CHANGE_COLOR_RGB:
			case NGHOST_ACTION_TYPE_HUE_LIGHT_CHANGE_BRIGHTNESS:
			case NGHOST_ACTION_TYPE_HUE_LIGHT_BLINK:
			case NGHOST_ACTION_TYPE_HUE_LIGHT_BLINK_LONG:
			case NGHOST_ACTION_TYPE_HUE_LIGHT_COLOR_LOOP:
			case NGHOST_ACTION_TYPE_HUE_LIGHT_STOP_COLOR_LOOP:
				// Iterate lights
				for( i = 0; i < NDeviceHUE_Light_NHUELightList_GetLightCount( lightList ); i++ )
				{
					// Build parser output list
					BUILD_PARSER_OUTPUT_LIST( );

					// Get light
					if( !( hueLight = NDeviceHUE_Light_NHUELightList_GetLightByIndex( lightList,
						i ) ) )
						continue;

					// Element to be processed
					snprintf( requestedElementBasis,
						256,
						"/ghost/devices/%s/state/lights/%s/",
						NGhostClient_Device_NGhostDeviceContainer_GetUUID( deviceContainer ),
						NDeviceHUE_Light_NHUELight_GetName( hueLight ) );

					// Complete path
					switch( actionType )
					{
						case NGHOST_ACTION_TYPE_HUE_LIGHT_ACTIVATE:
						case NGHOST_ACTION_ACTION_TYPE_HUE_LIGHT_DEACTIVATE:
							snprintf( requestedElement,
								256,
								"%s%s",
								requestedElementBasis,
								NDeviceHUE_Service_NDeviceHUELightServiceType_GetRESTKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_IS_ON ) );
							break;

						case NGHOST_ACTION_TYPE_HUE_LIGHT_CHANGE_COLOR_RGB:
							snprintf( requestedElement,
								256,
								"%s%s",
								requestedElementBasis,
								NDeviceHUE_Service_NDeviceHUELightServiceType_GetRESTKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB ) );
							break;

						case NGHOST_ACTION_TYPE_HUE_LIGHT_CHANGE_BRIGHTNESS:
							snprintf( requestedElement,
								256,
								"%s%s",
								requestedElementBasis,
								NDeviceHUE_Service_NDeviceHUELightServiceType_GetRESTKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_BRIGHTNESS ) );
							break;
						case NGHOST_ACTION_TYPE_HUE_LIGHT_BLINK:
						case NGHOST_ACTION_TYPE_HUE_LIGHT_BLINK_LONG:
							snprintf( requestedElement,
								256,
								"%s%s",
								requestedElementBasis,
								NDeviceHUE_Service_NDeviceHUELightServiceType_GetRESTKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_ALERT ) );
							break;

						case NGHOST_ACTION_TYPE_HUE_LIGHT_COLOR_LOOP:
						case NGHOST_ACTION_TYPE_HUE_LIGHT_STOP_COLOR_LOOP:
							snprintf( requestedElement,
								256,
								"%s%s",
								requestedElementBasis,
								NDeviceHUE_Service_NDeviceHUELightServiceType_GetRESTKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_EFFECT ) );
							break;

						default:
							break;
					}

					// Create sentence
					snprintf( sentenceBuffer,
						2048,
						NGhostCommon_Action_NGhostActionType_GetSentence( actionType ),
						NDeviceHUE_Light_NHUELight_GetName( hueLight ) );

					// Build hue data
					if( !NGhostClient_Device_Action_BuildHUEActionListDataInternal( actionType,
						parserOutputList,
						NFALSE ) )
					{
						// Unlock light list
						NDeviceHUE_Light_NHUELightList_DeactivateProtection( lightList );

						// Notify
						NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

						// Destroy parser output list
						NParser_Output_NParserOutputList_Destroy( &parserOutputList );

						// Free
						NLib_Memoire_NListe_Detruire( &actionList );

						// Quit
						return NULL;
					}

					// Build action
					if( !( action = NGhostCommon_Action_NGhostAction_Build( "127.0.0.1",
						NGhostClient_Configuration_NGhostClientConfiguration_GetNetworkListeningPort( NGhostClient_Device_NGhostDeviceContainer_GetGhostClientConfiguration( deviceContainer ) ),
						requestedElement,
						NTYPE_REQUETE_HTTP_PUT,
						parserOutputList,
						actionType,
						sentenceBuffer,
						NULL ) ) ) // TODO change with correct UUID after presentation
					{
						// Unlock light list
						NDeviceHUE_Light_NHUELightList_DeactivateProtection( lightList );

						// Notify
						NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

						// Free
						NLib_Memoire_NListe_Detruire( &actionList );

						// Quit
						return NULL;
					}

					// Add action
					if( !NLib_Memoire_NListe_Ajouter( actionList,
						action ) )
					{
						// Unlock light list
						NDeviceHUE_Light_NHUELightList_DeactivateProtection( lightList );

						// Notify
						NOTIFIER_ERREUR( NERREUR_COPY );

						// Free
						NLib_Memoire_NListe_Detruire( &actionList );

						// Quit
						return NULL;
					}
				}
				break;

			case NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_ACTIVATE:
			case NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_DEACTIVATE:
			case NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_CHANGE_COLOR_RGB:
			case NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_CHANGE_BRIGHTNESS:
			case NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_BLINK:
			case NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_BLINK_LONG:
			case NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_COLOR_LOOP:
			case NGHOST_ACTION_TYPE_HUE_LIGHT_ALL_STOP_COLOR_LOOP:
				// Build parser output list
				BUILD_PARSER_OUTPUT_LIST( );

				// Element to be processed
				snprintf( requestedElement,
					256,
					"/ghost/devices/%s/state/lights",
					NGhostClient_Device_NGhostDeviceContainer_GetUUID( deviceContainer ) );

				// Create sentence
				strncpy( sentenceBuffer,
					NGhostCommon_Action_NGhostActionType_GetSentence( actionType ),
					2048 );

				// Build hue data
				if( !NGhostClient_Device_Action_BuildHUEActionListDataInternal( actionType,
					parserOutputList,
					NTRUE ) )
				{
					// Unlock light list
					NDeviceHUE_Light_NHUELightList_DeactivateProtection( lightList );

					// Notify
					NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

					// Free
					NLib_Memoire_NListe_Detruire( &actionList );

					// Quit
					return NULL;
				}

				// Build action
				if( !( action = NGhostCommon_Action_NGhostAction_Build( "127.0.0.1",
					NGhostClient_Configuration_NGhostClientConfiguration_GetNetworkListeningPort( NGhostClient_Device_NGhostDeviceContainer_GetGhostClientConfiguration( deviceContainer ) ),
					requestedElement,
					NTYPE_REQUETE_HTTP_PUT,
					parserOutputList,
					actionType,
					sentenceBuffer,
					NULL ) ) ) // TODO change with correct uuid after presentation
				{
					// Unlock light list
					NDeviceHUE_Light_NHUELightList_DeactivateProtection( lightList );

					// Notify
					NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

					// Free
					NLib_Memoire_NListe_Detruire( &actionList );

					// Quit
					return NULL;
				}

				// Add action
				if( !NLib_Memoire_NListe_Ajouter( actionList,
					action ) )
				{
					// Unlock light list
					NDeviceHUE_Light_NHUELightList_DeactivateProtection( lightList );

					// Notify
					NOTIFIER_ERREUR( NERREUR_COPY );

					// Free
					NLib_Memoire_NListe_Detruire( &actionList );

					// Quit
					return NULL;
				}
				break;

			default:
				break;
		}

	}

	// Unlock light list
	NDeviceHUE_Light_NHUELightList_DeactivateProtection( lightList );

	// OK
	return actionList;
}

/**
 * Build door opener action list
 *
 * @param deviceContainer
 * 		The device container
 *
 * @return the door action list
 */
__PRIVATE __ALLOC NListe *NGhostClient_Device_Action_BuildDoorOpenerActionList( const void *deviceContainer )
{
	// Ghost action action list
	NListe *out;

	// Action
	NGhostAction *action;

	// Path
	char path[ 256 ];

	// Sentence
	char sentence[ 256 ];

	// Allocate memory
	if( !( out = NLib_Memoire_NListe_Construire( (void ( ___cdecl * )( void * ))NGhostCommon_Action_NGhostAction_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Build opening path
	snprintf( path,
		256,
		"/ghost/devices/%s/state/%s",
		NGhostClient_Device_NGhostDeviceContainer_GetUUID( deviceContainer ),
		NGhostClient_Service_Device_Proxy_DoorOpener_NGhostDoorOpenerProxyService_GetName( NGHOST_DOOR_OPENER_PROXY_SERVICE_OPEN ) );

	// Build sentence
	snprintf( sentence,
		256,
		NGhostCommon_Action_NGhostActionType_GetSentence( NGHOST_ACTION_TYPE_DOOR_OPEN ),
		NGhostClient_Device_NGhostDeviceContainer_GetName( deviceContainer ) );

	// Build opening door action
	if( !( action = NGhostCommon_Action_NGhostAction_Build( "127.0.0.1",
		NGhostClient_Configuration_NGhostClientConfiguration_GetNetworkListeningPort( NGhostClient_Device_NGhostDeviceContainer_GetGhostClientConfiguration( deviceContainer ) ),
		path,
		NTYPE_REQUETE_HTTP_POST,
		NULL,
		NGHOST_ACTION_TYPE_DOOR_OPEN,
		sentence,
		NULL ) ) ) // TODO Change with correct UUID after presentation
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NLib_Memoire_NListe_Detruire( &out );

		// Quit
		return NULL;
	}

	// Add action
	NLib_Memoire_NListe_Ajouter( out,
		action );

	// OK
	return out;
}

/**
 * Build mFi mPower action list
 *
 * @param deviceContainer
 * 		The device container
 *
 * @return the built actions list
 */
__ALLOC NListe *NGhostClient_Device_Action_BuildMFIMPowerActionList( const NGhostDeviceContainer *deviceContainer )
{
	// mFi mPower device
	NDeviceMFI *mfi;

	// Outlet list
	NOutletList *outletList;

	// Outlet
	const NOutlet *outlet;

	// Output
	NListe *actionList;

	// Action
	NGhostAction *ghostAction;

	// Parser output list
	NParserOutputList *parserOutputList;

	// Iterator
	NU32 i;

	// Action type
	NGhostActionType actionType;

	// Element
	char element[ 256 ];

	// Sentence
	char sentence[ 256 ];

	// Buffer
	char buffer[ 256 ];

	// Get mfi device
	if( !( mfi = NGhostClient_Device_NGhostDeviceContainer_GetDevice( deviceContainer ) )
		|| !( outletList = NDeviceMFI_NDeviceMFI_GetOutletList( mfi ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NULL;
	}

	// Build list
	if( !( actionList = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NGhostCommon_Action_NGhostAction_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Lock
	NDeviceMFI_Outlet_NOutletList_ActivateProtection( outletList );

	// Iterate
	for( i = 0; i < NDeviceMFI_Outlet_NOutletList_GetOutletCount( outletList ); i++ )
		// Get outlet
		if( ( outlet = NDeviceMFI_Outlet_NOutletList_GetOutlet( outletList,
			i ) ) != NULL )
			for( actionType = NGHOST_ACTION_TYPE_OUTLET_ACTIVATE; actionType < NGHOST_ACTION_TYPE_OUTLET_DEACTIVATE + 1; actionType++ )
			{
				// Build parser output list
				if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
				{
					// Notify
					NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

					// Destroy list
					NLib_Memoire_NListe_Detruire( &actionList );

					// Quit
					return NULL;
				}

				// Element
				snprintf( element,
					256,
					"/ghost/devices/%s/state/list/%d/isOn",
					NGhostClient_Device_NGhostDeviceContainer_GetUUID( deviceContainer ),
					i );

				// Sentence
				snprintf( buffer,
					256,
					"%d",
					NDeviceMFI_Outlet_NOutlet_GetPort( outlet ) );
				snprintf( sentence,
					256,
					"%s %s",
					NGhostCommon_Action_NGhostActionType_GetSentence( actionType ),
					NDeviceMFI_Outlet_NOutlet_GetName( outlet ) != NULL ?
						NDeviceMFI_Outlet_NOutlet_GetName( outlet )
						: buffer );

				// Add output option
				NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
					NDeviceMFI_Service_NMFIOutletService_GetName( NMFI_OUTLET_SERVICE_IS_ON ),
					(NBOOL)( actionType == NGHOST_ACTION_TYPE_OUTLET_ACTIVATE ) );

				// Build action
				if( !( ghostAction = NGhostCommon_Action_NGhostAction_Build( "127.0.0.1",
					NGhostClient_Configuration_NGhostClientConfiguration_GetNetworkListeningPort( NGhostClient_Device_NGhostDeviceContainer_GetGhostClientConfiguration( deviceContainer ) ),
					element,
					NTYPE_REQUETE_HTTP_POST,
					parserOutputList,
					actionType,
					sentence,
					NULL ) ) ) // TODO change with correct UUID after presentation
				{
					// Notify
					NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

					// Destroy list
					NLib_Memoire_NListe_Detruire( &actionList );

					// Quit
					return NULL;
				}

				// Add to list
				NLib_Memoire_NListe_Ajouter( actionList,
					ghostAction );
			}

	// Unlock
	NDeviceMFI_Outlet_NOutletList_DeactivateProtection( outletList );

	// OK
	return actionList;
}

/**
 * Action update thread
 *
 * @param ghostActionUpdater
 * 		Ghost action updater instance
 *
 * @return if the operation succeeded
 */
__THREAD NBOOL NGhostClient_Device_Action_UpdateThread( NGhostActionUpdater *actionUpdater )
{
	// Action list
	NListe *builtActionList = NULL;

	// Action
	NGhostAction *newAction;

	// Ghost action list
	NGhostActionList *ghostActionList;

	// Device container
	const NGhostDeviceContainer *ghostDeviceContainer;

	// Get data
	if( !( ghostActionList = NGhostCommon_Action_NGhostActionUpdater_GetActionList( actionUpdater ) )
		|| !( ghostDeviceContainer = NGhostCommon_Action_NGhostActionUpdater_GetDevice( actionUpdater ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	// Update loop
	do
	{
		// Build
		switch( NGhostClient_Device_NGhostDeviceContainer_GetType( ghostDeviceContainer ) )
		{
			case NDEVICE_TYPE_HUE:
				builtActionList = NGhostClient_Device_Action_BuildHUEActionList( ghostDeviceContainer );
				break;

			case NDEVICE_TYPE_GHOST_DOOR_OPENER:
				builtActionList = NGhostClient_Device_Action_BuildDoorOpenerActionList( ghostDeviceContainer );
				break;

			case NDEVICE_TYPE_MFI_MPOWER:
				builtActionList = NGhostClient_Device_Action_BuildMFIMPowerActionList( ghostDeviceContainer );
				break;

			default:
				break;
		}

		// Build action list
		if( builtActionList != NULL )
		{
			// Lock list
			NLib_Memoire_NListe_ActiverProtection( ghostActionList->m_list );

			// Update
			while( NLib_Memoire_NListe_ObtenirNombre( builtActionList  ) > 0 )
				if( ( newAction = (NGhostAction*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( builtActionList,
					0 ) ) != NULL )
				{
					// Add action
					NGhostCommon_Action_NGhostActionList_AddAction( ghostActionList,
						newAction );

					// Remove element from list without freeing it
					NLib_Memoire_NListe_SupprimerDepuisIndex2( builtActionList,
						0,
						NFALSE );
				}

			// Unlock list
			NLib_Memoire_NListe_DesactiverProtection( ghostActionList->m_list );

			// Destroy new action list
			NLib_Memoire_NListe_Detruire( &builtActionList );
		}

		// Wait
		NLib_Temps_Attendre( NGHOST_CLIENT_DEVICE_ACTION_DELAY_BETWEEN_UPDATE );
	} while( NGhostCommon_Action_NGhostActionUpdater_IsUpdateThreadRunning( actionUpdater ) );

	// OK
	return NTRUE;
}
