#include "../../../include/NGhostClient.h"

// -------------------------------------------------------
// struct NGhostClient::Device::Remote::NGhostRemoteDevice
// -------------------------------------------------------

/**
 * Follow packet to token gesture (private)
 *
 * @param packet
 * 		The received packet
 * @param client
 * 		The receiver client
 *
 * @return if operation succeeded
 */
__PRIVATE __CALLBACK NBOOL NGhostClient_Device_Remote_NGhostRemoteDevice_CallbackPacketReceive( const NPacket *packet,
	const NClient *client )
{
	// HTTP client
	NClientHTTP *httpClient;

	// Remote device
	NGhostRemoteDevice *this;

	// Get data
	if( !( httpClient = NLib_Module_Reseau_Client_NClient_ObtenirDataUtilisateur( client ) )
		|| !( this = NHTTP_Client_NClientHTTP_ObtenirDataUtilisateur( httpClient ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	// Token receive
	if( !NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_IsAuthenticated( this->m_authentication ) )
		// Process token reception
		return NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_CallbackPacketReceive( packet,
			client );
	else
		// OK
		return NTRUE;
}

/**
 * Share watcher ip
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The ip to share
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostClient_Device_Remote_NGhostRemoteDevice_ShareReferal( NGhostRemoteDevice *this,
	const char *ip )
{
	// Data
	char data[ 128 ];

	// Authentication entry
	const NAuthenticationEntry *authenticationEntry;

	// Check parameter
	if( !ip
		|| !( authenticationEntry = NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_GetAuthenticationEntry( this->m_authentication ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

		// Quit
		return NFALSE;
	}

	// Build data
	snprintf( data,
		128,
		"{ \"watcher\": \"%s\" }",
		ip );

	// Send request
	return (NBOOL)( NHTTP_Client_NClientHTTP_EnvoyerRequete3( this->m_httpClient,
		NTYPE_REQUETE_HTTP_POST,
		"/ghost/status/referal/watcher",
		data,
		(NU32)strlen( data ),
		NDEVICE_COMMON_AUTHENTICATION_BROADCAST_CONNECTION_TIMEOUT,
		NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( authenticationEntry ),
		NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( authenticationEntry ) != NULL ?
			NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( authenticationEntry )
			: "" )
			&& NGhostClient_Device_Remote_NGhostRemoteAuthentication_NotifyClient( this->m_authentication ) );
}

/**
 * Share watchers
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostClient_Device_Remote_NGhostRemoteDevice_FindAndShareWatcher( NGhostRemoteDevice *this )
{
	// IP list
	NListe *ipList;

	// Iterator
	NU32 i;

	// Result
	__OUTPUT NBOOL result = NTRUE;

	// Lock device list
	NGhostClient_Device_NGhostDeviceContainerList_ActivateProtection( this->m_deviceContainerList );

	// Get watchers IPs list
	ipList = NGhostClient_Device_NGhostDeviceContainerList_FindGhostWatcherIPList( this->m_deviceContainerList );

	// Unlock device list
	NGhostClient_Device_NGhostDeviceContainerList_DeactivateProtection( this->m_deviceContainerList );

	// Process IPs list
	if( ipList != NULL )
	{
		// Iterate ips
		for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( ipList ); i++ )
			// Process ip
			result = (NBOOL)( NGhostClient_Device_Remote_NGhostRemoteDevice_ShareReferal( this,
				NLib_Memoire_NListe_ObtenirElementDepuisIndex( ipList,
					i ) ) && result );

		// Destroy ip list
		NLib_Memoire_NListe_Detruire( &ipList );
	}

	// OK?
	return result;
}

/**
 * Share authentication
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostClient_Device_Remote_NGhostRemoteDevice_ShareAuthentication( NGhostRemoteDevice *this )
{
	// Authentication entry
	const NAuthenticationEntry *authenticationEntry;

	// Check authentication
	if( !NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_IsAuthenticated( this->m_authentication )
		|| !( authenticationEntry = NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_GetAuthenticationEntry( this->m_authentication ) ) )
		return NFALSE;

	// Share
	return NDeviceCommon_Authentication_NAuthenticationManager_SendAllAuthentication( this->m_authenticationManager,
		this->m_httpClient,
		NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( authenticationEntry ) );
}

/**
 * Update thread
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
__PRIVATE __THREAD NBOOL NGhostClient_Device_Remote_NGhostRemoteDevice_UpdateThread( NGhostRemoteDevice *this )
{
	do
	{
		// Is device authenticated?
		if( NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_IsAuthenticated( this->m_authentication ) )
		{
			// Share watcher
			NGhostClient_Device_Remote_NGhostRemoteDevice_FindAndShareWatcher( this );

			// Share authentication
			NGhostClient_Device_Remote_NGhostRemoteDevice_ShareAuthentication( this );
		}

		// Wait
		NLib_Temps_Attendre( NGHOST_CLIENT_DEVICE_REMOTE_DELAY_BETWEEN_UPDATE );
	} while( this->m_isUpdateThreadRunning );

	// OK
	return NTRUE;
}

/**
 * Build ghost remote device
 *
 * @param authenticationManager
 * 		The authentication manager
 * @param contactingIP
 * 		The contacting IP
 * @param hostname
 * 		The remote ghost hostname
 * @param port
 * 		The remote ghost port
 * @param deviceType
 * 		The remote device type
 * @param deviceContainerList
 * 		The device container list
 *
 * @return the ghost remote device instance
 */
__ALLOC NGhostRemoteDevice *NGhostClient_Device_Remote_NGhostRemoteDevice_Build( NAuthenticationManager *authenticationManager,
	const char *contactingIP,
	const char *hostname,
	NU32 port,
	NDeviceType deviceType,
	NGhostDeviceContainerList *deviceContainerList )
{
	// Output
	__OUTPUT NGhostRemoteDevice *out;

	// Check parameter
	if( !contactingIP )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quit
		return NULL;
	}

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostRemoteDevice ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Save data
	out->m_deviceContainerList = deviceContainerList;
	out->m_authenticationManager = authenticationManager;
	if( !( out->m_contactingIP = NLib_Chaine_Dupliquer( contactingIP ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build http client
	if( !( out->m_httpClient = NHTTP_Client_NClientHTTP_Construire2( hostname,
		port,
		out,
		NGhostClient_Device_Remote_NGhostRemoteDevice_CallbackPacketReceive ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out->m_contactingIP );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Set delay between two emissions
	NHTTP_Client_NClientHTTP_DefinirDelaiEntreEnvoiDeuxRequete( out->m_httpClient,
		NGHOST_CLIENT_DEVICE_REMOTE_DELAY_BETWEEN_TWO_HTTP_PACKET );

	// Build authentication
	if( !( out->m_authentication = NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_Build( hostname,
		port,
		authenticationManager,
		deviceType,
		out->m_httpClient,
		deviceContainerList,
		out->m_contactingIP ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy client
		NHTTP_Client_NClientHTTP_Detruire( &out->m_httpClient );

		// Free
		NFREE( out->m_contactingIP );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Run thread
	out->m_isUpdateThreadRunning = NTRUE;
	if( !( out->m_updateThread = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NGhostClient_Device_Remote_NGhostRemoteDevice_UpdateThread,
		out,
		&out->m_isUpdateThreadRunning ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Authentication
		NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_Destroy( &out->m_authentication );

		// Destroy client
		NHTTP_Client_NClientHTTP_Detruire( &out->m_httpClient );

		// Free
		NFREE( out->m_contactingIP );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy remote device
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Device_Remote_NGhostRemoteDevice_Destroy( NGhostRemoteDevice **this )
{
	// Destroy thread
	NLib_Thread_NThread_Detruire( &(*this)->m_updateThread );

	// Destroy authentication
	NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_Destroy( &(*this)->m_authentication );

	// Destroy HTTP client
	NHTTP_Client_NClientHTTP_Detruire( &(*this)->m_httpClient );

	// Free
	NFREE( (*this)->m_contactingIP );
	NFREE( (*this) );
}

/**
 * Get remote authentication
 *
 * @param this
 * 		This instance
 *
 * @return the remote authentication
 */
const NGhostRemoteDeviceAuthentication *NGhostClient_Device_Remote_NGhostRemoteDevice_GetRemoteAuthentication( const NGhostRemoteDevice *this )
{
	return this->m_authentication;
}

/**
 * Get contacting IP
 *
 * @param this
 * 		This instance
 *
 * @return the contacting IP
 */
const char *NGhostClient_Device_Remote_NGhostRemoteDevice_GetContactingIP( const NGhostRemoteDevice *this )
{
	return this->m_contactingIP;
}

/**
 * Build parser output list (private)
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 * @param service
 * 		The requested service
 *
 * @return if operation succeeded
 */
__PRIVATE NBOOL NGhostClient_Device_Remote_NGhostRemoteDevice_BuildParserOutputListInternal( const NGhostRemoteDevice *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList,
	NGhostRemoteDeviceService service )
{
	// Key
	char key[ 128 ];

	// Build key
	snprintf( key,
		128,
		"%s%s%s",
		keyRoot,
		strlen( keyRoot ) > 0 ?
			"."
			: "",
		NGhostClient_Service_Device_NGhostRemoteDeviceService_GetServiceName( service ) );

	// Add
	switch( service )
	{
		default:
			return NFALSE;

		case NGHOST_REMOTE_DEVICE_SERVICE_AUTHENTICATION:
			return NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_BuildParserOutputList( this->m_authentication,
				key,
				parserOutputList );

		case NGHOST_REMOTE_DEVICE_SERVICE_DEVICE_TYPE:
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				NDeviceCommon_Type_NDeviceType_GetName( NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_GetDeviceType( this->m_authentication ) ) );
		case NGHOST_REMOTE_DEVICE_SERVICE_DEVICE:
			break;
	}

	// OK
	return NTRUE;
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
NBOOL NGhostClient_Device_Remote_NGhostRemoteDevice_BuildParserOutputList( const NGhostRemoteDevice *this,
	const char *key,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Iterator
	NGhostRemoteDeviceService service = (NGhostRemoteDeviceService)0;

	// Fill with data
	for( ; service < NGHOST_REMOTE_DEVICE_SERVICES; service++ )
		// Add data
		NGhostClient_Device_Remote_NGhostRemoteDevice_BuildParserOutputListInternal( this,
			key,
			parserOutputList,
			service );

	// OK
	return NTRUE;
}

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 *
 * @return if operation succeeded
 */
NBOOL NGhostClient_Device_Remote_NGhostRemoteDevice_ProcessRESTGetRequest( const NGhostRemoteDevice *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *requestedElement,
	NU32 *cursor )
{
	// Service type
	NGhostRemoteDeviceService service;

	// Find service type
	switch( service = NGhostClient_Service_Device_NGhostRemoteDeviceService_FindService( requestedElement,
		cursor ) )
	{
		default:
			return NFALSE;

		case NGHOST_REMOTE_DEVICE_SERVICE_ROOT:
			return NGhostClient_Device_Remote_NGhostRemoteDevice_BuildParserOutputList( this,
				"",
				parserOutputList );

		case NGHOST_REMOTE_DEVICE_SERVICE_AUTHENTICATION:
			return NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_ProcessRESTGetRequest( this->m_authentication,
				parserOutputList,
				requestedElement,
				cursor );

		case NGHOST_REMOTE_DEVICE_SERVICE_DEVICE_TYPE:
		case NGHOST_REMOTE_DEVICE_SERVICE_DEVICE:
			return NGhostClient_Device_Remote_NGhostRemoteDevice_BuildParserOutputListInternal( this,
				"",
				parserOutputList,
				service );
	}
}

/**
 * Send request
 *
 * @param this
 * 		This instance
 * @param request
 * 		The http request
 * @param requestTimeout
 * 		The request timeout
 *
 * @return if the operation succeeded
 */
NBOOL NGhostClient_Device_Remote_NGhostRemoteDevice_SendRequest( NGhostRemoteDevice *this,
	__WILLBEOWNED NRequeteHTTP *request,
	NU32 requestTimeout )
{
	// Remote device
	const NAuthenticationEntry *authenticationEntry;

	// Token
	const char *token;

	// Base64 encoded token
	char *encodedToken;

	// Buffer
	char buffer[ 256 ];

	// Get token
	if( !NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_IsAuthenticated( this->m_authentication )
		|| !( authenticationEntry = NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_GetAuthenticationEntry( this->m_authentication ) )
		|| !( token = NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( authenticationEntry ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_CANT_PROCESS_REQUEST );

		// Quit
		return NFALSE;
	}

	// Build token
	snprintf( buffer,
		256,
		"%s:",
		token );

	// Base64 encode
	if( !( encodedToken = NLib_Chaine_ConvertirBase64( buffer,
		(NU32)strlen( buffer ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quit
		return NFALSE;
	}

	// Fill buffer
	snprintf( buffer,
		256,
		"Basic %s",
		encodedToken );

	// Remove authentication attribute
	NHTTP_Commun_Traitement_Requete_NRequeteHTTP_RemplacerAttribut( request,
		NMOT_CLEF_REQUETE_HTTP_KEYWORD_AUTHORIZATION,
		buffer );

	// Free
	NFREE( encodedToken );

	// Add authentication attribute
	NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
		NMOT_CLEF_REQUETE_HTTP_KEYWORD_AUTHORIZATION,
		buffer );

	// Send request
	return NHTTP_Client_NClientHTTP_EnvoyerRequete( this->m_httpClient,
		request,
		requestTimeout );
}
