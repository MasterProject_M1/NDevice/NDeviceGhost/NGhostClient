#include "../../../include/NGhostClient.h"

// ---------------------------------------------------------------------
// struct NGhostClient::Device::Remote::NGhostRemoteDeviceAuthentication
// ---------------------------------------------------------------------

/**
 * Check if authentication was manually added in authentication manager (private)
 *
 * @param this
 * 		This instance
 */
__PRIVATE void NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_CheckAuthenticationPresentInAuthenticationManager( NGhostRemoteDeviceAuthentication *this )
{
	// Entry
	NAuthenticationEntry *entry;

	// Lock authentication manager
	NDeviceCommon_Authentication_NAuthenticationManager_ActivateProtection( this->m_authenticationManager );

	// Look for
	if( ( entry = (NAuthenticationEntry*)NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationForHostnameAndPort( this->m_authenticationManager,
		this->m_hostname,
		this->m_port ) ) != NULL )
	{
		// Set read only flag
		NDeviceCommon_Authentication_NAuthenticationEntry_SetReadOnly( entry );

		// Save
		this->m_authenticationEntry = entry;
	}

	// Unlock authentication manager
	NDeviceCommon_Authentication_NAuthenticationManager_DeactivateProtection( this->m_authenticationManager );
}

/**
 * Send authentication details (private)
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostClient_Device_Remote_NGhostRemoteAuthentication_SendAuthenticationList( const NGhostRemoteDeviceAuthentication *this )
{
	// Check authentication entry
	if( !this->m_authenticationEntry )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	// Send authentication
	return NDeviceCommon_Authentication_NAuthenticationManager_SendAllAuthentication( this->m_authenticationManager,
		(NClientHTTP*)this->m_httpClient,
		NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( this->m_authenticationEntry ) );
}

/**
 * Notify we are here to this instance
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
NBOOL NGhostClient_Device_Remote_NGhostRemoteAuthentication_NotifyClient( const NGhostRemoteDeviceAuthentication *this )
{
	// Data
	char data[ 128 ];

	// Check authentication entry
	if( !this->m_authenticationEntry )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	// Build data
	snprintf( data,
		128,
		"{ \"client\": \"%s\" }",
		this->m_contactingIP );

	// Send request
	return NHTTP_Client_NClientHTTP_EnvoyerRequete3( (NClientHTTP*)this->m_httpClient,
		NTYPE_REQUETE_HTTP_POST,
		"/ghost/status/referal/client",
		data,
		(NU32)strlen( data ),
		NDEVICE_COMMON_AUTHENTICATION_BROADCAST_CONNECTION_TIMEOUT,
		NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( this->m_authenticationEntry ),
		NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( this->m_authenticationEntry ) != NULL ?
			NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( this->m_authenticationEntry )
			: "" );
}

/**
 * Send initial data
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostClient_Device_Remote_NGhostRemoteAuthentication_SendInitialDetail( const NGhostRemoteDeviceAuthentication *this )
{
	return (NBOOL)( NGhostClient_Device_Remote_NGhostRemoteAuthentication_SendAuthenticationList( this )
		&& NGhostClient_Device_Remote_NGhostRemoteAuthentication_NotifyClient( this ) );
}

/**
 * Authentication thread (private)
 *
 * @param this
 * 		This instance
 *
 * @return if operation succeeded
 */
__PRIVATE __THREAD NBOOL NGhostClient_Device_Remote_NGhostRemoteAuthentication_AuthenticationThread( NGhostRemoteDeviceAuthentication *this )
{
	// Authentication thread
	do
	{
		// Check in authentication manager if present (manual add)
		NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_CheckAuthenticationPresentInAuthenticationManager( this );

		// Send http request
		NHTTP_Client_NClientHTTP_EnvoyerRequete2( (NClientHTTP*)this->m_httpClient,
			NTYPE_REQUETE_HTTP_GET,
			NDEVICE_COMMON_TYPE_GHOST_TOKEN_MANAGER,
			NULL,
			0,
			NGHOSTCLIENT_DEVICE_REMOTE_AUTHENTICATION_CONNECTION_TIMEOUT );

		// Increase retry count
		this->m_authenticationRetryCount++;

		// Save last retry timestamp
		this->m_lastAuthenticationAttemptTimestamp = NLib_Temps_ObtenirTimestamp( );

		// Are we running?
		if( this->m_isAuthenticationThreadRunning )
			// Wait
			NLib_Temps_Attendre( NGHOSTCLIENT_DEVICE_REMOTE_AUTHENTICATION_CONNECTION_RETRY_DELAY );
	} while( this->m_isAuthenticationThreadRunning
		&& !NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_IsAuthenticated( this ) );

	// Share authentications
	if( this->m_isAuthenticationThreadRunning )
		NGhostClient_Device_Remote_NGhostRemoteAuthentication_SendInitialDetail( this );

	// OK
	return NTRUE;
}

/**
 * Build authentication
 *
 * @param hostname
 * 		The remote hostname
 * @param port
 * 		The remote port
 * @param authenticationManager
 * 		The authentication manager
 * @param deviceType
 * 		The device type
 * @param httpClient
 * 		The http client
 * @param deviceContainerList
 * 		The device container list
 * @param contactingIP
 * 		The IP used to contact the device (from our end) The value is stored in NGhostRemoteDevice
 *
 * @return the device authentication instance
 */
__ALLOC NGhostRemoteDeviceAuthentication *NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_Build( const char *hostname,
	NU32 port,
	NAuthenticationManager *authenticationManager,
	NDeviceType deviceType,
	const NClientHTTP *httpClient,
	NGhostDeviceContainerList *deviceContainerList,
	const char *contactingIP )
{
	// Output
	__OUTPUT NGhostRemoteDeviceAuthentication *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostRemoteDeviceAuthentication ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Duplicate hostname/ip
	if( !( out->m_hostname = NLib_Chaine_Dupliquer( hostname ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save
	out->m_port = port;
	out->m_authenticationManager = authenticationManager;
	out->m_lastAuthenticationAttemptTimestamp = NLib_Temps_ObtenirTimestamp( );
	out->m_deviceType = deviceType;
	out->m_httpClient = httpClient;
	out->m_contactingIP = contactingIP;
	out->m_deviceContainerList = deviceContainerList;

	// Check authentication manager
	NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_CheckAuthenticationPresentInAuthenticationManager( out );

	// Check if we need authentication
	if( !NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_IsAuthenticated( out ) )
	{
		// Run thread
		out->m_isAuthenticationThreadRunning = NTRUE;

		// Build authentication thread
		if( !( out->m_thread = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NGhostClient_Device_Remote_NGhostRemoteAuthentication_AuthenticationThread,
			out,
			&out->m_isAuthenticationThreadRunning ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Free
			NFREE( out->m_hostname );
			NFREE( out );

			// Quit
			return NULL;
		}
	}
	else
		// Send authentication
		NGhostClient_Device_Remote_NGhostRemoteAuthentication_SendInitialDetail( out );

	// OK
	return out;
}

/**
 * Destroy authentication
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_Destroy( NGhostRemoteDeviceAuthentication **this )
{
	// Do we have an authentication thread?
	if( (*this)->m_thread != NULL )
		// Destroy thread
		NLib_Thread_NThread_Detruire( &(*this)->m_thread );

	// Free
	NFREE( (*this)->m_hostname );
	NFREE( (*this) );
}

/**
 * Get authentication entry
 *
 * @param this
 * 		This instance
 *
 * @return the authentication entry
 */
const NAuthenticationEntry *NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_GetAuthenticationEntry( const NGhostRemoteDeviceAuthentication *this )
{
	return this->m_authenticationEntry;
}

/**
 * Is authenticated?
 *
 * @param this
 * 		This instance
 *
 * @return if we're authenticated on remote device
 */
NBOOL NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_IsAuthenticated( const NGhostRemoteDeviceAuthentication *this )
{
	return (NBOOL)( this->m_authenticationEntry != NULL );
}

/**
 * Get device type
 *
 * @param this
 * 		This instance
 *
 * @return the device type
 */
NDeviceType NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_GetDeviceType( const NGhostRemoteDeviceAuthentication *this )
{
	return this->m_deviceType;
}

/**
 * Build parser output list (private)
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 * @param serviceType
 * 		The service type
 *
 * @return if operation succeeded
 */
__PRIVATE NBOOL NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_BuildParserOutputListInternal( const NGhostRemoteDeviceAuthentication *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList,
	NGhostRemoteDeviceAuthenticationService serviceType )
{
	// Key
	char key[ 64 ];

	// Build key
	snprintf( key,
		64,
		"%s%s%s",
		keyRoot,
		strlen( keyRoot ) > 0 ?
			"."
			: "",
		NGhostClient_Service_Device_NGhostRemoteDeviceAuthenticationService_GetServiceName( serviceType ) );

	// Analyze service
	switch( serviceType )
	{
		case NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_HOSTNAME:
			NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				this->m_hostname );
			break;
		case NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_PORT:
			NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				this->m_port );
			break;
		case NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_DEVICE_TYPE:
			NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				NDeviceCommon_Type_NDeviceType_GetName( this->m_deviceType ) );
			break;
		case NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_IS_AUTHENTICATED:
			NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
				key,
				NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_IsAuthenticated( this ) );
			break;
		case NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_LAST_ATTEMPT_TIME:
			if( !NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_IsAuthenticated( this ) )
				NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
					key,
					(NU32)this->m_lastAuthenticationAttemptTimestamp );
			break;
		case NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_ATTEMPT_COUNT:
			if( !NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_IsAuthenticated( this ) )
				NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
					key,
					this->m_authenticationRetryCount );
			break;
		case NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_TOKEN:
			if( NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_IsAuthenticated( this )
				&& NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( this->m_authenticationEntry ) != NULL )
				NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( this->m_authenticationEntry ) );
			break;

		default:
			return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if operation succeeded
 */
NBOOL NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_BuildParserOutputList( const NGhostRemoteDeviceAuthentication *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Iterator
	NGhostRemoteDeviceAuthenticationService i = (NGhostRemoteDeviceAuthenticationService)0;

	// Iterate
	for( ; i < NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICES; i++ )
		// Add
		NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_BuildParserOutputListInternal( this,
			keyRoot,
			parserOutputList,
			i );

	// OK
	return NTRUE;
}

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 *
 * @return if operation succeeded
 */
NBOOL NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_ProcessRESTGetRequest( const NGhostRemoteDeviceAuthentication *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *requestedElement,
	NU32 *cursor )
{
	// Service type
	NGhostRemoteDeviceAuthenticationService serviceType;

	// Process service type
	switch( ( serviceType = NGhostClient_Service_Device_NGhostRemoteDeviceAuthenticationService_FindService( requestedElement,
		cursor ) ) )
	{
		default:
			return NFALSE;

		case NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_ROOT:
			return NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_BuildParserOutputList( this,
				"",
				parserOutputList );

		case NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_HOSTNAME:
		case NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_PORT:
		case NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_DEVICE_TYPE:
		case NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_IS_AUTHENTICATED:
		case NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_LAST_ATTEMPT_TIME:
		case NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_ATTEMPT_COUNT:
		case NGHOST_REMOTE_DEVICE_AUTHENTICATION_SERVICE_TOKEN:
			return NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_BuildParserOutputListInternal( this,
				"",
				parserOutputList,
				serviceType );
	}
}

/**
 * Broadcast
 */
__PRIVATE NBOOL NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_BroadcastNewToken( NGhostRemoteDeviceAuthentication *this,
	const NAuthenticationEntry *authenticationEntry )
{
	// Request
	NRequeteHTTP *request;

	// Parser output list
	NParserOutputList *parserOutputList;

	// Output
	__OUTPUT NBOOL result;

	// Json
	char *json;

	// Build parser output list
	if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Add authentication details
	NDeviceCommon_Authentication_NAuthenticationEntry_BuildParserOutputList( authenticationEntry,
		"",
		parserOutputList );

	// Export to json
	if( !( json = NJson_Engine_Builder_Build( parserOutputList ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NFALSE;
	}

	// Destroy
	NParser_Output_NParserOutputList_Destroy( &parserOutputList );

	// Build request
	if( !( request = NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire2( NTYPE_REQUETE_HTTP_POST,
		NDEVICE_COMMON_TYPE_GHOST_AUTHENTICATION ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( json );

		// Quit
		return NFALSE;
	}

	// Add data
	NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterData( request,
		json,
		(NU32)strlen( json ) );

	// Free
	NFREE( json );

	// Add json header
	NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
		NMOT_CLEF_REQUETE_HTTP_KEYWORD_CONTENT_TYPE,
		NHTTP_Commun_HTTP_Mimetype_NTypeFlux_ObtenirType( NTYPE_FLUX_JSON ) );

	// Broadcast
	result = NGhostClient_Device_NGhostDeviceContainerList_BroadcastRequestGhostDevice( this->m_deviceContainerList,
		request );

	// Destroy request
	NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Detruire( &request );

	// OK?
	return result;
}

/**
 * Packet receive callback
 *
 * @param packet
 * 		The received packet
 * @param client
 * 		The client
 *
 * @return if operation succeeded
 */
__CALLBACK NBOOL NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_CallbackPacketReceive( const NPacket *packet,
	const NClient *client )
{
	// Parser output list
	NParserOutputList *parserOutputList;

	// Parser output
	const NParserOutput *parserOutput;

	// HTTP response
	NReponseHTTP *httpResponse;

	// Secure data
	char *secureData;

	// Token
	const char *token;

	// HTTP client
	NClientHTTP *httpClient;

	// Remote device
	NGhostRemoteDevice *remoteDevice;

	// This instance
	NGhostRemoteDeviceAuthentication *this;

	// Authentication entry
	NAuthenticationEntry *authenticationEntry;

	// Get data
	if( !( httpClient = NLib_Module_Reseau_Client_NClient_ObtenirDataUtilisateur( client ) )
		|| !( remoteDevice = NHTTP_Client_NClientHTTP_ObtenirDataUtilisateur( httpClient ) )
		|| !( this = (NGhostRemoteDeviceAuthentication*)NGhostClient_Device_Remote_NGhostRemoteDevice_GetRemoteAuthentication( remoteDevice ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	// Allocate Secure data
	if( !( secureData = calloc( NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) + 1,
		sizeof( char ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NFALSE;
	}

	// Copy data
	memcpy( secureData,
		NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) );

	// Build response
	if( !( httpResponse = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Construire2( secureData ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Free
		NFREE( secureData );

		// Quit
		return NFALSE;
	}

	// Free
	NFREE( secureData );

	// Get data
	if( !( secureData = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirCopieData( httpResponse ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Destroy response
		NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &httpResponse );

		// Quit
		return NFALSE;
	}

	// Destroy response
	NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &httpResponse );

	// Parse packet content
	if( !( parserOutputList = NJson_Engine_Parser_Parse( secureData,
		(NU32)strlen( secureData ) ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Destroy data
		NFREE( secureData );

		// Quit
		return NFALSE;
	}

	// Free
	NFREE( secureData );

	// Look for token key
	if( !( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( parserOutputList,
		NGHOSTCOMMON_SERVICE_TOKEN_KEY,
		NTRUE,
		NTRUE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Destroy parser output list
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NFALSE;
	}

	// Read token
	if( NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_STRING
		|| !( token = NParser_Output_NParserOutput_GetValue( parserOutput ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Destroy parser output list
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NFALSE;
	}

	// Build authentication entry
	if( !( authenticationEntry = NDeviceCommon_Authentication_NAuthenticationEntry_Build( this->m_hostname,
		this->m_port,
		token,
		NULL,
		NAUTHENTICATION_ENTRY_TYPE_GHOST_TOKEN,
		NAUTHENTICATION_METHOD_TOKEN,
		NTRUE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy parser output list
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NFALSE;
	}

	// Broadcast new authentication data
	NGhostClient_Device_Remote_NGhostRemoteDeviceAuthentication_BroadcastNewToken( this,
		authenticationEntry );

	// Lock authentication manager
	NDeviceCommon_Authentication_NAuthenticationManager_ActivateProtection( this->m_authenticationManager );

	// Add entry to authentication manager
	NDeviceCommon_Authentication_NAuthenticationManager_AddAuthentication( this->m_authenticationManager,
		authenticationEntry );

	// Save
	NDeviceCommon_Authentication_NAuthenticationManager_Save( this->m_authenticationManager );

	// Unlock authentication manager
	NDeviceCommon_Authentication_NAuthenticationManager_DeactivateProtection( this->m_authenticationManager );

	// Destroy parser output list
	NParser_Output_NParserOutputList_Destroy( &parserOutputList );



	// OK
	return NTRUE;
}
