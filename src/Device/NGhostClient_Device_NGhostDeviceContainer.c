#include "../../include/NGhostClient.h"

// --------------------------------------------------
// struct NGhostClient::Device::NGhostDeviceContainer
// --------------------------------------------------

/**
 * Build a device
 *
 * @param type
 * 		The device type
 * @param name
 * 		The device name
 * @param ip
 * 		The device ip
 * @param uuid
 * 		The unique device id
 * @param contactingIP
 * 		The contacting IP from our end used to contact the service
 * @param authenticationManager
 * 		The authentication manager
 * @param deviceContainerList
 * 		The device container list
 * @param actionList
 * 		The action list
 *
 * @return the device container instance
 */
__ALLOC NGhostDeviceContainer *NGhostClient_Device_NGhostDeviceContainer_Build( NDeviceType type,
	const char *name,
	const char *ip,
	const char *uuid,
	const char *contactingIP,
	NAuthenticationManager *authenticationManager,
	void *deviceContainerList,
	NGhostActionList *actionList )
{
	// Output
	__OUTPUT NGhostDeviceContainer *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostDeviceContainer ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Duplicate data
	if( !( out->m_ip = NLib_Chaine_Dupliquer( ip ) )
		|| !( out->m_uuid = NLib_Chaine_Dupliquer( uuid ) )
		|| !( out->m_name = NLib_Chaine_Dupliquer( name ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out->m_uuid );
		NFREE( out->m_ip );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save
	out->m_type = type;
	out->m_configuration = NGhostClient_Device_NGhostDeviceContainerList_GetConfiguration( deviceContainerList );

	// Create device
	if( NDeviceCommon_Type_NDeviceType_IsGhostDevice( type ) )
	{
		// Build remote ghost device
		if( !( out->m_device = NGhostClient_Device_Remote_NGhostRemoteDevice_Build( authenticationManager,
			contactingIP,
			out->m_ip,
			NDeviceCommon_Type_NDeviceType_GetListeningPort( out->m_type ),
			out->m_type,
			deviceContainerList ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Free
			NFREE( out->m_name );
			NFREE( out->m_uuid );
			NFREE( out->m_ip );
			NFREE( out );

			// Quit
			return NULL;
		}
	}
	else
	{
		switch( type )
		{
			case NDEVICE_TYPE_HUE:
				// Build device
				if( !( out->m_device = NDeviceHUE_NDeviceHUE_Build( uuid,
					out->m_ip,
					0,
					NDEVICE_COMMON_TYPE_HUE_AUTO_LIGHT_UPDATE_DELAY ) ) )
				{
					// Notify
					NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

					// Free
					NFREE( out->m_name );
					NFREE( out->m_uuid );
					NFREE( out->m_ip );
					NFREE( out );

					// Quit
					return NULL;
				}
				break;
			case NDEVICE_TYPE_MFI_MPOWER:
				// Build device
				if( !( out->m_device = NDeviceMFI_NDeviceMFI_Build( out->m_ip,
					NDeviceCommon_Type_NDeviceType_GetListeningPort( type ) ) ) )
				{
					// Notify
					NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

					// Free
					NFREE( out->m_name );
					NFREE( out->m_uuid );
					NFREE( out->m_ip );
					NFREE( out );

					// Quit
					return NULL;
				}
				break;

			default:
				// Notify
				NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

				// Free
				NFREE( out->m_name );
				NFREE( out->m_uuid );
				NFREE( out->m_ip );
				NFREE( out );

				// Quit
				return NULL;
		}
	}

	// Build action updater
	if( !( out->m_actionUpdater = NGhostCommon_Action_NGhostActionUpdater_Build( actionList,
		out,
		NGhostClient_Device_Action_UpdateThread ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy device
		if( NDeviceCommon_Type_NDeviceType_IsGhostDevice( out->m_type ) )
			NGhostClient_Device_Remote_NGhostRemoteDevice_Destroy( (NGhostRemoteDevice**)&out->m_device );
		else
			switch( out->m_type )
			{
				case NDEVICE_TYPE_HUE:
					NDeviceHUE_NDeviceHUE_Destroy( (NDeviceHUE**)&out->m_device );
					break;
				case NDEVICE_TYPE_MFI_MPOWER:
					NDeviceMFI_NDeviceMFI_Destroy( (NDeviceMFI**)&out->m_device );
					break;

				default:
					break;
			}

		// Free
		NFREE( out->m_name );
		NFREE( out->m_ip );
		NFREE( out->m_uuid );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy container
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Device_NGhostDeviceContainer_Destroy( NGhostDeviceContainer **this )
{
	// Destroy action updater
	NGhostCommon_Action_NGhostActionUpdater_Destroy( &( *this )->m_actionUpdater );

	// Destroy device
	if( NDeviceCommon_Type_NDeviceType_IsGhostDevice( (*this)->m_type ) )
		NGhostClient_Device_Remote_NGhostRemoteDevice_Destroy( (NGhostRemoteDevice**)&(*this)->m_device );
	else
		switch( (*this)->m_type )
		{
			case NDEVICE_TYPE_HUE:
				NDeviceHUE_NDeviceHUE_Destroy( (NDeviceHUE**)&(*this)->m_device );
				break;
			case NDEVICE_TYPE_MFI_MPOWER:
				NDeviceMFI_NDeviceMFI_Destroy( (NDeviceMFI**)&(*this)->m_device );
				break;

			default:
				break;
		}

	// Free
	NFREE( (*this)->m_name );
	NFREE( (*this)->m_ip );
	NFREE( (*this)->m_uuid );
	NFREE( (*this) );
}

/**
 * Get device type
 *
 * @param this
 * 		This instance
 *
 * @return the device type
 */
NDeviceType NGhostClient_Device_NGhostDeviceContainer_GetType( const NGhostDeviceContainer *this )
{
	return this->m_type;
}

/**
 * Get device name
 *
 * @param this
 * 		This instance
 *
 * @return the device name
 */
const char *NGhostClient_Device_NGhostDeviceContainer_GetName( const NGhostDeviceContainer *this )
{
	return this->m_name;
}

/**
 * Get device
 *
 * @param this
 * 		This instance
 *
 * @return the device
 */
void *NGhostClient_Device_NGhostDeviceContainer_GetDevice( const NGhostDeviceContainer *this )
{
	return this->m_device;
}

/**
 * Get the uuid
 *
 * @param this
 * 		This instance
 *
 * @return the uuid
 */
const char *NGhostClient_Device_NGhostDeviceContainer_GetUUID( const NGhostDeviceContainer *this )
{
	return this->m_uuid;
}

/**
 * Get the device ip
 *
 * @param this
 * 		This instance
 *
 * @return the device ip
 */
const char *NGhostClient_Device_NGhostDeviceContainer_GetIP( const NGhostDeviceContainer *this )
{
	return this->m_ip;
}

/**
 * Get configuration
 *
 * @param this
 * 		This instance
 *
 * @return the configuration
 */
const NGhostClientConfiguration *NGhostClient_Device_NGhostDeviceContainer_GetGhostClientConfiguration( const NGhostDeviceContainer *this )
{
	return this->m_configuration;
}

/**
 * Is this uuid associated to this device
 *
 * @param this
 * 		This instance
 * @param uuid
 * 		The uuid to compare with
 *
 * @return if uuid is the same then the device's one
 */
NBOOL NGhostClient_Device_NGhostDeviceContainer_IsUUIDAssociatedWithDevice( const NGhostDeviceContainer *this,
	const char *uuid )
{
	return NLib_Chaine_Comparer( this->m_uuid,
		uuid,
		NFALSE,
		0 );
}

/**
 * Build parser output list (internal)
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param isRootOnly
 * 		Is root only?
 * @param index
 * 		The device container index
 * @param serviceType
 * 		The service type to get
 *
 * @return if operation succeeded
 */
__PRIVATE NBOOL NGhostClient_Device_NGhostDeviceContainer_BuildParserOutputListInternal( const NGhostDeviceContainer *this,
	__OUTPUT NParserOutputList *parserOutputList,
	NBOOL isRootOnly,
	NU32 index,
	NGhostDeviceServiceType serviceType )
{
	// Key
	char key[ 512 ];

	// Callback for key obtention
	__CALLBACK const char *( ___cdecl *callbackKeyObtention )( NGhostDeviceServiceType );

	// Find which function use
	if( isRootOnly )
		callbackKeyObtention = NGhostClient_Service_Device_NGhostDeviceServiceList_GetLowLevelJsonKey;
	else
		callbackKeyObtention = NGhostClient_Service_Device_NGhostDeviceServiceList_GetJsonKey;

	switch( serviceType )
	{
		case NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_UUID:
			// UUID
			snprintf( key,
				512,
				callbackKeyObtention( NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_UUID ),
				index );
			NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				this->m_uuid );
			break;
		case NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_IP:
			// IP
			snprintf( key,
				512,
				callbackKeyObtention( NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_IP ),
				index );
			NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				this->m_ip );
			break;

		case NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_TYPE:
			// Type
			snprintf( key,
				512,
				callbackKeyObtention( NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_TYPE ),
				index );
			NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				NDeviceCommon_Type_NDeviceType_GetUserFriendlyName( this->m_type ) );
			break;

		case NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_STATE:
			// New key
			snprintf( key,
				512,
				callbackKeyObtention( NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_STATE ),
				index );

			// Fill with type-proper informations
			if( NDeviceCommon_Type_NDeviceType_IsGhostDevice( this->m_type ) )
				// Ghost device
				NGhostClient_Device_Remote_NGhostRemoteDevice_BuildParserOutputList( this->m_device,
					key,
					parserOutputList );
			else
				// Other device type
				switch( this->m_type )
				{
					case NDEVICE_TYPE_HUE:
						NDeviceHUE_NDeviceHUE_BuildParserOutputList( this->m_device,
							key,
							parserOutputList );
						break;

					case NDEVICE_TYPE_MFI_MPOWER:
						NDeviceMFI_NDeviceMFI_BuildParserOutputList( this->m_device,
							parserOutputList,
							key );
						break;


					default:
						break;
				}
			break;

		default:
			return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param isRootOnly
 * 		We only want the root?
 * @param index
 * 		The device index
 *
 * @return if operation succeeded
 */
NBOOL NGhostClient_Device_NGhostDeviceContainer_BuildParserOutputList( const NGhostDeviceContainer *this,
	__OUTPUT NParserOutputList *parserOutputList,
	NBOOL isRootOnly,
	NU32 index )
{
	// Service type
	NGhostDeviceServiceType serviceType = (NGhostDeviceServiceType)0;

	// Add all services
	for( ; serviceType < NGHOST_CLIENT_SERVICES_DEVICE; serviceType++ )
		NGhostClient_Device_NGhostDeviceContainer_BuildParserOutputListInternal( this,
			parserOutputList,
			isRootOnly,
			index,
			serviceType );

	// OK
	return NTRUE;
}

/**
 * Process REST get request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element in device
 * @param cursor
 * 		The cursor in the requested element
 * @param clientID
 * 		The client id
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostClient_Device_NGhostDeviceContainer_ProcessRESTGetRequest( const NGhostDeviceContainer *this,
	const char *requestedElement,
	NU32 *cursor,
	NU32 clientID )
{
	// Parser output list
	NParserOutputList *parserOutputList;

	// Service
	NGhostDeviceServiceType serviceType;

	// Build parser output list
	if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Process
	switch( ( serviceType = NGhostClient_Service_Device_NGhostDeviceServiceList_FindService( requestedElement,
		cursor,
		NFALSE ) ) )
	{
		case NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_UUID:
		case NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_IP:
		case NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_TYPE:
			if( !NGhostClient_Device_NGhostDeviceContainer_BuildParserOutputListInternal( this,
				parserOutputList,
				NTRUE,
				0,
				serviceType ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Free
				NParser_Output_NParserOutputList_Destroy( &parserOutputList );

				// Quit
				return NULL;
			}
			break;

		case NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_STATE:
			// Ghost device?
			if( NDeviceCommon_Type_NDeviceType_IsGhostDevice( this->m_type ) )
			{
				// Process ghost GET request
				if( !NGhostClient_Device_Remote_NGhostRemoteDevice_ProcessRESTGetRequest( this->m_device,
					parserOutputList,
					requestedElement,
					cursor ) )
				{
					// Notify
					NOTIFIER_ERREUR( NERREUR_CANT_PROCESS_REQUEST );

					// Destroy
					NParser_Output_NParserOutputList_Destroy( &parserOutputList );

					// Quit
					return NULL;
				}
			}
			// Non-ghost device
			else
				switch( this->m_type )
				{
					case NDEVICE_TYPE_HUE:
						// HUE GET request
						if( !NDeviceHUE_NDeviceHUE_ProcessRESTGETRequest( this->m_device,
							parserOutputList,
							requestedElement,
							cursor ) )
						{
							// Notify
							NOTIFIER_ERREUR( NERREUR_CANT_PROCESS_REQUEST );

							// Destroy
							NParser_Output_NParserOutputList_Destroy( &parserOutputList );

							// Quit
							return NULL;
						}
						break;

					case NDEVICE_TYPE_MFI_MPOWER:
						if( !NDeviceMFI_NDeviceMFI_ProcessRESTGETRequest( this->m_device,
							parserOutputList,
							requestedElement,
							cursor ) )
						{
							// Notify
							NOTIFIER_ERREUR( NERREUR_CANT_PROCESS_REQUEST );

							// Destroy
							NParser_Output_NParserOutputList_Destroy( &parserOutputList );

							// Quit
							return NULL;
						}
						break;

					default:
						break;
				}
			break;

		case NGHOST_CLIENT_SERVICE_DEVICE_ROOT:
			// Build device all proper-informations list
			if( !NGhostClient_Device_NGhostDeviceContainer_BuildParserOutputList( this,
				parserOutputList,
				NTRUE,
				0 ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Free
				NParser_Output_NParserOutputList_Destroy( &parserOutputList );

				// Quit
				return NULL;
			}
			break;

		default:
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Destroy
			NParser_Output_NParserOutputList_Destroy( &parserOutputList );

			// Quit
			return NULL;
	}

	// OK
	return NDeviceCommon_Service_GenerateJsonHTTPResponseWithParserOutputList( NHTTP_CODE_200_OK,
		parserOutputList,
		clientID );
}

/**
 * Process REST put request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 *		The requested element
 * @param cursor
 * 		The cursor in the requested element
 * @param clientID
 * 		The client id
 * @param clientJsonData
 * 		The parsed client json sent data
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostClient_Device_NGhostDeviceContainer_ProcessRESTPutRequest( NGhostDeviceContainer *this,
	const char *requestedElement,
	NU32 *cursor,
	NU32 clientID,
	const NParserOutputList *clientJsonData )
{
	// Check parser output list
	if( !clientJsonData )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

		// Quit
		return NULL;
	}

	// Process
	switch( NGhostClient_Service_Device_NGhostDeviceServiceList_FindService( requestedElement,
		cursor,
		NFALSE ) )
	{
		case NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_STATE:
			switch( this->m_type )
			{
				case NDEVICE_TYPE_HUE:
					// Check authentication
					if( !NDeviceHUE_NDeviceHUE_IsReady( this->m_device ) )
						// Quit
						return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_NOT_INITIALIZATED,
							NHTTP_CODE_417_EXPECTATION_FAILED,
							clientID );

					// Process request
					if( !NDeviceHUE_NDeviceHUE_ProcessRESTPUTRequest( this->m_device,
						requestedElement,
						cursor,
						clientJsonData ) )
					{
						// Notify
						NOTIFIER_ERREUR( NERREUR_CANT_PROCESS_REQUEST );

						// Quit
						return NULL;
					}
					break;

				default:
					break;
			}
			break;

		default:
			return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
				NHTTP_CODE_400_BAD_REQUEST,
				clientID );
	}

	// OK
	return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_OK_MESSAGE,
		NHTTP_CODE_200_OK,
		clientID );
}

/**
 * Process REST post request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 *		The requested element
 * @param cursor
 * 		The cursor in the requested element
 * @param clientID
 * 		The client id
 * @param clientJsonData
 * 		The parsed client json sent data
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostClient_Device_NGhostDeviceContainer_ProcessRESTPostRequest( NGhostDeviceContainer *this,
	const char *requestedElement,
	NU32 *cursor,
	NU32 clientID,
	const NParserOutputList *clientJsonData )
{
	// Process
	switch( NGhostClient_Service_Device_NGhostDeviceServiceList_FindService( requestedElement,
		cursor,
		NFALSE ) )
	{
		case NGHOST_CLIENT_SERVICE_DEVICE_DEVICE_STATE:
			switch( this->m_type )
			{
				case NDEVICE_TYPE_HUE:
					break;

				case NDEVICE_TYPE_MFI_MPOWER:
					// Check user data
					if( !clientJsonData )
					{
						// Notify
						NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

						// Quit
						return NULL;
					}

					// Process request
					if( !NDeviceMFI_NDeviceMFI_ProcessRESTPOSTRequest( this->m_device,
						clientJsonData,
						requestedElement,
						cursor ) )
					{
						// Notify
						NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

						// Quit
						return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
							NHTTP_CODE_400_BAD_REQUEST,
							clientID );
					}
					break;


				case NDEVICE_TYPE_GHOST_CLIENT:
					break;

				case NDEVICE_TYPE_GHOST_AUDIO:
					// Check user data
					if( !clientJsonData )
					{
						// Notify
						NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

						// Quit
						return NULL;
					}

					// Process
					switch( NGhostClient_Service_Device_Proxy_Audio_NGhostClientProxyAudioService_FindService( requestedElement,
						cursor ) )
					{
						case NGHOST_CLIENT_PROXY_AUDIO_SERVICE_START:
							if( !NGhostClient_Service_Device_Proxy_Audio_ProcessStartRequest( this->m_device,
								clientJsonData ) )
								return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
									NHTTP_CODE_400_BAD_REQUEST,
									clientID );
							break;
						case NGHOST_CLIENT_PROXY_AUDIO_SERVICE_STOP:
							return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_NOT_IMPLEMENTED_MESSAGE,
								NHTTP_CODE_424_METHOD_FAILURE,
								clientID );

						default:
							return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
								NHTTP_CODE_400_BAD_REQUEST,
								clientID );
					}
					break;

				case NDEVICE_TYPE_GHOST_DOOR_OPENER:
					switch( NGhostClient_Service_Device_Proxy_DoorOpener_NGhostDoorOpenerProxyService_FindService( requestedElement,
						cursor ) )
					{
						case NGHOST_DOOR_OPENER_PROXY_SERVICE_OPEN:
							if( !NGhostClient_Service_Device_Proxy_DoorOpener_ProcessDoorOpening( this ) )
								return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_INTERNAL_ERROR_MESSAGE,
									NHTTP_CODE_500_INTERNAL_SERVER_ERROR,
									clientID );
							break;

						default:
							return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
								NHTTP_CODE_400_BAD_REQUEST,
								clientID );
					}
					break;

				default:
					break;
			}
			break;

		default:
			return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
				NHTTP_CODE_400_BAD_REQUEST,
				clientID );
	}

	// OK
	return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_OK_MESSAGE,
		NHTTP_CODE_200_OK,
		clientID );
}

/**
 * Send request to device
 *
 * @param this
 * 		This instance
 * @param request
 * 		The request
 *
 * @return if the operation succeeded
 */
NBOOL NGhostClient_Device_NGhostDeviceContainer_SendRequest( NGhostDeviceContainer *this,
	__WILLBEOWNED NRequeteHTTP *request )
{
	// Analyze type
	switch( this->m_type )
	{
		case NDEVICE_TYPE_HUE:
		case NDEVICE_TYPE_MFI_MPOWER:
			return NFALSE;

		default:
			return NGhostClient_Device_Remote_NGhostRemoteDevice_SendRequest( this->m_device,
				request,
				NGHOST_CLIENT_DEVICE_REMOTE_BROADCAST_CONNECTION_TIMEOUT );
	}
}
