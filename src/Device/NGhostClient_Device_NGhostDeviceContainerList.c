#include "../../include/NGhostClient.h"

// ------------------------------------------------------
// struct NGhostClient::Device::NGhostDeviceContainerList
// ------------------------------------------------------

/**
 * Build device container list
 *
 * @param configuration
 * 		The ghost configuration
 * @param ghostCommon
 * 		The ghost common instance
 *
 * @return the device container list
 */
__ALLOC NGhostDeviceContainerList *NGhostClient_Device_NGhostDeviceContainerList_Build( const NGhostClientConfiguration *configuration,
	NGhostCommon *ghostCommon )
{
	// Output
	__OUTPUT NGhostDeviceContainerList *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostDeviceContainerList ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Create device list
	if( !( out->m_device = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NGhostClient_Device_NGhostDeviceContainer_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save
	out->m_configuration = configuration;
	out->m_authenticationManager = NGhostCommon_NGhostCommon_GetAuthenticationManager( ghostCommon );
	out->m_ghostCommon = ghostCommon;

	// OK
	return out;
}

/**
 * Destroy device container list
 *
 * @param this
 * 		This instance
 */
void NGhostClient_Device_NGhostDeviceContainerList_Destroy( NGhostDeviceContainerList **this )
{
	// Destroy device list
	NLib_Memoire_NListe_Detruire( &(*this)->m_device );

	// Free
	NFREE( (*this) );
}

/**
 * Add device to list
 *
 * @param this
 * 		This instance
 * @param scannerResult
 * 		The scanner result to add
 *
 * @return the built device
 */
__PRIVATE const NGhostDeviceContainer *NGhostClient_Device_NGhostDeviceContainerList_AddDevice( NGhostDeviceContainerList *this,
	const NDeviceScannerResult *scannerResult )
{
	// Device container
	__OUTPUT NGhostDeviceContainer *deviceContainer;

	// Check state
	if( this->m_isDuplicate )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NULL;
	}

	// Build device
	if( !( deviceContainer = NGhostClient_Device_NGhostDeviceContainer_Build( NDeviceScanner_Result_NDeviceScannerResult_GetType( scannerResult ),
		NDeviceScanner_Result_NDeviceScannerResult_GetName( scannerResult ),
		NDeviceScanner_Result_NDeviceScannerResult_GetIP( scannerResult ),
		NDeviceScanner_Result_NDeviceScannerResult_GetUUID( scannerResult ),
		NDeviceScanner_Result_NDeviceScannerResult_GetRequestingIP( scannerResult ),
		this->m_authenticationManager,
		this,
		NGhostCommon_NGhostCommon_GetActionList( this->m_ghostCommon ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Add
	if( !NLib_Memoire_NListe_Ajouter( this->m_device,
		deviceContainer ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_MEMORY );

		// Quit
		return NULL;
	}

	// OK
	return deviceContainer;
}

/**
 * Process scanner result set
 *
 * @param this
 * 		This instance
 * @param scannerResultSetList
 *		The scanner result set (NListe<NDeviceScannerResultSet*>)
 *
 * @return if process succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NGhostClient_Device_NGhostDeviceContainerList_ProcessScannerResultSet( NGhostDeviceContainerList *this,
	const NListe *scannerResultSetList )
{
	// Iterators
	NU32 i = 0,
		j;

	// Scanner result set
	const NDeviceScannerResultSet *scannerResultSet;

	// Scanner result
	const NDeviceScannerResult *scannerResult;

	// Check
	if( this->m_isDuplicate )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	// Lock mutex
	NLib_Memoire_NListe_ActiverProtection( this->m_device );

	// Iterate
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( scannerResultSetList ); i++ )
		// Get result set
		if( ( scannerResultSet = NLib_Memoire_NListe_ObtenirElementDepuisIndex( scannerResultSetList,
			i ) ) != NULL )
			for( j = 0; j < NDeviceScanner_Result_NDeviceScannerResultSet_GetCount( scannerResultSet ); j++ )
				// Get result
				if( ( scannerResult = NDeviceScanner_Result_NDeviceScannerResultSet_Get( scannerResultSet,
					j ) ) != NULL )
					// Check filtered properties
					if( !NGhostClient_Configuration_NGhostClientConfiguration_IsDeviceTypeFiltered( this->m_configuration,
						NDeviceScanner_Result_NDeviceScannerResult_GetType( scannerResult ) )
						// UUID not filtered?
						&& !NGhostClient_Configuration_NGhostClientConfiguration_IsDeviceUUIDFiltered( this->m_configuration,
							NDeviceScanner_Result_NDeviceScannerResult_GetUUID( scannerResult ) )
						// Not the current client?
						&& ( !NLib_Chaine_Comparer( NDeviceScanner_Result_NDeviceScannerResult_GetUUID( scannerResult ),
								NGhostClient_Configuration_NGhostClientConfiguration_GetDeviceUniqueID( this->m_configuration ),
								NTRUE,
								0 )
							|| NDeviceScanner_Result_NDeviceScannerResult_GetType( scannerResult ) != NDEVICE_TYPE_GHOST_CLIENT ) )
						// Check if already present
						if( NGhostClient_Device_NGhostDeviceContainerList_FindDeviceWithUUID( this,
							NDeviceScanner_Result_NDeviceScannerResult_GetUUID( scannerResult ) ) == NULL
							&& NGhostClient_Device_NGhostDeviceContainerList_FindDeviceWithIPAndType( this,
								NDeviceScanner_Result_NDeviceScannerResult_GetIP( scannerResult ),
								NDeviceScanner_Result_NDeviceScannerResult_GetType( scannerResult ) ) == NULL )
							// Add to list
							if( NGhostClient_Device_NGhostDeviceContainerList_AddDevice( this,
								scannerResult ) == NULL )
								// Notify
								NOTIFIER_ERREUR( NERREUR_MEMORY );

	// Unlock mutex
	NLib_Memoire_NListe_DesactiverProtection( this->m_device );

	// OK
	return NTRUE;
}

/**
 * Get ghost watcher device ip list
 *
 * @param this
 * 		This instance
 *
 * @return the ghost watcher ip list
 */
__MUSTBEPROTECTED __ALLOC NListe *NGhostClient_Device_NGhostDeviceContainerList_FindGhostWatcherIPList( const NGhostDeviceContainerList *this )
{
	// Output
	__OUTPUT NListe *out;

	// Iterator
	NU32 i;

	// IP duplicate
	char *ipDuplicate;

	// Device container
	const NGhostDeviceContainer *deviceContainer;

	// Allocate memory
	if( !( out = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NLib_Memoire_Liberer ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Look for watcher
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_device ); i++ )
		// Get device
		if( ( deviceContainer = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_device,
			i ) ) != NULL )
			// Check type
			if( NGhostClient_Device_NGhostDeviceContainer_GetType( deviceContainer ) == NDEVICE_TYPE_GHOST_WATCHER )
			{
				// Add to current ghost client list
				NGhostCommon_NGhostCommon_AddWatcherIP( this->m_ghostCommon,
					NGhostClient_Device_NGhostDeviceContainer_GetIP( deviceContainer ) );

				// Duplicate IP
				if( ( ipDuplicate = NLib_Chaine_Dupliquer( NGhostClient_Device_NGhostDeviceContainer_GetIP( deviceContainer ) ) ) !=
					NULL )
					// Add to output list
					NLib_Memoire_NListe_Ajouter( out,
						ipDuplicate );
			}

	// OK
	return out;
}

/**
 * Get device with uuid
 *
 * @param this
 * 		This instance
 * @param uuid
 * 		The device uuid
 *
 * @return the device
 */
__MUSTBEPROTECTED NGhostDeviceContainer *NGhostClient_Device_NGhostDeviceContainerList_FindDeviceWithUUID( const NGhostDeviceContainerList *this,
	const char *uuid )
{
	// Iterator
	NU32 i = 0;

	// Device
	NGhostDeviceContainer *deviceContainer;

	// Look for
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_device ); i++ )
		// Get device
		if( ( deviceContainer = (NGhostDeviceContainer*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_device,
			i ) ) != NULL )
			// Check uuid
			if( NGhostClient_Device_NGhostDeviceContainer_IsUUIDAssociatedWithDevice( deviceContainer,
				uuid ) )
				return deviceContainer;

	// Not found
	return NULL;
}

/**
 * Find device with ip and type
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The ip
 * @param type
 * 		The type
 *
 * @return the device or NULL
 */
__MUSTBEPROTECTED NGhostDeviceContainer *NGhostClient_Device_NGhostDeviceContainerList_FindDeviceWithIPAndType( const NGhostDeviceContainerList *this,
	const char *ip,
	NDeviceType type )
{
	// Iterator
	NU32 i = 0;

	// Device
	const NGhostDeviceContainer *deviceContainer;

	// Look for
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_device ); i++ )
		// Get device container
		if( ( deviceContainer = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_device,
			i ) ) != NULL )
			// Check
			if( NLib_Chaine_Comparer( NGhostClient_Device_NGhostDeviceContainer_GetIP( deviceContainer ),
				ip,
				NTRUE,
				0 )
				&& NGhostClient_Device_NGhostDeviceContainer_GetType( deviceContainer ) == type )
				// OK
				return (NGhostDeviceContainer*)deviceContainer;

	// Not found
	return NULL;
}

/**
 * Get device with index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The device index
 *
 * @return the device
 */
__MUSTBEPROTECTED NGhostDeviceContainer *NGhostClient_Device_NGhostDeviceContainerList_FindDeviceWithIndex( const NGhostDeviceContainerList *this,
	NU32 index )
{
	return (NGhostDeviceContainer*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_device,
		index );
}

/**
 * Get device count
 *
 * @param this
 * 		This instance
 *
 * @return the device count
 */
__MUSTBEPROTECTED NU32 NGhostClient_Device_NGhostDeviceContainerList_GetDeviceCount( const NGhostDeviceContainerList *this )
{
	return NLib_Memoire_NListe_ObtenirNombre( this->m_device );
}

/**
 * Get configuration
 *
 * @param this
 * 		This instance
 *
 * @return the configuration
 */
const NGhostClientConfiguration *NGhostClient_Device_NGhostDeviceContainerList_GetConfiguration( const NGhostDeviceContainerList *this )
{
	return this->m_configuration;
}

/**
 * Get ghost common instance
 *
 * @param this
 * 		This instance
 *
 * @return the ghost common instance
 */
NGhostCommon *NGhostClient_Device_NGhostDeviceContainerList_GetCommon( const NGhostDeviceContainerList *this )
{
	return this->m_ghostCommon;
}

/**
 * Duplicate list (to use each time you want to work with items)
 *
 * @param this
 * 		This instance
 *
 * @return the duplicated devices list
 */
__ALLOC __WILLLOCK __WILLUNLOCK NGhostDeviceContainerList *NGhostClient_Device_NGhostDeviceContainerList_Duplicate( const NGhostDeviceContainerList *this )
{
	// Output
	NGhostDeviceContainerList *out;

	// Iterate
	NU32 i = 0;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostDeviceContainerList ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Create list
	if( !( out->m_device = NLib_Memoire_NListe_Construire( NULL ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Set duplicate
	out->m_isDuplicate = NTRUE;
	out->m_configuration = this->m_configuration;

	// Lock mutex
	NLib_Memoire_NListe_ActiverProtection( this->m_device );

	// Fill list
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_device ); i++ )
		NLib_Memoire_NListe_Ajouter( out->m_device,
			(void*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_device,
				i ) );

	// Unlock mutex
	NLib_Memoire_NListe_DesactiverProtection( this->m_device );

	// OK
	return out;
}

/**
 * Get the device to process (only for duplicate)
 *
 * @param this
 * 		This instance
 * @param requestedDevice
 * 		The requested devices uri
 * @param cursor
 * 		The cursor in the requested uri
 *
 * @return the device to be processed
 */
const NGhostDeviceContainer *NGhostClient_Device_NGhostDeviceContainerList_GetDeviceToProcess( const NGhostDeviceContainerList *this,
	const char *requestedDevice,
	NU32 *cursor )
{
	// Device identifier
	char *deviceIdentifier;

	// Device index
	NU32 deviceIndex;

	// Device
	NGhostDeviceContainer *deviceContainer;

	// Check for duplicate
	if( !this->m_isDuplicate )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NULL;
	}

	// Read which device should be processed
	if( !( deviceIdentifier = NLib_Chaine_LireJusqua( requestedDevice,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NULL;
	}

	// Is it a number?
	if( NLib_Chaine_EstUnNombreEntier( deviceIdentifier,
		10 ) )
	{
		// Parse
		if( ( deviceIndex = (NU32)strtol( deviceIdentifier,
			NULL,
			10 ) ) >= NLib_Memoire_NListe_ObtenirNombre( this->m_device ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Free
			NFREE( deviceIdentifier );

			// Quit
			return NULL;
		}

		// Get device
		if( !( deviceContainer = NGhostClient_Device_NGhostDeviceContainerList_FindDeviceWithIndex( this,
			deviceIndex ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Free
			NFREE( deviceIdentifier );

			// Quit
			return NULL;
		}
	}
	else
		// Find device
		if( !( deviceContainer = NGhostClient_Device_NGhostDeviceContainerList_FindDeviceWithUUID( this,
			deviceIdentifier ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Free
			NFREE( deviceIdentifier );

			// Quit
			return NULL;
		}

	// Free device identifier
	NFREE( deviceIdentifier );

	// OK
	return deviceContainer;
}

/**
 * Broadcast packet request to ghost devices
 *
 * @param this
 * 		This instance
 * @param request
 * 		The http request
 *
 * @return if the operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NGhostClient_Device_NGhostDeviceContainerList_BroadcastRequestGhostDevice( NGhostDeviceContainerList *this,
	const NRequeteHTTP *request )
{
	// Iterator
	NU32 i = 0;

	// Device
	NGhostDeviceContainer *deviceContainer;

	// Request copy
	NRequeteHTTP *requestCopy;

	// Lock
	NLib_Memoire_NListe_ActiverProtection( this->m_device );

	// Iterate
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_device ); i++ )
		// Get device
		if( ( deviceContainer = (NGhostDeviceContainer*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_device,
			i ) ) != NULL )
			// Ghost device?
			if( NDeviceCommon_Type_NDeviceType_IsGhostDevice( NGhostClient_Device_NGhostDeviceContainer_GetType( deviceContainer ) ) )
				// Duplicate packet
				if( ( requestCopy = NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire3( request ) ) != NULL )
					// Send packet
					NGhostClient_Device_NGhostDeviceContainer_SendRequest( deviceContainer,
						requestCopy );


	// Unlock
	NLib_Memoire_NListe_DesactiverProtection( this->m_device );

	// OK
	return NTRUE;
}

/**
 * Broadcast packet request to ghost devices except specified ip:port one
 *
 * @param this
 * 		This instance
 * @param request
 * 		The http request
 * @param ipFilter
 * 		The ip filter
 *
 * @return if the operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NGhostClient_Device_NGhostDeviceContainerList_BroadcastRequestGhostDevice2( NGhostDeviceContainerList *this,
	const NRequeteHTTP *request,
	const char *ipFilter )
{
	// Iterator
	NU32 i = 0;

	// Device
	NGhostDeviceContainer *deviceContainer;

	// Request copy
	NRequeteHTTP *requestCopy;

	// Lock
	NLib_Memoire_NListe_ActiverProtection( this->m_device );

	// Iterate
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_device ); i++ )
		// Get device
		if( ( deviceContainer = (NGhostDeviceContainer*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_device,
			i ) ) != NULL )
			// Check ip filter
			if( !NLib_Chaine_Comparer( NGhostClient_Device_NGhostDeviceContainer_GetIP( deviceContainer ),
				ipFilter,
				NTRUE,
				0 ) )
				// Ghost device?
				if( NDeviceCommon_Type_NDeviceType_IsGhostDevice( NGhostClient_Device_NGhostDeviceContainer_GetType( deviceContainer ) ) )
					// Duplicate packet
					if( ( requestCopy = NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire3( request ) ) !=
						NULL )
						// Send packet
						NGhostClient_Device_NGhostDeviceContainer_SendRequest( deviceContainer,
							requestCopy );

	// Unlock
	NLib_Memoire_NListe_DesactiverProtection( this->m_device );

	// OK
	return NTRUE;
}

/**
 * Activate protection
 *
 * @param this
 * 		This instance
 */
__WILLLOCK void NGhostClient_Device_NGhostDeviceContainerList_ActivateProtection( NGhostDeviceContainerList *this )
{
	NLib_Memoire_NListe_ActiverProtection( this->m_device );
}

/**
 * Deactivate protection
 *
 * @param this
 * 		This instance
 */
__WILLUNLOCK void NGhostClient_Device_NGhostDeviceContainerList_DeactivateProtection( NGhostDeviceContainerList *this )
{
	NLib_Memoire_NListe_DesactiverProtection( this->m_device );
}
