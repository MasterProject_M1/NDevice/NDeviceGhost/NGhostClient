#include "../include/NGhostClient.h"

// ---------------------------------
// struct NGhostClient::NGhostClient
// ---------------------------------

/**
 * Build the client
 *
 * @param configPath
 * 		The config file path
 *
 * @return the ghost client instance
 */
__ALLOC NGhostClient *NGhostClient_NGhostClient_Build( const char *configPath )
{
	// Output
	__OUTPUT NGhostClient *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostClient ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	/* Build configuration */
	if( !( out->m_configuration = NGhostClient_Configuration_NGhostClientConfiguration_Build( configPath ) ) )
	{
		// Create default configuration file
		if( !( out->m_configuration = NGhostClient_Configuration_NGhostClientConfiguration_Build2( ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Free
			NFREE( out );

			// Quit
			return NULL;
		}

		// Save the configuration
		if( !NGhostClient_Configuration_NGhostClientConfiguration_Save( out->m_configuration,
			configPath ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_CANT_WRITE );

			// Destroy configuration
			NGhostClient_Configuration_NGhostClientConfiguration_Destroy( &out->m_configuration );

			// Free
			NFREE( out );

			// Quit
			return NULL;
		}
	}

	/* Build services */
	// Scanner services
	if( !( out->m_scannerService = NGhostClient_Service_Scanner_NGhostClientScannerService_Build( out ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy configuration
		NGhostClient_Configuration_NGhostClientConfiguration_Destroy( &out->m_configuration );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	/* Common data */
	if( !( out->m_common = NGhostCommon_NGhostCommon_Build( NGhostClient_Configuration_NGhostClientConfiguration_IsNetworkMustListenAllInterface( out->m_configuration ) ?
			NULL
			: NGhostClient_Configuration_NGhostClientConfiguration_GetNetworkListeningInterface( out->m_configuration ),
		NGhostClient_Configuration_NGhostClientConfiguration_GetNetworkListeningPort( out->m_configuration ),
		NGhostClient_Service_ProcessRequest,
		NDeviceCommon_Type_NDeviceType_GetName( NDEVICE_TYPE_GHOST_CLIENT ),
		out,
		NDEVICE_TYPE_GHOST_CLIENT,
		NGhostClient_Configuration_NGhostClientConfiguration_IsAuthenticationManagerDataPersistent( out->m_configuration ),
		NGhostClient_Configuration_NGhostClientConfiguration_GetDeviceName( out->m_configuration ),
		NGhostClient_Configuration_NGhostClientConfiguration_GetAuthenticationUsername( out->m_configuration ),
		NGhostClient_Configuration_NGhostClientConfiguration_GetAuthenticationPassword( out->m_configuration ),
		NGHOST_VERSION_MAJOR,
		NGHOST_VERSION_MINOR,
		NGhostClient_Configuration_NGhostClientConfiguration_GetTrustedTokenIPList( out->m_configuration ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy service
		NGhostClient_Service_Scanner_NGhostClientScannerService_Destroy( (NGhostClientScannerService**)&out->m_scannerService );

		// Destroy configuration
		NGhostClient_Configuration_NGhostClientConfiguration_Destroy( &out->m_configuration );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	/* Build device container list */
	if( !( out->m_device = NGhostClient_Device_NGhostDeviceContainerList_Build( out->m_configuration,
		out->m_common ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy common
		NGhostCommon_NGhostCommon_Destroy( &out->m_common );

		// Destroy service
		NGhostClient_Service_Scanner_NGhostClientScannerService_Destroy( (NGhostClientScannerService**)&out->m_scannerService );

		// Destroy device list
		NGhostClient_Device_NGhostDeviceContainerList_Destroy( &out->m_device );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Start monitoring
	out->m_isRunning = NTRUE;
	if( !( out->m_monitoring = NGhostCommon_Monitoring_NMonitoringAgent_Build( NGhostCommon_NGhostCommon_GetTokenManager( out->m_common ),
		out,
		&out->m_isRunning,
		NULL ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy common data
		NGhostCommon_NGhostCommon_Destroy( &out->m_common );

		// Destroy service
		NGhostClient_Service_Scanner_NGhostClientScannerService_Destroy( (NGhostClientScannerService**)&out->m_scannerService );

		// Destroy device list
		NGhostClient_Device_NGhostDeviceContainerList_Destroy( &out->m_device );

		// Destroy configuration
		NGhostClient_Configuration_NGhostClientConfiguration_Destroy( &out->m_configuration );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy client
 *
 * @param this
 * 		This instance
 */
void NGhostClient_NGhostClient_Destroy( NGhostClient **this )
{
	// Destroy monitoring agent
	NGhostCommon_Monitoring_NMonitoringAgent_Destroy( &(*this)->m_monitoring );

	// Destroy services
	NGhostClient_Service_Scanner_NGhostClientScannerService_Destroy( (NGhostClientScannerService**)&(*this)->m_scannerService );

	// Destroy devices list
	NGhostClient_Device_NGhostDeviceContainerList_Destroy( &(*this)->m_device );

	// Destroy common basis
	NGhostCommon_NGhostCommon_Destroy( &(*this)->m_common );

	// Destroy configuration
	NGhostClient_Configuration_NGhostClientConfiguration_Destroy( &(*this)->m_configuration );

	// Free
	NFREE( (*this) );
}

/**
 * Get configuration
 *
 * @param this
 * 		This instance
 *
 * @return the ghost client configuration
 */
const NGhostClientConfiguration *NGhostClient_NGhostClient_GetConfiguration( const NGhostClient *this )
{
	return this->m_configuration;
}

/**
 * Get scanner service
 *
 * @param this
 * 		This instance
 *
 * @return the scanner service (NGhostClientScannerService*)
 */
const void *NGhostClient_NGhostClient_GetScannerService( const NGhostClient *this )
{
	return this->m_scannerService;
}

/**
 * Call scanning service
 *
 * @param this
 * 		This instance
 * @param wantedState
 * 		Wanted state: NTRUE for service activation, NFALSE for service deactivation
 *
 * @return if the scanned successfully started
 */
NBOOL NGhostClient_NGhostClient_CallScanningService( NGhostClient *this,
	NBOOL wantedState )
{
	// Activate?
	if( wantedState == NTRUE )
		// Activate
		return NGhostClient_Service_Scanner_NGhostClientScannerService_RunScan( this->m_scannerService );
	// Deactivate?
	else
		// Is scanner currently running?
		if( NGhostClient_Service_Scanner_NGhostClientScannerService_IsOperating( this->m_scannerService ) == NTRUE )
		{
			// Deactivate
			NGhostClient_Service_Scanner_NGhostClientScannerService_StopScan( this->m_scannerService );

			// OK
			return NTRUE;
		}
		// The scanner is not currently running
		else
			// Nothing happened
			return NFALSE;
}

/**
 * Process scanner result set
 *
 * @param this
 * 		This instance
 * @param scannerResultSet
 * 		The scanner result set (NListe<NDeviceScannerResultSet*>)
 *
 * @return if operation succeeded
 */
NBOOL NGhostClient_NGhostClient_ProcessScannerResultSet( NGhostClient *this,
	const NListe *scannerResultSet )
{
	return NGhostClient_Device_NGhostDeviceContainerList_ProcessScannerResultSet( this->m_device,
		scannerResultSet );
}

/**
 * Get device container list
 *
 * @param this
 * 		This instance
 *
 * @return the device container list
 */
const NGhostDeviceContainerList *NGhostClient_NGhostClient_GetDeviceContainerList( const NGhostClient *this )
{
	return this->m_device;
}

/**
 * Increase processed requests count
 *
 * @param this
 * 		This instance
 */
void NGhostClient_NGhostClient_IncreaseProcessedRequestCount( NGhostClient *this )
{
	NGhostCommon_NGhostCommon_IncreaseRequestCount( this->m_common );
}

/**
 * Get device status
 *
 * @param this
 * 		This instance
 *
 * @return the device status
 */
const NGhostDeviceStatus *NGhostClient_NGhostClient_GetDeviceStatus( const NGhostClient *this )
{
	return NGhostCommon_NGhostCommon_GetDeviceStatus( this->m_common );
}

/**
 * Get authentication manager
 *
 * @param this
 * 		This instance
 *
 * @return the authentication manager
 */
NAuthenticationManager *NGhostClient_NGhostClient_GetAuthenticationManager( const NGhostClient *this )
{
	return NGhostCommon_NGhostCommon_GetAuthenticationManager( this->m_common );
}

/**
 * Get token manager
 *
 * @param this
 * 		This instance
 *
 * @return the token manager
 */
NGhostTokenManager *NGhostClient_NGhostClient_GetTokenManager( NGhostClient *this )
{
	return NGhostCommon_NGhostCommon_GetTokenManager( this->m_common );
}

/**
 * Get ghost info
 *
 * @param this
 * 		This instance
 *
 * @return the ghost info
 */
const NGhostInfo *NGhostClient_NGhostClient_GetGhostInfo( const NGhostClient *this )
{
	return NGhostCommon_NGhostCommon_GetDeviceInfo( this->m_common );
}

/**
 * Get common
 *
 * @param this
 * 		This instance
 *
 * @return the ghost common
 */
NGhostCommon *NGhostClient_NGhostClient_GetGhostCommon( NGhostClient *this )
{
	return this->m_common;
}

/**
 * Is running?
 *
 * @param this
 * 		This instance
 *
 * @return if client is running
 */
NBOOL NGhostClient_NGhostClient_IsRunning( const NGhostClient *this )
{
	return this->m_isRunning;
}

/**
 * Get IP
 *
 * @param this
 * 		This instance
 *
 * @return the IP
 */
const char *NGhostClient_NGhostClient_GetIP( const NGhostClient *this )
{
	return NGhostCommon_NGhostCommon_GetIP( this->m_common );
}