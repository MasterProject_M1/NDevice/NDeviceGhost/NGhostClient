#include "../include/NGhostClient.h"

// ------------------------
// namespace NGhostListener
// ------------------------

#define GHOST_CLIENT_CONFIGURATION_FILE_PATH		"GhostClient.conf"

/**
 * The entry point
 *
 * @param argc
 * 		The arguments count
 * @param argv
 * 		The arguments vector
 *
 * @return EXIT_SUCCESS if succedeed
 */
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// Ghost client
	NGhostClient *client;

	// Init NLib
	NLib_Initialiser( NULL );

	// Build client
	if( !( client = NGhostClient_NGhostClient_Build( GHOST_CLIENT_CONFIGURATION_FILE_PATH ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit NLib
		NLib_Detruire( );

		// Quit
		return EXIT_FAILURE;
	}

	// Display header
	NDeviceCommon_DisplayHeader( "Client",
		NGhostClient_Configuration_NGhostClientConfiguration_GetNetworkListeningPort( NGhostClient_NGhostClient_GetConfiguration( client ) ),
		NGhostClient_NGhostClient_GetIP( client ) );

	// Wait until close
	while( NGhostClient_NGhostClient_IsRunning( client ) )
		// Wait
		NLib_Temps_Attendre( 16 );

	// Destroy client
	NGhostClient_NGhostClient_Destroy( &client );

	// Quit NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}
